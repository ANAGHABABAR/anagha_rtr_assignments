#include <GL/freeglut.h>

bool bFullscreen = false;

int main(int argc, char** argv)
{
	//function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	//function calls
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);//Red Green Blue Alpha
	glutInitWindowSize(800, 600);//mazya window chi size 800 * 600 kar
	glutInitWindowPosition(100, 100);
	glutCreateWindow("GLUT:ANAGHA");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);

}//end of main function definition


void initialize()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}//end of initialize function definiton

void resize(int width, int height)
{
	if (height <= 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

}//end of resize function definition

void display(void)
{
	//code

	//Triangle
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glTranslatef(0.5f, 0.0f, 0.0f);
	/*glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	 
	glEnd();*/

	//Triangle 2
	//glTranslatef(0.0f, 0.5f, 0.0f);
	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.8f, -0.8f, -0.8f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.8f, 0.8f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.8f, -0.5f, 0.0f);

	glEnd();



	/*Pentagon 

	glBegin(GL_POLYGON);

	glColor3f(1.0f,0.0f,0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(-0.5f,0.0f,0.0f);

	glColor3f(-1.0f,0.0f,-1.0f);
	glVertex3f(-0.3f,-0.5f,0.0f);

	glColor3f(0.0f, 1.0f, -1.0f);
	glVertex3f(0.3f,-0.5f,0.0f);

	glEnd();
	*/


	//square

	/*glLoadIdentity();
	glTranslatef(0.5f, 0.0f, 0.0f);
	glBegin(GL_QUADS);

	glColor3f(1.0f,0.0f,0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.3f, -0.3f, -0.3f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);

	glEnd();*/



	glFlush();

}//end of display function's definition

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':

	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;

	default:
		break;

	}//end of switch
}//end of keyboard function definition


void mouse(int button, int state, int x, int y)
{
	//code

	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;

	deafault:
		break;
	}//end of switch 
}//end of mouse function's definition

void uninitialize(void)
{
	//code
}

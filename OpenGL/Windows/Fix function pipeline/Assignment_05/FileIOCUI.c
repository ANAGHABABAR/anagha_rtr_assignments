#include<stdio.h>
#include<stdlib.h> //for exit()

int main(void)
{
	FILE *pFile = NULL;
	
	if(fopen_s(&pFile, "AnaghaLOG.txt", "w") != 0)
	{
		printf("\nCannot open desired file!\n");
		exit(0);
	}
	
	fprintf(pFile, "India is my Country.\n");
	
	fclose(pFile);
	pFile = NULL;
	
	
	return(0);
}
In this program, we have created a log file using GUI application. 
The moto behind creating the log file for GUI application is, whenever performing any activity on window, we cannt print it on Window.
So, a developer should have a log of what he is performing on a Window. We can print it directly into a file to trace the program's flow as well.

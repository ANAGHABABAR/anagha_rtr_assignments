//Headers
#include <windows.h>
#include<stdio.h>// for FILE, fopen_s, fprintf, fclose

FILE* gpFile = NULL;//global file pointer's declaration

//global function declaraion
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg; 
	TCHAR szAppName[] = TEXT("MyWindow");

	if (fopen_s(&gpFile, "GUILog_Anagha.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot create desired file!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	fprintf(gpFile,"\nInitializing structure members of WNDCLASSEX\n");
	//code
	//initialization of WNDCLASSEX 
	wndclass.cbSize = sizeof(WNDCLASSEX); 
	wndclass.style = CS_HREDRAW | CS_VREDRAW; 
	wndclass.cbClsExtra = 0; 
	wndclass.cbWndExtra = 0; 
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); 
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); 
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	fprintf(gpFile, "\nRegistering the class using RegisterClassEx()\n");
	RegisterClassEx(&wndclass);

	//create window
	fprintf(gpFile, "\nCalling CreateWindow()\n");

	hwnd = CreateWindow(szAppName,
		TEXT("ANAGHA's WINDOW"),
		WS_OVERLAPPEDWINDOW, 
		CW_USEDEFAULT, 
		CW_USEDEFAULT, 
		CW_USEDEFAULT, 
		CW_USEDEFAULT, 
		NULL, 
		NULL, 
		hInstance, 
		NULL);

	fprintf(gpFile, "\nDisplaying the window on screen using ShowWindow()\n");
	ShowWindow(hwnd, iCmdShow);

	fprintf(gpFile, "\nUpdating window using : UpdateWindow()\n");
	UpdateWindow(hwnd);

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		fprintf(gpFile, "\nInside while loop : Message loop\n");
		fprintf(gpFile, "\nCall To : TranslateMessage()\n");
		TranslateMessage(&msg);

		fprintf(gpFile, "\nCall to : DispatchMessage()\n");
		DispatchMessage(&msg);
	}

	fprintf(gpFile, "\n CLOSING THE FILE...\n");
	fclose(gpFile);
	gpFile = NULL;


	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	fprintf(gpFile, "\nInside WndProc() : Callback method\n");

	switch (iMsg)
	{

	case WM_CREATE:
		fprintf(gpFile, "\nInside WM_CREATE\n");
		fprintf(gpFile, "India is My Country. I am proud of my Country.\n");
		break;

	case WM_DESTROY:
		fprintf(gpFile,"Jai Hind\n");
		fprintf(gpFile, "\nInside WM_DESTROY : Calling PostQuitMessage()\n");
		PostQuitMessage(0);
		break;

	}

	fprintf(gpFile, "\nReturning from WndProc : Called DefWindowProc()\n");
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
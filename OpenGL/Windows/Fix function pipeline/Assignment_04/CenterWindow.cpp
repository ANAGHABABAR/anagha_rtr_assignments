#include<windows.h>

//global function declaration
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Assignment_04");
	int iDeskWidth = 0, iDeskHeight = 0;
	int iWidthCenter = 0, iHeightCenter = 0;
	int x = 0, y = 0;

	//code
	iDeskWidth = GetSystemMetrics(SM_CXSCREEN);//x = horizontal
	iWidthCenter = iDeskWidth / 2;

	iDeskHeight = GetSystemMetrics(SM_CYSCREEN);//y = vertical
	iHeightCenter = iDeskHeight / 2;

	x = iWidthCenter - (800 / 2);
	y = iHeightCenter - (600 / 2);

	//initialization of WNDCLASSEX
	
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WindowProcedure;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register he above class
	RegisterClassEx(&wndclass);

	//create window : This creates a window in the memory
	hwnd = CreateWindow(szAppName,
		TEXT("Anagha's 4th Assignment"),
		WS_OVERLAPPEDWINDOW,
		x,
		y,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	//Show window : To show the window on screen which has been created in the Memory using CreateWindow
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


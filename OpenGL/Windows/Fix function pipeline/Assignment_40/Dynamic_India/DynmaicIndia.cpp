#include<windows.h>
#include<stdio.h>
#include <mmsystem.h>
#include "MyIcon.h"
#include "resource.h"
#include<gl/gl.h>//graphic library
#include<gl/glu.h>//graphic library utility
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib, "winmm.lib")

//2 NEW MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;//global file pointer's declaration
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int bDoneCnt = 0;
static GLfloat WaitForI1 = -1.0f;
static GLfloat WaitForA = 1.0f;
static GLfloat WaitForN = -1.0f;
static GLfloat WaitForD = -1.0f;
static GLfloat WaitForI2 = 1.0f;
static GLfloat Aircraft2Split_X = 0.0f;
static GLfloat Aircraft2Split_Y = 0.0f;
BOOL bPlaySound = FALSE;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void Initialize(void);
	void Display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGL Dynamic India");
	bool bDone = false;
	int iDeskWidth = 0, iDeskHeight = 0;
	int iWidthCenter = 0, iHeightCenter = 0;
	int x = 0, y = 0;


	//validite check for opening a file
	if (fopen_s(&gpFile, "OpenGL Dynamic India.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot create desired file!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	fprintf(gpFile, "\nInside WinMain..File Opened in Write Mode Successfully!!\n");

	//code
	iDeskWidth = GetSystemMetrics(SM_CXSCREEN);// x = horizontal
	iWidthCenter = iDeskWidth / 2;

	iDeskHeight = GetSystemMetrics(SM_CYSCREEN);// y = vertical
	iHeightCenter = iDeskHeight / 2;

	x = iWidthCenter - (WIN_WIDTH / 2);
	y = iHeightCenter - (WIN_HEIGHT / 2);

	//initialization of WNDCLASSEX
	fprintf(gpFile, "\nInitializing WNDCLASSEX members");
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	//register he above class
	fprintf(gpFile, "\nCalling RegisterClassEx()\n");
	RegisterClassEx(&wndclass);

	//create window : This creates a window in the memory
	fprintf(gpFile, "\nCalling CreateWindoEx()..\n");
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Dynamic INDIA :  Anagha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		x,
		y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	//Added change in centralize window for fullscreen
	ghwnd = hwnd;
	fprintf(gpFile, "\nCalling Initialize()..\n");
	Initialize();

	//Show window : To show the window on screen which has been created in the Memory using CreateWindow
	fprintf(gpFile, "\nCalling ShowWindow()..\n");
	ShowWindow(hwnd, iCmdShow);

	fprintf(gpFile, "\nCalling SetForegroundWindow()..\n");
	SetForegroundWindow(hwnd);

	fprintf(gpFile, "\nCalling SetFocus()..\n");
	SetFocus(hwnd);

	//GAME LOOP
	while (bDone == false)
	{
		fprintf(gpFile, "\nEntered inside GAME LOOP while() \n");
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			fprintf(gpFile, "\nEntered Inside Game Loop If()\n");
			if (msg.message == WM_QUIT)
			{
				bDoneCnt++;
				fprintf(gpFile, "\nbDone set as true %d times\n", bDoneCnt);
				bDone = true;
			}
			else
			{
				fprintf(gpFile, "\nInside else() of PeekMessage()\nCalling TransalateMessage() \n");
				TranslateMessage(&msg);

				fprintf(gpFile, "\nCalling DispatchMessage()..\n");
				DispatchMessage(&msg);
			}
		}
		else
		{
			fprintf(gpFile, "\nelse of Game loop while()\n");
			if (gbActiveWindow == true)
			{
				//here, you should call Update function for OpenGL rendering


				//here, you should call Display function for OpenGL rendering
				fprintf(gpFile, "\nCalling Display()..\n");
				Display();


			}//gbActiveWindow
		}//else
	}///while

	fprintf(gpFile, "\n CLOSING THE FILE...\n");
	fclose(gpFile);
	gpFile = NULL;

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	//local function declaration
	fprintf(gpFile, "\nInside WndProc()..\n");
	void ToggleFullscreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		fprintf(gpFile, "\nInside Case WM_SETFOCUS\n");
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf(gpFile, "\nInside Case WM_KILLFOCUS\n");
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		fprintf(gpFile, "\nInside Case WM_ERASEBKGND\n");
		return(0);

	case WM_SIZE:
		fprintf(gpFile, "\nInside case WM_SIZE\n");
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		fprintf(gpFile, "\nInside Case WM_KEYDOWN\n");

		switch (wParam)
		{
		case VK_ESCAPE: //VK : Virtual keycode
			fprintf(gpFile, "\nInside Case VK_ESCAPE : Esc pressed.\nCalling DestroyWindow()..\n");
			DestroyWindow(hwnd);

			//fallthrough case
		case 0x46:

		case 0x66:
			fprintf(gpFile, "\nInside Cases 'f' or 'F' fallthrough Calling ToggleFullscreen()\n");
			ToggleFullscreen();
			break;

		default:
			break;
		}//inner switch complete
		break;//WM_KEYDOWN case's brak

	case WM_CLOSE:
		fprintf(gpFile, "\nInside Case WM_CLOSE..\nCalling DestroyWindow()..\n");
		DestroyWindow(hwnd);

	case WM_DESTROY:
		fprintf(gpFile, "\nInside case WM_DESTROY\nCallling Uninitialize() & PostQuitMessage(0)..\n");
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//code
	if (gbFullscreen == false)// 1st if
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);
			}//innermost i.e.3rd if closed
		}//2nd if closed
		ShowCursor(FALSE);
		gbFullscreen = true;
	}//1st if completed

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}//else closed

}//Toggle fullscreen closed

void Initialize()
{
	//function declaration
	void Resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	PlaySound(MAKEINTRESOURCE(soundName),
		GetModuleHandle(TEXT("NULL")),
		SND_RESOURCE | SND_ASYNC);

	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "\nChoosePixelFormat() failed!\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "\nSetPixelFormat() failed!\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() failed!\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "\nwglMakeCurrent() failed!\n");
		DestroyWindow(ghwnd);
	}

	//SetClearColor
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);//black color, last one is of alpha

	//warm up call for Resize()
	Resize(WIN_WIDTH, WIN_HEIGHT);

}

void Resize(int iWidth, int iHeight)
{
	//code
	if (iHeight == 0)
	{
		iHeight = 1;
	}

	glViewport(0, 0, iWidth, iHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)iWidth / (GLfloat)iHeight, 0.1f, 100.0f);

}

void Display()
{
	//function declarations
	void I1(void);
	void N(void);
	void D(void);
	void I2(void);
	void A(void);
	void Aircraft1(void);
	void Aircraft2(void);
	void Aircraft3(void);



		
	//PlaySound(TEXT("RangDe.WAV"), NULL, SND_FILENAME | SND_SENTRY);




	//code
	glClear(GL_COLOR_BUFFER_BIT);

	I1();
	if (WaitForA <= 0)
	{
		N();
	}
	if (WaitForI2 <= 0)
	{
		D();
	}
	if (WaitForN >= 0)
	{
		I2();
	}
	if (WaitForI1 >= 0)
	{
		A();
	}
	

	if (WaitForD >= 1.0f)
	{
		Aircraft1();
		Aircraft2();
		Aircraft3();
	}
	

	//Aircraft1();
	//Aircraft2();
	//Aircraft3();

	SwapBuffers(ghdc);

}


void Aircraft1()
{
	static GLfloat x_animation = -4.0f, y_animation = -4.0f;
	if (x_animation <= 4.7f)
	{
		x_animation += 0.001f;
	}

	if (x_animation >= 4.7f)
	{
		x_animation += 0.001f;
		if (y_animation >= -6.0f)
		{
			y_animation -= 0.001f;
		}
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(x_animation, 0.0f, -6.0f);

	//middle rectangle
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.6f, 0.4, 0.0f);

	glVertex3f(-2.8f, 0.4f, 0.0f);

	glVertex3f(-2.8f, 0.3f, 0.0f);

	glVertex3f(-2.6f, 0.3f, 0.0f);

	glEnd();

	//upper triangle of saffron color

	glBegin(GL_TRIANGLES);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-2.6f, 0.4f, 0.0f);
	glVertex3f(-2.8f, 0.55f, 0.0f);
	glVertex3f(-2.8f, 0.4f, 0.0f);

	glEnd();

	// Below triangle of green color

	glBegin(GL_TRIANGLES);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-2.6f, 0.3f, 0.0f);
	glVertex3f(-2.8f, 0.15f, 0.0f);
	glVertex3f(-2.8f, 0.3f, 0.0f);

	glEnd();

	//front triangle of white color

	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.6f, 0.4, 0.0f);

	glVertex3f(-2.4f, 0.35, 0.0f);

	glVertex3f(-2.6f, 0.3f, 0.0f);

	glEnd();

	// I of IAF

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.78f, 0.38f, 0.0f);
	glVertex3f(-2.78f, 0.32f, 0.0f);

	glEnd();

	//A of IAF

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.74f, 0.38f, 0.0f);
	glVertex3f(-2.76f, 0.32f, 0.0f);

	glVertex3f(-2.74f, 0.38f, 0.0f);
	glVertex3f(-2.72f, 0.32f, 0.0f);

	glVertex3f(-2.75f, 0.35f, 0.0f);
	glVertex3f(-2.73f, 0.35f, 0.0f);

	glEnd();

	//F of IAF
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.7f, 0.38f, 0.0f);
	glVertex3f(-2.7f, 0.32f, 0.0f);

	glVertex3f(-2.7f, 0.38f, 0.0f);
	glVertex3f(-2.66f, 0.38f, 0.0f);

	glVertex3f(-2.7f, 0.35f, 0.0f);
	glVertex3f(-2.67f, 0.35f, 0.0f);

	glEnd();

	//fogg of indian color rectangle

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-2.8f, 0.38f, 0.0f);
	glVertex3f(-2.9f, 0.38f, 0.0f);
	glVertex3f(-2.9f, 0.36f, 0.0f);
	glVertex3f(-2.8f, 0.36f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.8f, 0.36f, 0.0f);
	glVertex3f(-2.9f, 0.36f, 0.0f);
	glVertex3f(-2.9f, 0.34f, 0.0f);
	glVertex3f(-2.8f, 0.34f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-2.8f, 0.34f, 0.0f);
	glVertex3f(-2.9f, 0.34f, 0.0f);
	glVertex3f(-2.9f, 0.32f, 0.0f);
	glVertex3f(-2.8f, 0.32f, 0.0f);

	glEnd();

	//fogg of indian color rectangle

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-2.8f, 0.38f, 0.0f);
	glVertex3f(-2.9f, 0.38f, 0.0f);
	glVertex3f(-2.9f, 0.36f, 0.0f);
	glVertex3f(-2.8f, 0.36f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.8f, 0.36f, 0.0f);
	glVertex3f(-2.9f, 0.36f, 0.0f);
	glVertex3f(-2.9f, 0.34f, 0.0f);
	glVertex3f(-2.8f, 0.34f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-2.8f, 0.34f, 0.0f);
	glVertex3f(-2.9f, 0.34f, 0.0f);
	glVertex3f(-2.9f, 0.32f, 0.0f);
	glVertex3f(-2.8f, 0.32f, 0.0f);

	glEnd();


}

void Aircraft2()		// Upper aircraft
{
	static GLfloat x_animation = -4.0f, y_animation = 4.0f, y_animation2 = 0.0f;
	if (x_animation <= 4.7f)
	{
		x_animation += 0.001f;
	}

	if (y_animation >= -1.0f)
	{
		y_animation -= 0.001f;
	}

	if (x_animation >= 4.7f)
	{
		y_animation2 += 0.001;
	}

	if (x_animation >= 4.7f)
	{
		x_animation += 0.001;
	}

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(x_animation, y_animation+y_animation2, -6.0f);
		
		//middle rectangle
		glBegin(GL_QUADS);

		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-2.6f, 1.4, 0.0f);
		glVertex3f(-2.8f, 1.4f, 0.0f);
		glVertex3f(-2.8f, 1.3f, 0.0f);
		glVertex3f(-2.6f, 1.3f, 0.0f);

		glEnd();

		//upper triangle of saffron color

		glBegin(GL_TRIANGLES);

		glColor3f(1.5f, 0.5f, 0.0f);
		glVertex3f(-2.6f, 1.4f, 0.0f);
		glVertex3f(-2.8f, 1.55f, 0.0f);
		glVertex3f(-2.8f, 1.4f, 0.0f);

		glEnd();

		// Below triangle of green color
	
		glBegin(GL_TRIANGLES);

		glColor3f(0.5f, 1.5f, 0.0f);
		glVertex3f(-2.6f, 1.3f, 0.0f);
		glVertex3f(-2.8f, 1.15f, 0.0f);
		glVertex3f(-2.8f, 1.3f, 0.0f);

		glEnd();

		//front triangle of white color
		glBegin(GL_TRIANGLES);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-2.6f, 1.4, 0.0f);

		glVertex3f(-2.4f, 1.35, 0.0f);

		glVertex3f(-2.6f, 1.3f, 0.0f);

		glEnd();

		// I of IAF
		glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 1.0f);

		glVertex3f(-2.78f, 1.38f, 0.0f);
		glVertex3f(-2.78f, 1.32f, 0.0f);

		glEnd();

		//A of IAF

		glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 1.0f);

		glVertex3f(-2.74f, 1.38f, 0.0f);
		glVertex3f(-2.76f, 1.32f, 0.0f);

		glVertex3f(-2.74f, 1.38f, 0.0f);
		glVertex3f(-2.72f, 1.32f, 0.0f);

		glVertex3f(-2.75f, 1.35f, 0.0f);
		glVertex3f(-2.73f, 1.35f, 0.0f);

		glEnd();

		//F of IAF
		glBegin(GL_LINES);
		glColor3f(0.0f, 0.0f, 1.0f);

		glVertex3f(-2.7f, 1.38f, 0.0f);
		glVertex3f(-2.7f, 1.32f, 0.0f);

		glVertex3f(-2.7f, 1.38f, 0.0f);
		glVertex3f(-2.66f, 1.38f, 0.0f);

		glVertex3f(-2.7f, 1.35f, 0.0f);
		glVertex3f(-2.67f, 1.35f, 0.0f);

		glEnd();

		//fogg of indian color rectangle

		glBegin(GL_QUADS);

		glColor3f(1.5f, 0.5f, 0.0f);
		glVertex3f(-2.8f, 1.38f, 0.0f);
		glVertex3f(-2.9f, 1.38f, 0.0f);
		glVertex3f(-2.9f, 1.36f, 0.0f);
		glVertex3f(-2.8f, 1.36f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-2.8f, 1.36f, 0.0f);
		glVertex3f(-2.9f, 1.36f, 0.0f);
		glVertex3f(-2.9f, 1.34f, 0.0f);
		glVertex3f(-2.8f, 1.34f, 0.0f);

		glColor3f(0.5f, 1.5f, 0.0f);
		glVertex3f(-2.8f, 1.34f, 0.0f);
		glVertex3f(-2.9f, 1.34f, 0.0f);
		glVertex3f(-2.9f, 1.32f, 0.0f);
		glVertex3f(-2.8f, 1.32f, 0.0f);

		glEnd();


}

void Aircraft3()		//Lower Aircraft
{
	static GLfloat x_animation = -4.0f, y_animation = -4.0f, y_animation2 = 0.0f;
	if (x_animation <= 4.7f)
	{
		x_animation += 0.001f;
	}

	if (y_animation <= 1.0f)
	{
		y_animation += 0.001f;
	}

	if (x_animation >= 4.7f)
	{
		y_animation2 -= 0.001f;
	}

	if (x_animation >= 4.7f)
	{
		x_animation += 0.001f;
	}

	Aircraft2Split_X = x_animation;
	Aircraft2Split_Y = y_animation;

	fprintf(gpFile, "\n\n------> Aircraft2Split_X = %f \n\n Aircraft2Split_Y = %f\n\n", Aircraft2Split_X, Aircraft2Split_Y);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(x_animation, y_animation+y_animation2, -6.0f);
	//glTranslatef(0.0f, 0.0f, -6.0f);
	//middle rectangle
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-2.6f, -0.6, 0.0f);

	glVertex3f(-2.8f, -0.6f, 0.0f);
	
	glVertex3f(-2.8f, -0.7f, 0.0f);

	glVertex3f(-2.6f, -0.7f, 0.0f);

	glEnd();

	//upper triangle of saffron color

	glBegin(GL_TRIANGLES);

	glColor3f(1.5f, 0.5f, 0.0f);

	glVertex3f(-2.6f, -0.6f, 0.0f);
	glVertex3f(-2.8f, -0.45f, 0.0f);
	glVertex3f(-2.8f, -0.6f, 0.0f);

	glEnd();

	// Below triangle of green color
	glBegin(GL_TRIANGLES);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-2.6f, -0.7f, 0.0f);
	glVertex3f(-2.8f, -0.85f, 0.0f);
	glVertex3f(-2.8f, -0.7f, 0.0f);

	glEnd();

	//front triangle of white color
	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.6f, -0.6, 0.0f);

	glVertex3f(-2.4f, -0.65, 0.0f);

	glVertex3f(-2.6f, -0.7f, 0.0f);

	glEnd();

	// I of IAF
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.78f, -0.62f, 0.0f);
	glVertex3f(-2.78f, -0.68f, 0.0f);

	glEnd();

	//A of IAF
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.74f, -0.62f, 0.0f);
	glVertex3f(-2.76f, -0.68f, 0.0f);

	glVertex3f(-2.74f, -0.62f, 0.0f);
	glVertex3f(-2.72f, -0.68f, 0.0f);

	glVertex3f(-2.75f, -0.65f, 0.0f);
	glVertex3f(-2.73f, -0.65f, 0.0f);

	glEnd();

	//F of IAF
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(-2.7f, -0.62f, 0.0f);
	glVertex3f(-2.7f, -0.68f, 0.0f);

	glVertex3f(-2.7f, -0.62f, 0.0f);
	glVertex3f(-2.66f, -0.62f, 0.0f);

	glVertex3f(-2.7f, -0.65f, 0.0f);
	glVertex3f(-2.67f, -0.65f, 0.0f);

	glEnd();

	//fogg of indian color rectangle

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-2.8f, -0.62f, 0.0f);
	glVertex3f(-2.9f, -0.62f, 0.0f);
	glVertex3f(-2.9f, -0.64f, 0.0f);
	glVertex3f(-2.8f, -0.64f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-2.8f, -0.64f, 0.0f);
	glVertex3f(-2.9f, -0.64f, 0.0f);
	glVertex3f(-2.9f, -0.66f, 0.0f);
	glVertex3f(-2.8f, -0.66f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-2.8f, -0.66f, 0.0f);
	glVertex3f(-2.9f, -0.66f, 0.0f);
	glVertex3f(-2.9f, -0.68f, 0.0f);
	glVertex3f(-2.8f, -0.68f, 0.0f);

	glEnd();

	
}

void I1()
{
	static GLfloat x_animation = -4.0f, y_animation = 4.0f;
	if (x_animation <= 0.0f)
	{
		x_animation += 0.0005f;
	}

	WaitForI1 = x_animation;

	fprintf(gpFile,"\n**************** WaitForI1 : %f\n\n", WaitForI1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//glTranslatef(0.0f, y_animation, -6.0f);
	glTranslatef(x_animation, 0.0f, -6.0f);

	glBegin(GL_QUADS);// I top horizontal line part

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.2f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.9f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.9f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.2f, 0.7f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);// I middle vertical line

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.5f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.6f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.6f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.5, 0.5, 0.0f);
	glEnd();

	glBegin(GL_QUADS);// I middle vertical line 2nd part

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.5, 0.5, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.6f, 0.5f, 0.0f);

	glVertex3f(-1.6f, 0.3f, 0.0f);

	glVertex3f(-1.5f, 0.3f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);// I middle vertical line 3rd part

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.5, 0.3, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.6f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.6f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.5f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// I bottom horizontal line

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.2f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.9f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.9f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.2f, 0.0f, 0.0f);

	glEnd();

}

void N(void)
{
	static GLfloat x_animation = -4.0f, y_animation = -4.0f;

	if (y_animation <= 0.0f)
	{
		y_animation += 0.0004f;
	}

	WaitForN = y_animation;
	
	fprintf(gpFile, "\n$$$$$$$$$$$$$$$$ WaitForN : %f\n\n", WaitForN);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//glTranslatef(x_animation, 0.0f, -6.0f);
	glTranslatef(0.0f, y_animation, -6.0f);

	// N splitting into 3 QUADS of 1st Vertical Line

	glBegin(GL_QUADS);// N 1st Vertical line : 1/3

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.9f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-1.0f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.5f, 0.0f);

	glEnd();

	//

	glBegin(GL_QUADS);// N 1st Vertical line : 2/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// N 1st Vertical line : 3/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.9f, 0.0f, 0.0f);

	glEnd();



	glBegin(GL_QUADS);// N 2nd Diagonal line

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.9f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.9f, 0.6f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.2f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// N 3rd Vertical line

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glColor3f(1.5f, 1.5f, 0.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.5f, 0.8f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.6f, 0.8f, 0.0f);

	glEnd();


	// N splitting into 3 QUADS of 1st Vertical Line

	glBegin(GL_QUADS);// N 1st Vertical line : 1/3

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.6f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 0.8f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);

	glEnd();

	//

	glBegin(GL_QUADS);// N 1st Vertical line : 2/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// N 1st Vertical line : 3/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glEnd();


}

void D(void)
{
	static GLfloat x_animation = 0.0f, y_animation = 0.0f;
	if (x_animation <= 1.0f)
	{
		x_animation += 0.0001f;
	}

	if (y_animation <= 1.0f)
	{
		y_animation += 0.0001f;
	}

	WaitForD = x_animation;

	fprintf(gpFile, "\n\n***********scalef output x_animation = %f", WaitForD);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -6.0f);
	glScalef(x_animation, y_animation, 0.0f);

	glBegin(GL_QUADS);// 1st D vertical Line : 1/3

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.2f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.5f, 0.0f);


	glEnd();

	glBegin(GL_QUADS);// 1st D vertical Line : 2/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);


	glEnd();

	glBegin(GL_QUADS);// 1st D vertical Line : 3/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.3f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.2f, 0.1f, 0.0f);


	glEnd();

	glBegin(GL_QUADS);// 2nd D horizontal Line : TOP

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.4f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(-0.4f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.8f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// 3rd D horizontal Line :

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(-0.4f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// 4th D Verticalontal Line

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.7f, 0.0f);

	glColor3f(1.5f, 1.5f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.2f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 0.7f, 0.0f);

	glEnd();

	

	glBegin(GL_QUADS);// 1st D vertical Line : 1/3


	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.2f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.5f, 0.0f);


	glEnd();

	glBegin(GL_QUADS);// 1st D vertical Line : 2/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.3f, 0.0f);


	glEnd();

	glBegin(GL_QUADS);// 1st D vertical Line : 3/3

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.2f, 0.1f, 0.0f);

	glEnd();


}

void I2(void)
{

	static GLfloat x_animation = -4.0f, y_animation = 4.0f;

	if (y_animation >= 0.0f)
	{
		y_animation -= 0.0004f;
	}

	WaitForI2 = y_animation;

	fprintf(gpFile, "\n\n$$$$$$$$$$$$$$$$$$ WaitForI2 : %f", WaitForI2);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, y_animation, -6.0f);
	//I - 4th I in India
	glBegin(GL_QUADS);// I top horizontal line part

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.4f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.4f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.1f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.1f, 0.8f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);// I middle vertical line

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.8f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(0.7f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8, 0.5, 0.0f);
	glEnd();

	glBegin(GL_QUADS);// I middle vertical line 2nd part

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8, 0.5, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.5f, 0.0f);

	glVertex3f(0.7f, 0.3f, 0.0f);

	glVertex3f(0.8f, 0.3f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);// I middle vertical line 3rd part

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.8, 0.3, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.7f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.8f, 0.1f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);// I bottom horizontal line part

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(0.4f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.1f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.1f, 0.1f, 0.0f);

	glEnd();


}

void A(void)
{
	static GLfloat x_animation = 4.0f, y_animation = 4.0f;
	if (x_animation >= 0.0f)
	{
		x_animation -= 0.0005f;
	}
	
	WaitForA = x_animation;

	fprintf(gpFile, "\n###################### WaitForA : %f", WaitForA);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(x_animation, 0.0f, -6.0f);
	//broken Diagonal 1 : A 1/3

	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.9f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.3f, 0.8f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.3f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.9f, 0.7f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.4f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.3f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.3f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.5f, 0.0f);

	glEnd();

	//2/4 of LHS of A
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.3f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.3f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.3f, 0.0f);

	glEnd();

	//3/4 of LHS of A vertical line

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.3f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.3f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.4f, 0.1f, 0.0f);

	glEnd();

	//4/4 of LHS of A vertical line

	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.4f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.3f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.3f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.4f, 0.0f, 0.0f);

	glEnd();

	//right vertical line 1/4
	glBegin(GL_QUADS);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.9f, 0.7f, 0.0f);

	glColor3f(1.5f, 0.5f, 0.0f);
	glVertex3f(1.8f, 0.7f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.9f, 0.5f, 0.0f);

	glEnd();

	//right vertical line 2/4
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.9f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.5f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.9f, 0.3f, 0.0f);

	glEnd();

	//right vertical line 3/4
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.9f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.3f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.8f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.9f, 0.1f, 0.0f);

	glEnd();
	//right vertical line 4/4
	glBegin(GL_QUADS);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.9f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.8f, 0.1f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.8f, 0.0f, 0.0f);

	glColor3f(0.5f, 1.5f, 0.0f);
	glVertex3f(1.9f, 0.0f, 0.0f);

	glEnd();

	//MIddle horizontal line of A
	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.4f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.4f, 0.3f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.8f, 0.3f, 0.0f);

	glEnd();

}


void Uninitialize()
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);


		if (wglGetCurrentContext() == ghrc)
		{
			wglMakeCurrent(NULL, NULL);//aata amhi rendering context vaparat nhi ahot
		}

		if (ghrc)//rendering context ahe ka bgha.asel tr delete kara
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}

		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}

}

#include<windows.h>
#include<stdio.h>
#include "MyIcon.h"
#include<gl/gl.h>
#pragma comment(lib, "opengl32.lib")

//2 NEW MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;
FILE* gpFile = NULL;//global file pointer's declaration
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	void Initialize(void);
	void Display(void);

	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGL_Template_Application");
	bool bDone = false;
	int iDeskWidth = 0, iDeskHeight = 0;
	int iWidthCenter = 0, iHeightCenter = 0;
	int x = 0, y = 0;


	//validitye check for opening a file
	if (fopen_s(&gpFile, "OpenGLTemplate_Anagha.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot create desired file!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	//code
	iDeskWidth = GetSystemMetrics(SM_CXSCREEN);// x = horizontal
	iWidthCenter = iDeskWidth / 2;

	iDeskHeight = GetSystemMetrics(SM_CYSCREEN);// y = vertical
	iHeightCenter = iDeskHeight / 2;

	x = iWidthCenter - (WIN_WIDTH / 2);
	y = iHeightCenter - (WIN_HEIGHT / 2);

	//initialization of WNDCLASSEX
	fprintf(gpFile, "\nBefore initialization of wndclass..\n");
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	fprintf(gpFile, "\nAfter initialization of wndclass..\n");

	//register he above class
	RegisterClassEx(&wndclass);
	fprintf(gpFile, "\n Call to RegisterClassEx() Completed..\n");

	//create window : This creates a window in the memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL TEMPLATE :  Anagha"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);
	fprintf(gpFile, "\nCall to hwnd = CreateWindowEx() completed \n");

	//Added change in centralize window for fullscreen
	ghwnd = hwnd;
	Initialize();
	fprintf(gpFile, "\.Call to Initialize() Completed.\n");

	//Show window : To show the window on screen which has been created in the Memory using CreateWindow
	ShowWindow(hwnd, iCmdShow);
	fprintf(gpFile, "\nCall to ShowWindow() completed..\n");

	SetForegroundWindow(hwnd);
	fprintf(gpFile, "\nCall to SetForegroundWindow() completed..\n");

	SetFocus(hwnd);
	fprintf(gpFile, "\nCall to SetFocus() completed..\n");

	//GAME LOOP
	while (bDone == false)
	{
		fprintf(gpFile, "\nInside while()..\n");

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			fprintf(gpFile, "\nInside if() of PeekMessage..\n");

			if (msg.message == WM_QUIT)
			{
				fprintf(gpFile, "\nInside msg.message == WM_QUIT\n");

				bDone = true;
			}
			else
			{
				fprintf(gpFile, "\n Else of msg.message == WM_QUIT\n");

				TranslateMessage(&msg);
				fprintf(gpFile, "\nCall to TranslateMessage() completed..\n");

				DispatchMessage(&msg);
				fprintf(gpFile, "\nCall to DispatchMessage() completed..\n");

			}
		}
		else
		{
			fprintf(gpFile, "\nelse{} of PeekMessage() \n");

			if (gbActiveWindow == true)
			{

				fprintf(gpFile, "\nInside else { if {} } of PeekMessage \n");

				//here, you should call Update function for OpenGL rendering

				//here, you should call Display function for OpenGL rendering
				fprintf(gpFile, "\nCalling Display()\n");
				Display();
			}//gbActiveWindow
		}//else
	}///while

	

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf(gpFile, "\nEntered  in WndProc().. \n");

	//local function declaration
	void ToggleFullscreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	//code
	switch (iMsg)
	{

	case WM_SETFOCUS:
		fprintf(gpFile, "\nInside WM_SETFOCUS case..\tgbActiveWindow set to True..\n");

		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf(gpFile, "\nInside WM_KILLFOCUS case..\tgbActiveWndow set to false\n");

		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		fprintf(gpFile, "\nInside WM_ERASEBKGND case..\t Returning with status (0)\n");

		return(0);

	case WM_SIZE:
		fprintf(gpFile, "\nInside WM_SIZE case..\tCalling Resize\n");

		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
		fprintf(gpFile, "\nInside WM_KEYDOWN case..\n");

		switch (wParam)
		{
		case VK_ESCAPE: //VK : Virtual keycode
			fprintf(gpFile, "\nInside WM_ESCAPE case..\tCalling DestroyWindow()..\n");

			DestroyWindow(hwnd);

		//fallthrough case
		case 0x46:
			fprintf(gpFile, "\nInside 0x46 case..for Capital or small 'f'..\t calling ToggleFullscreen()..\n");

		case 0x66:
			fprintf(gpFile, "\nInside 0x66 case..for Capital or small 'f'..\tCalling ToggleFullscreen()..\n");

			ToggleFullscreen();
			break;

		default:
			fprintf(gpFile, "\nInside default case..\n");

			break;
		}//inner switch complete
		break;//WM_KEYDOWN case's brak

	case WM_CLOSE:
		fprintf(gpFile, "\nInside WM_CLOSE case..\tCalling DestroyWindow()\n");

		DestroyWindow(hwnd);

	case WM_DESTROY:
		fprintf(gpFile, "\nInside WM_DESTROY case..\tCalling Uninitialize() and PostQuitMessage()..\n");

		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	fprintf(gpFile, "\nreturn of DefWindowProc()..\n");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	//local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };
	fprintf(gpFile, "\nAfter MONITORINFO structure initialization..\n");


	//code
	if (gbFullscreen == false)// 1st if
	{
		fprintf(gpFile, "\nInside if(gbfullscreen == false)..\n");

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		fprintf(gpFile, "\nAfter dwStyle = GetWindowLong()..\n");

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			fprintf(gpFile, "\nInside dwStyle & WS_OVERLAPPEDWINDOW condition\n");

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nCalling SetWindowLong()..\n");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				fprintf(gpFile, "\nCalling SetWindowPos()..\n");
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);
			}//innermost i.e.3rd if closed
		}//2nd if closed
		fprintf(gpFile, "\nCalling ShowCursor()..\n");
		ShowCursor(FALSE);
		gbFullscreen = true;
	}//1st if completed

	else
	{
		fprintf(gpFile, "\nCalling SetWindowLong() inside else..\n");
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		fprintf(gpFile, "\nCalling SetWindowPlacement() inside else\n");
		SetWindowPlacement(ghwnd, &wpPrev);

		fprintf(gpFile, "\nCalling SetWindowPos() inside else..\n");
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		fprintf(gpFile, "\nCalling ShowCursor(TRUE) inside else..\n");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}//else closed

}//Toggle fullscreen closed

void Initialize()
{
	fprintf(gpFile, "\nInside Initialize()..\n");

	//function declaration
	void Resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "\nChoosePixelFormat() failed!\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "\nSetPixelFormat() failed!\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() failed!\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "\nwglMakeCurrent() failed!\n");
		DestroyWindow(ghwnd);
	}

	//SetClearColor
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	//warm up call for Resize()
	Resize(WIN_WIDTH, WIN_HEIGHT);

}

void Resize(int iWidth, int iHeight)
{
	//code
	if (iHeight == 0)
	{
		iHeight = 1;
	}

	glViewport(0, 0, (GLsizei)WIN_WIDTH, (GLsizei)WIN_HEIGHT);
}

void Display()
{
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	SwapBuffers(ghdc);//call of win32 SDK
}


void Uninitialize()
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);


		if (wglGetCurrentContext() == ghrc)
		{
			wglMakeCurrent(NULL, NULL);//aata amhi rendering context vaparat nhi ahot
		}

		if (ghrc)//rendering context ahe ka bgha.asel tr delete kara
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}

		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}
	fprintf(gpFile, "\n CLOSING the file from Uninitialize()...\n");
	fclose(gpFile);
	gpFile = NULL;
}

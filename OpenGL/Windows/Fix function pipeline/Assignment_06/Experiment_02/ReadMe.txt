This is an experimental application completed using Assignment's actual program with suggested changes by Sir.
In this application we have used 'GetDC' instead of BeginPaint and 'ReleaseDC' instead of EndPaint. 
WM_PAINT's case has been replaced with 'WM_LBUTTONDOWN'. So, prviously what used to happen on output window directly, i.e. Printing of 'Hello World!!!', will now take place o performing the Left click.
Also, Log file has been attached for the better understanding of a Program's flow.
The output screenshot is attached in the same folder.

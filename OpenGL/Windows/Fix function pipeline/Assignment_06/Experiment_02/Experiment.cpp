//Headers
#include <windows.h>
#include<stdio.h>// for FILE, fopen_s, fprintf, fclose

//global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gpFile = NULL;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	if (fopen_s(&gpFile, "PaintExperiment_LOG.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot create desired file!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	//code
	fprintf(gpFile, "\nIndie WinMain()..\n");

	fprintf(gpFile, "\nInitializing the WNDCLASSEX members..\n");
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	fprintf(gpFile, "\nCalling function : RegisterClassEx()\n");
	RegisterClassEx(&wndclass);

	//create window
	fprintf(gpFile, "\nCalling function : CreateWindow() and return value i.e. window handle will be stored in 'hwnd'\n");
	hwnd = CreateWindow(szAppName,
		TEXT("ANAGHA's PAINT APPLICATION"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	fprintf(gpFile, "\nCalling function: ShowWindow() to Display the window on screen\n");
	ShowWindow(hwnd, iCmdShow);

	UpdateWindow(hwnd);

	//message loop
	fprintf(gpFile, "\nEntering while : Message loop\n");
	while (GetMessage(&msg, NULL, 0, 0))
	{
		fprintf(gpFile, "\ncalling function : TranslateMessage()\n");
		TranslateMessage(&msg);

		fprintf(gpFile, "\ncalling function : DispatchMessage()\n");
		DispatchMessage(&msg);
	}

	fprintf(gpFile, "\nClosing the file..\n");
	fclose(gpFile);
	gpFile = NULL;


	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//variable declarations
	HDC hdc;
	RECT rc;
	TCHAR str[] = TEXT("Hello World!!!");

	//code
	switch (iMsg)
	{
	case WM_LBUTTONDOWN:

		fprintf(gpFile, "\n\nLeft button clicked.\nInside WM_LBUTTONDOWN case..\n");
		fprintf(gpFile, "\nCalling function : GetClientRect()\n");
		GetClientRect(hwnd, &rc);

		fprintf(gpFile, "\nCalling function : GetDC() and the return value is stored in 'hdc'\n");
		hdc = GetDC(hwnd);
		fprintf(gpFile, "\n'hdc is : '\n");

		fprintf(gpFile, "\nCalling function: SetBkColor to set background color as BLACK\n");
		SetBkColor(hdc, RGB(0, 0, 0));

		fprintf(gpFile, "\nCalling function : SetTextColor() to set text color as GREEN\n");
		SetTextColor(hdc, RGB(0, 255, 0));

		fprintf(gpFile, "\nCalling function : DrawText()\n");
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

		fprintf(gpFile, "\nCalling function : ReleaseDC()\n");
		ReleaseDC(hwnd, hdc);
		break;

	case WM_DESTROY:
		fprintf(gpFile, "\nInside WM_DESTROY.\n Calling function : PostQuitMessage()\n");
		PostQuitMessage(0);
		break;
	}

	fprintf(gpFile, "\nReturning from WndProc.\n Calling function : DefWindowProc()\n");
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

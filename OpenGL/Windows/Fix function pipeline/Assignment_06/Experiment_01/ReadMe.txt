This is an experimental application completed using Assignment's actual program with suggested changes by Sir.
In this application we have used 'GetDC' instead of BeginPaint and 'ReleaseDC' instead of EndPaint. Also, Log file has been attached for the better understanding of a Program's flow.
The output screenshot is attached in the same folder.

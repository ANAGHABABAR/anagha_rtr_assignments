In this program, we have added message boxes on WM_CREATE, WM_LBUTTONDOWN,WM_KEYDOWN.
WM_CREATE message boc opens when window gets created which happens only once.
WM_LBUTTONDOWN : on the click of mouse's button, this message box appears.
WM_KEYDOWN: on the click of any keyboard's key, this message box appears.

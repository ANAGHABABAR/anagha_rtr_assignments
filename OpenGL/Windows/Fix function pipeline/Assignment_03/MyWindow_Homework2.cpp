//Headers
#include <windows.h>

//global function declaraion
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;// -> HANDLE -> UINT -> unsigned int
	MSG msg; //struct of 6 members as : HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam , DWORD time(time at which message was posted),POINT pt, DWORD lPrivate
			 //(POINT pt: the cursor position, in screen coordinates, when the message was posted.
	TCHAR szAppName[] = TEXT("MyWindow");//TCHAR: wide character type, in UNICODE


	//code
	//initialization of WNDCLASSEX 
	wndclass.cbSize = sizeof(WNDCLASSEX); //Count of Bytes
	wndclass.style = CS_HREDRAW | CS_VREDRAW; //CS : Class Style, H: Horizontal, V: Vertical
	wndclass.cbClsExtra = 0; //Class baddal Kahi extra mahiti sangychi ahe ka?
	wndclass.cbWndExtra = 0; //Window baddal kahi extra mahiti sangychi ahe ka?
	wndclass.lpfnWndProc = WndProc;//Callbac function cha address, as the function name itself represents an address of the function
	wndclass.hInstance = hInstance;//Main madhun aalela hInstance
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION); //NULL indicates: DEVuser tells OS, "I don't have my custom Icon. Please give default",IDI_APPLICATION : APPLICATION chya ID cha ICON de
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //NULL indicates : Dev User tells OS,"I don't have my custom Icon. Please give default", IDC_ARROW: ID Cursor ARROW
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	//GetStockObject returns an object of type HGDIOBJ i.e. Handle to Graphics Device Interface Object , which we need to typecast as per our need
	//Here our need as HBRUSH. Hence we have typecasted it using HBRUSH. 2 More as HPEN, HFONT are the type of objects that we can typecast to,if needed.
	wndclass.lpszClassName = szAppName;//Ya case madhe MyWindow mentioned on line no 15
	wndclass.lpszMenuName = NULL;//Tuzya class la Menu la naav ahe ka? : Nahi
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);//OS la apan sangto ki mazyakde maza custom Icon nhiye tar tuza Default icon de jo APPLICATION chya ID cha ICON asel
	//ani 'Sm' represents  Small Icon mhnje jo apan task bar mdhe bghto to. mhnjech Window Minimize kelyvr ha icon taskbar madhe disla pahije

	//register above class
	RegisterClassEx(&wndclass);//structure cha address pathvate ahe. No return value is caught since we are not going to use it anywhere ahead in the code

	//create window
	hwnd = CreateWindow(szAppName, //Class chya window ch Naav i.e. MyWindow_Homework2
		TEXT("ANAGHA's WINDOW"),//Windows ch Caption bar var ch naav konta pahije
		WS_OVERLAPPEDWINDOW, //tayari honari window hi Z-ORDER madhe TOP l pahije 
		CW_USEDEFAULT, //CW_USEDEFAULT is a Macro like above all params, We tell OS here: "Tula yogya vatel to X co-ordinate de"
		CW_USEDEFAULT, //"Tula(OS) yogya vatel to Y co-ordinate de mhnjech DEFAULT"
		CW_USEDEFAULT, //"Tula(OS) yogya vatel ti mhnjech DEFAULT WIDTH de"
		CW_USEDEFAULT, //"Tula(OS) yogya vatel ti HEIGHT de"
		NULL, //OS vicharte tu kontya Window chi CHILD window ahes ka?: Tevha apan NUL sangto, in this case OS understands internally this is
		      //HWND_DESKTOP ahe. Mhnjech aplya window chi PARENT window HI DESKTOP ahe.
		NULL, //Window chya MENU cha handle e.g File, Edit, Search, etc.. Jar window la MENU nasel tr NULL mhan
		hInstance, //WinMain cha 1st Parameter
		NULL);//Varchya 10 goshtin peksha ajun kahi Additional Information dyaychi ahe ka?
	//CreateWindow() he MEMORY mdhe Window tayar karycha kaam krta. Actual Displayin ShowWindow() mule hota
	
	ShowWindow(hwnd, iCmdShow);//return value for thi sfunction is not caught as we're not going to use that value ahead in the code. 
							   //ShowWindow() chi return value hi BOOL ahe either TRUE(1) or FALSE(0). This function acually displayes the Window on Screen
	UpdateWindow(hwnd);// Mazya Window chi wall Mazya Class chya Brush chya color ne paint kr. In out case it is BLACK.
					   // Even if the Developer uSer doesn't call UpdateWindow() in that case OS internallly calls it.

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	switch (iMsg)
	{
	case WM_DESTROY: 
					PostQuitMessage(0);
					break;
	case WM_CREATE: 
					MessageBox(hwnd, TEXT("WM_CREATE is Received"), TEXT("MyMessage-1"), MB_OK);
					break;

	case WM_LBUTTONDOWN:
					MessageBox(hwnd, TEXT("WM_LBUTTONDOWN is Received"), TEXT("MyMessage-2"), MB_OK);
					break;
		
	case WM_KEYDOWN:
					MessageBox(hwnd, TEXT("WM_KEYDOWN is Received"), TEXT("MyMessage-3"), MB_OK);
					break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
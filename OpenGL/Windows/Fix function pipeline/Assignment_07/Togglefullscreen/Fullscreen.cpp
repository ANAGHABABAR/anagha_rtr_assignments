#include<windows.h>

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Assignment_07");
	int iDeskWidth = 0, iDeskHeight = 0;
	int iWidthCenter = 0, iHeightCenter = 0;
	int x = 0, y = 0;

	//code
	iDeskWidth = GetSystemMetrics(SM_CXSCREEN);//x = horizontal
	iWidthCenter = iDeskWidth / 2;

	iDeskHeight = GetSystemMetrics(SM_CYSCREEN);//y = vertical
	iHeightCenter = iDeskHeight / 2;

	x = iWidthCenter - (800 / 2);
	y = iHeightCenter - (600 / 2);

	//initialization of WNDCLASSEX

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register he above class
	RegisterClassEx(&wndclass);

	//create window : This creates a window in the memory
	hwnd = CreateWindow(szAppName,
		TEXT("Toggle Fullscreen Assignment :  Anagha"),
		WS_OVERLAPPEDWINDOW,
		x,
		y,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	//Added change in centralize window for fullscreen
	ghwnd = hwnd;

	//Show window : To show the window on screen which has been created in the Memory using CreateWindow
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	//local function declaration
	void ToggleFullscreen(void);

	//code
	switch (iMsg)
	{

	case WM_KEYDOWN:

		switch (wParam)
		{
			//falllthrough case
		case 0x46:

		case 0x66:
			ToggleFullscreen();
			break;

		Default: 
			break;
		}//inner switch complete
		break;//WM_KEYDOWN case's brak


	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen()
{
	//local variable declaration
	MONITORINFO mi = {sizeof(MONITORINFO)};

	//code
	if (gbFullscreen == false)// 1st if
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, (mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);
			}//innermost i.e.3rd if closed
		}//2nd if closed
		ShowCursor(FALSE);
		gbFullscreen = true;
	}//1st if completed

	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));

		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}//else closed

}//Toggle fullscreen closed

#include <stdio.h>

int main(void)
{
	//Defining Struct
	struct MyData
	{
		int i;
		float f;
		long double d;
	}data;//declaring a single struct variable of type MyData locally
	
	//variable declarations
	int iSize;
	int fSize;
	int dSize;
	int struct_MyData_Size;
	
	//assigning values to members of MyData struct
	data.i = 90;
	data.f = 3.56f;
	data.d = 34.56789;
	
	//Displaying members of the struct MyData
	printf("\n i = %d\n f = %f\n d = %lf\n",data.i,data.f,data.d);
	
	//Sizes of all datatypes including MyData struct's size
	iSize = sizeof(data.i);
	fSize = sizeof(data.f);
	dSize = sizeof(data.d);
	struct_MyData_Size = sizeof(data);
	
	//displaying all the calculated sizes
	printf("\nSize of i = %d\n",iSize);
	printf("\nSize of f = %d\n",fSize);
	printf("\nSize of d = %d\n",dSize);
	printf("\nSize of struct MyData = %d\n",struct_MyData_Size);
	

	return(0);
}
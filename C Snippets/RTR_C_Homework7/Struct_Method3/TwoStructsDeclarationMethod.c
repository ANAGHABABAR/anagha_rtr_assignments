#include <stdio.h>

int main(void)
{
	
	//DEFINING STRUCT
	struct MyPoint
	{
		int x;
		int y;
	}point;

	struct MyPointProperties
	{
		int quadrant;
		char axis_location[10];
	}point_properties;

	printf("\nEnter X co-ordinated for a Point : \n");
	scanf("%d",&point.x);
	
	printf("\nEnter Y co-ordinated for a Point : \n");
	scanf("%d",&point.y);
	
	printf("\nThe co-ordinates (x,y) that you have entered are (%d,%d)\n",point.x,point.y);
	
	if((point.x == 0) && (point.y == 0))
	{
		printf("\nThe point is the Origin!\n");
	}
	else
	{
		if(point.x == 0)
		{
			if(point.y < 0)
			{
				strcpy(point_properties.axis_location, "Negative Y");
			}
			
			if(point.y > 0)
			{
				strcpy(point_properties.axis_location,"Posiive Y");
			}
			
			point_properties.quadrant = 0; //A point lying on any of the Quadrant axis is not a part of any Quadrant
			printf("\nThe Point lies on %s AXIS!!\n",point_properties.axis_location);
		}
		else if(point.y == 0)
		{
			if(point.x < 0)
			{
				strcpy(point_properties.axis_location, "Negative X");
			}
			
			if(point.x > 0)
			{
				strcpy(point_properties.axis_location,"Posiive X");
			}
			
			point_properties.quadrant = 0; //A point lying on any of the Quadrant axis is not a part of any Quadrant
			printf("\nThe Point lies on %s AXIS!!\n",point_properties.axis_location);
		}
		else //Both x and y are NON ZERO
		{
			point_properties.axis_location[0] = '\0';// A point lying in any of the 4 quadrants cannot be lying on any of the co-ordinate axes
			
			if(point.x > 0 && point.y > 0)//both values are positive
			{
				point_properties.quadrant = 1;
			}
			else if(point.x < 0 && point.y < 0)//both values are negative
			{
				point_properties.quadrant = 3;
			}
			else if(point.x < 0 && point.y > 0)//X is Negative, Y is POSITIVE
			{
				point_properties.quadrant = 2;
			}
			else//Y is Negative and X is POSITIVE
			{
				point_properties.quadrant = 4;
			}
			printf("\nThe Point lies in %d QUADRANT!!\n",point_properties.quadrant);

		}
	}
	return(0);
}
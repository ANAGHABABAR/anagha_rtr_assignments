#include <stdio.h>

int main(void)
{
	//defining struct
	struct MyPoint
	{
		int x;
		int y;
	}point_A,point_B,point_C,point_D,point_E; //declaration of MyPoint struct's variables locally
	
	//Assigning data values to data members of struct MyPoint for variable point_A
	point_A.x = 10;	
	point_A.y = 11;
	
	//Assigning data values to data members of struct MyPoint for variable point_B
	point_B.x = 20;	
	point_B.y = 21;
	
	//Assigning data values to data members of struct MyPoint for variable point_C
	point_C.x = 30;	
	point_C.y = 31;
	
	//Assigning data values to data members of struct MyPoint for variable point_D
	point_D.x = 40;	
	point_D.y = 41;
	
	//Assigning data values to data members of struct MyPoint for variable point_E
	point_E.x = 50;	
	point_E.y = 51;
	
	//Displaying values for all data members in all vaariables
	printf("\nValues assigned for data members of point_A are (x,y) = (%d,%d)\n",point_A.x,point_A.y);
	printf("\nValues assigned for data members of point_B are (x,y) = (%d,%d)\n",point_B.x,point_B.y);
	printf("\nValues assigned for data members of point_C are (x,y) = (%d,%d)\n",point_C.x,point_C.y);
	printf("\nValues assigned for data members of point_D are (x,y) = (%d,%d)\n",point_D.x,point_D.y);
	printf("\nValues assigned for data members of point_E are (x,y) = (%d,%d)\n",point_E.x,point_E.y);

	
	
	return(0);
}
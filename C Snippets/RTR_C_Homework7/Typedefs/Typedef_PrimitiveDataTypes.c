#include<stdio.h>

typedef int MyInt;//type int has now been re-defined as MyInt.

int main()
{
	MyInt Add(MyInt,MyInt);//function prototype
	
	typedef int MyInt;
	typedef float MyFloat;
	typedef char CHARACTER;
	typedef double MyDouble;
	
	//Typedefs like win32 SDK
	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINSTANCE;
	
	//variable declarations
	MyInt a = 10, i;
	MyInt iArray[] = {9, 11, 21, 51, 101, 99, 189, 199};
	
	MyFloat f_arb = 3.14f;
	const arb_float = 45.67f;
	
	CHARACTER ch = '*';
	CHARACTER ch_Array1[] = "Hello ANAGHA";
	CHARACTER ch_Array2D[][10] = {"RTR","BATCH","2020-2021"};
	
	MyDouble d = 78.565664;
	
	UINT uint = 3456;
	HANDLE handle = 9867;
	HWND hwnd = 5667;
	HINSTANCE hinstance = 6768;
	
	printf("\n\n");
	
	printf("\nType MyInt a = %d\n",a);
	
	for(i = 0;i < sizeof(iArray); i++)
	{
		printf("\n Type MyInt array variable iArray[%d] = %d",i, iArray[i]);
	}
	
	printf("\nThe arb_float variable has the value %f\n",f_arb);
	printf("\nThe MyDouble variable is d = %lf\n",d);
	printf("\nThe character variable ch is = %c\n",ch);
	
	printf("\nType character variable ch_Array1 = %s \n",ch_Array1);
	
	for(i = 0;i < (sizeof(ch_Array2D)/sizeof(ch_Array2D[0])); i++)
	{
		printf("\n%s", ch_Array2D[i]);
	}
	
	
	printf("\nType UINT variable uint = %u\n",uint);
	printf("\nType HANDLE variable is = %u\n",handle);
	printf("\nType HWND variable is = %u\n",hwnd);
	printf("\nType HINSTANCE variable is = %u\n",hinstance);
	
	MyInt x = 40, y = 30, result = 0;
	
	result = Add(x,y);
	printf("\nAddition Result is %d\n", result);
	
	return(0);
}

MyInt Add(MyInt a, MyInt b)
{
	return a+b;
}
#include<stdio.h>

#define MAX_NAME_LENGTH 100

struct Employee
{
	char name[MAX_NAME_LENGTH];
	unsigned int age;
	char gender;
	double salary;
};

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	//Typedefs
	typedef struct Employee MyEmployeeType;
	typedef struct MyData MyDataType;
	
	//variable decclarations
	struct Employee emp = {"Govind",25,'M',10000.00};
	MyEmployeeType typedef_Emp = {"Madhav",26,'M',20000.00};
	
	struct MyData md = {30, 11.45f, 26.123456,'X'};
	MyDataType md_typedef;
	
	md_typedef.i = 11;
	md_typedef.f = 99.56f;
	md_typedef.d = 56.789065;
	md_typedef.c = 'T'; 
	
	printf("\nDisplaying Employee details : struct Employee emp\n");
	printf("\nEmployee name = %s\n",emp.name);
	printf("\nEmployee age = %d\n",emp.age);
	printf("\nEmployee gender = %c\n",emp.gender);
	printf("\nEmployee salary = %lf\n",emp.salary);
	
	printf("\nDisplaying Employee details : MyEmployeeType\n");
	printf("\nEmployee name = %s\n",typedef_Emp.name);
	printf("\nEmployee age = %d\n",typedef_Emp.age);
	printf("\nEmployee gender = %c\n",typedef_Emp.gender);
	printf("\nEmployee salary = %lf\n",typedef_Emp.salary);
	
	printf("\nstruct MyData : \n");
	printf("md.i = %d\n",md.i);
	printf("md.f = %f\n",md.f);
	printf("md.d = %lf\n",md.d);
	printf("md.c = %c\n",md.c);
	
	printf("\nstruct md_typedef : \n");
	printf("md_typedef.i = %d\n",md_typedef.i);
	printf("md_typedef.f = %f\n",md_typedef.f);
	printf("md_typedef.d = %lf\n",md_typedef.d);
	printf("md_typedef.c = %c\n",md_typedef.c);
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
}data;

int main(void)
{
	printf("\nEnter an INTEGER number : \n");
	scanf("%d",&data.i);
	
	printf("\nEnter an FLOAT number : \n");
	scanf("%f",&data.f);
	
	printf("\nEnter an DOUBLE number : \n");
	scanf("%lf",&data.d);
	
	printf("\nEnter an CHARACTER : \n");
	data.c = getch();
	
	printf("\nDisplaying all user accepted data members\n");
	printf("\ni = %d\n",data.i);
	printf("\nf = %f\n",data.f);
	printf("\nd = %lf\n",data.d);
	printf("\nc = %c\n",data.c);
	
	return(0);
}

#include<stdio.h>

struct MyData
{
	int x;
	int y;
};

int main(void)
{
	printf("\nINSIDE MAIN\n");
	struct MyData point_A,point_B,point_C,point_D,point_E;
	
	printf("\nEnter x member point_A : \n");
	scanf("%d",&point_A.x);
	printf("\nEnter y member : \n");
	scanf("%d",&point_A.y);
	
	printf("\nEnter x member point_B : \n");
	scanf("%d",&point_B.x);
	printf("\nEnter y member : \n");
	scanf("%d",&point_B.y);
	
	printf("\nEnter x member point_C : \n");
	scanf("%d",&point_C.x);
	printf("\nEnter y member : \n");
	scanf("%d",&point_C.y);
	
	printf("\nEnter x member point_D : \n");
	scanf("%d",&point_D.x);
	printf("\nEnter y member : \n");
	scanf("%d",&point_D.y);
	
	printf("\nEnter x member point_E : \n");
	scanf("%d",&point_E.x);
	printf("\nEnter y member : \n");
	scanf("%d",&point_E.y);
	
	printf("\nData members of point_A are (x, y) = (%d, %d)\n",point_A.x,point_A.y);
	printf("\nData members of point_B are (x, y) = (%d, %d)\n",point_B.x,point_B.y);
	printf("\nData members of point_C are (x, y) = (%d, %d)\n",point_C.x,point_C.y);
	printf("\nData members of point_D are (x, y) = (%d, %d)\n",point_D.x,point_D.y);
	printf("\nData members of point_E are (x, y) = (%d, %d)\n",point_E.x,point_E.y);
	
	return(0);
}
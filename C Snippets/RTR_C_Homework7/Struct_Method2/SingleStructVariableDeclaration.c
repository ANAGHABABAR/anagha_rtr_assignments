#include <stdio.h>

//Defining struct
struct MyData
{
	int i;
	float f;
	double d;
};

struct MyData data;//declaring a single struct variable of MyData Struct type

int main(void)
{
	int iSize;
	int fSize;
	int dSize;
	int struct_MyData_Size;
	
	//code
	data.i = 30;
	data.f = 11.56f;
	data.d = 1.23343;
	
	//Displaying values of data members of struct MyData
	printf("\ni =%d\n",data.i);
	printf("\nf =%f\n",data.f);
	printf("\nd =%lf\n",data.d);
	
	//calculating the sizes
	iSize = sizeof(data.i);
	fSize = sizeof(data.f);
	dSize = sizeof(data.d);
	struct_MyData_Size = sizeof(data);
	
	//Displaying calculated sizes
	printf("\nThe size of i = %d, in bytes\n",iSize);
	printf("\nThe size of f = %d, in bytes\n",fSize);
	printf("\nThe size of d = %d, in bytes\n",dSize);
	printf("\nThe size of struct_MyData_Size = %d, in bytes\n",struct_MyData_Size);
	
	return(0);
}
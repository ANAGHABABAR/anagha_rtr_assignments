#include <stdio.h>

//Defining struct
struct MyPoint
{
	int x;
	int y;
}point_A,point_B,point_C,point_D,point_E; //declaring 5 struct variables of type 'struct MyPoint' globally

int main(void)
{
	//Assigning data values to the data members of 'struct MyPoint' variable point_A
	point_A.x = 10;
	point_A.y = 20;
	
	//Assigning data values to the data members of 'struct MyPoint' variable point_B
	point_B.x = 11;
	point_B.y = 21;
	
	//Assigning data values to the data members of 'struct MyPoint' variable point_C
	point_C.x = 51;
	point_C.y = 101;
	
	//Assigning data values to the data members of 'struct MyPoint' variable point_D
	point_D.x = 201;
	point_D.y = 202;
	
	//Assigning data values to the data members of 'struct MyPoint' variable point_E
	point_A.x = 1001;
	point_A.y = 2001;
	
	//Displaying values for all variables of struct MyPoint
	printf("\nThe value for co-ordinates (x,y) in point_A is (%d,%d)\n",point_A.x,point_A.y);
	printf("\nThe value for co-ordinates (x,y) in point_B is (%d,%d)\n",point_B.x,point_B.y);
	printf("\nThe value for co-ordinates (x,y) in point_C is (%d,%d)\n",point_C.x,point_C.y);
	printf("\nThe value for co-ordinates (x,y) in point_D is (%d,%d)\n",point_D.x,point_D.y);
	printf("\nThe value for co-ordinates (x,y) in point_E is (%d,%d)\n",point_E.x,point_E.y);
		
	return(0);
}
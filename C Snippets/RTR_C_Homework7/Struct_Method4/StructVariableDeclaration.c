#pragma pack(1)//to consider exac size of struct. Without this the size of struct in current scenario would be 24. With the use of #pragma pack(1), the size of struct MyData is 17 which is exact
#include <stdio.h>

//DEFINING STRUCT

struct MyData
{
	char c;
	int i;
	float f;
	double d;
};

int main(void)
{
	//variable declarations
	int iSize;
	int fSize;
	int dSize;
	int cSize;
	int struct_MyData_Size;
	
	struct MyData data; //Declaring a single struct variable of type 'struct MyData' locally
	
	//code
	//Assigning Data values too the Data members of 'struct MyData'
	data.i = 30;
	data.f = 3.14f;
	data.d = 3.5678902;
	data.c = 'A';
	
	//Displaying values of data members of Struct MyData
	printf("\nData members of STRUCT MyData are :\n");
	printf("\nValue of i = %d\n",data.i);
	printf("\nValue of f is = %f\n",data.f);
	printf("\nValue of d = %lf\n",data.d);
	printf("\nValue of c = %c\n",data.c);

	
	//calculating sizes of data memebres in struct MyData
	iSize = sizeof(data.i);
	fSize = sizeof(data.f);
	dSize = sizeof(data.d);
	cSize = sizeof(data.c);
	
	//Displaying sizes of data members is struct MyData
	printf("\nSizes in BYTES of Data members in struct MyData are :\n");
	printf("\nSize of i = %d\n", iSize);
	printf("\nSize of f = %d\n", fSize);
	printf("\nSize of d = %d\n", dSize);
	printf("\nSize of c = %d\n", cSize);
	
	//calculating size of entire MyData struct in Bytes
	struct_MyData_Size = sizeof(struct MyData);
	
	printf("\nSize of Entire MyData struct = %d (Exact size given due to the use of #pragma pack(1))\n",struct_MyData_Size);
	
	return(0);
}

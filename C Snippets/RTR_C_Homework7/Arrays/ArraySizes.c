#include <stdio.h>

int main(void)
{
	//variable declarations
	int iArray_One[5];
	int iArray_Two[5][3];
	int iArray_Three[100][100][5];
	
	int num_rows_2D = 1;
	int num_columns_2D;
	
	int num_rows_3D  = 1;
	int num_columns_3D;
	int depth_3D;
	
	//code
	printf("\nSize of 1D integer array iArray_One = %lu\n", sizeof(iArray_One));
	printf("\nNumber of elements in 1-D integer  array iArray_One = %lu\n",(sizeof(iArray_One)/ sizeof(int)));
	
	printf("\nSize of 2D integer array iArray_Two = %lu\n", sizeof(iArray_Two));
	printf("\nNumber of elements in 2-D integer  array iArray_Two = %lu\n",(sizeof(iArray_Two)/ sizeof(iArray_Two[0])));
	printf("\nNumber of elements (columns) in each row in 2-D integer array iArray_Two = %lu \n",(sizeof(iArray_Two[0])/sizeof(iArray_Two[0][0])));
	num_columns_2D = (sizeof(iArray_Two[0])/sizeof(iArray_Two[0][0]));
	printf("\nNumber of elements in total in 2-D array iArray_Two = %d\n", num_rows_2D * num_columns_2D);
	
	printf("\nSize of 3-D integer array iArray_Three = %lu \n",sizeof(iArray_Three)/sizeof(iArray_Three[0]));
	printf("\nNumber of elements (columns) in each row in 3-D integer array iArray_Three = %lu \n",(sizeof(iArray_Three[0][0])/sizeof(iArray_Three[0][0][0])));
	num_columns_3D = (sizeof(iArray_Three[0])/sizeof(iArray_Three[0][0]));
	printf("\nNumber of elements in total in 3-D array iArray_Three = %d\n", num_rows_3D * num_columns_3D);
	depth_3D = (sizeof(iArray_Three[0][0])/sizeof(iArray_Three[0][0][0]));
	printf("\nNumber of elements in total in 3-D array iArray_Three = %d\n", num_rows_3D * num_columns_3D* depth_3D);
	
	
	
	
	
	return(0);
}
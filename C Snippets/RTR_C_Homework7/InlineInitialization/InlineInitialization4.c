#include <stdio.h>

//DEFINING STRUCT
	struct MyData
	{
		int i;
		float f;
		double d;
		char c;
	};
	
	
int main(void)
{
	//variable 1 Inlne initialization
	struct MyData data = {30,4.5f, 11.451995, 'G'};//Inline initialization of struct variable 'data' of type 'struct MyData'

	//variable 2 Inlne initialization
	struct MyData data2 = {11,3.9f, 16.458989, 'G'};
	
	//variable 3 Inlne initialization
	struct MyData data3 = {21,3.96f, 16.777823, 'H'};
	
	//variable 4 Inlne initialization
	struct MyData data4 = {51,9.9f, 145.3456788, 'R'};


	
	//Displaying all inline initialized members of 'data'
	printf("\ni = %d\n",data.i);
	printf("\nf = %f\n",data.f);
	printf("\nd = %lf\n",data.d);
	printf("\nc = %c\n",data.c);
	
	//Displaying all inline initialized members of data2
	printf("\n\n");
	printf("\ni = %d\n",data2.i);
	printf("\nf = %f\n",data2.f);
	printf("\nd = %lf\n",data2.d);
	printf("\nc = %c\n",data2.c);
	
	//Displaying all inline initialized members of data3
	printf("\n\n");
	printf("\ni = %d\n",data3.i);
	printf("\nf = %f\n",data3.f);
	printf("\nd = %lf\n",data3.d);
	printf("\nc = %c\n",data3.c);
	
	//Displaying all inline initialized members of data4
	printf("\n\n");
	printf("\ni = %d\n",data4.i);
	printf("\nf = %f\n",data4.f);
	printf("\nd = %lf\n",data4.d);
	printf("\nc = %c\n",data4.c);
	
	return(0);
}
#include <stdio.h>

//DEFINING STRUCT
	struct MyData
	{
		int i;
		float f;
		double d;
		char c;
	};
	
	struct MyData data = {30,4.5f, 11.451995, 'G'};//Inline initialization of struct variable 'data' of type 'struct MyData'

int main(void)
{
	//Displaying all inline initialized members
	printf("\ni = %d\n",data.i);
	printf("\nf = %f\n",data.f);
	printf("\nd = %lf\n",data.d);
	printf("\nc = %c\n",data.c);
	return(0);
}
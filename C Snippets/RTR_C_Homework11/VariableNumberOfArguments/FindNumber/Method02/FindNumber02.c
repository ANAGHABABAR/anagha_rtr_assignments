#include<stdio.h>
#include<stdarg.h>

#define NUM_TO_BE_FOUND 3
#define NUM_ELEMENTS 10

int main()
{
	//fun prototypes
	void FindNumber(int, int, ...);
	
	//code
	FindNumber(NUM_TO_BE_FOUND,NUM_ELEMENTS,3,58,23,45,7,8);
	
	return(0);
	
}

void FindNumber(int num_to_be_found, int num, ...)
{
	
	//function prototypes
	int va_FindNumber(int,int,va_list);
	
	//variable declarations
	int count = 0;
	int n;
	va_list numbers_list;
	
	//code
	va_start(numbers_list,num);
	
	count = va_FindNumber(num_to_be_found,num,numbers_list);
	
		
		if(count == 0)
		{
			printf("NUmber %d could not be found!!",num_to_be_found);
		}
		else
		{
			printf("Number %d found %d time!!",num_to_be_found,count);
		}
		
		va_end(numbers_list);
}

int va_FindNumber(int num_to_be_found,int num, va_list list)
{
	//variable declarations
	int count_num = 0;
	int n;
	
	//code
	while(num)
	{
		n = va_arg(list,int);
		
		if(n == num_to_be_found)
		{
			count_num++;
		}
		num--;
	}
	
	return(count_num);
}
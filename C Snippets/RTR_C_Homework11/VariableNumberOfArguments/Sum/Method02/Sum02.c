#include<stdio.h>
#include<stdarg.h>

int main()
{
	//function prototypes
	int CalculateSum(int, ...);
	
	//variabl declarations
	int answer;
	
	//code
	printf("\n\n");
	answer = CalculateSum(1, 10, 20, 30, 40, 50);
	printf("Answer = %d\n\n",answer);
	
	answer = CalculateSum(1,1,1,1,1,1,1,1,10);
	printf("Answer = %d\n\n",answer);
	
	answer = CalculateSum(0);
	printf("Answer = %d\n\n",answer);
	
	return(0);
}

int CalculateSum(int num, ...)//variadic function
{
	//function prototype
	int va_CalculateSum(int, va_list);
	
	//var declarations
	int sum = 0;
	int n;
	va_list numbers_list;
	
	//code
	va_start(numbers_list,num);
	
	sum = va_CalculateSum(num,numbers_list);
	
	va_end(numbers_list);
	return(sum);
}

int va_CalculateSum(int num,va_list list)
{
	//variable declarations
	int n, sum =0;
	
	//code
	while(num)
	{
		n = va_arg(list,int);
		sum = sum + n;
		num--;
	}
	return(sum);
}
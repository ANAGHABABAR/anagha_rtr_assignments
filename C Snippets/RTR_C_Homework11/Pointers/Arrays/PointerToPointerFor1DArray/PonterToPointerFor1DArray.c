#include<stdio.h>
#include<stdlib.h>

int main()
{
	void MyAlloc(int **ptr, unsigned int numOfElements);
	
	//variable declarations
	int *piArray = NULL;
	unsigned int numOfElements;
	int i;
	
	//code
	printf("\n\nHow many elements you want in an integer array ?\n");
	scanf("%d",&numOfElements);
	
	MyAlloc(&piArray, numOfElements);
	
	printf("\nEnter %u elements in an integer array:\n",numOfElements);
	for(i = 0; i < numOfElements; i++)
	{
		scanf("%d",&piArray[i]);
	}
	
	printf("\nNumbers entered by you are:\n");
	for(i = 0; i < numOfElements; i++)
	{
		printf("\n%d",piArray[i]);
	}
	
	if(piArray)
	{
		free(piArray);
		piArray = NULL;
		printf("\nFreed memory for piArray successfully!\n");
	}

	return(0);
}

void MyAlloc(int **ptr, unsigned int num_elements)
{
	//code
	*ptr = (int *)malloc(sizeof(int) * num_elements);
	if(*ptr == NULL)
	{
		printf("\nMemory allocation for *ptr failed..xiting\n");
		exit(0);
	}
	
	printf("\nMemory for *ptr has been successfully allocated..");
}
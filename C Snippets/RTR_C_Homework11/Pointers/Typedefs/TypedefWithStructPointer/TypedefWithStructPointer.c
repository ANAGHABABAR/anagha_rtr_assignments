#include<stdio.h>

//Defining struct 
struct MyData
{
	int i;
	float f;
	double d;
};

int main()
{
	//variable declarations
	int iSize;
	int fSize;
	int dSize;
	int struct_MyData_size;
	int ptr_to_struct_MyData_Size;
	
	typedef struct MyData * MyDataPtr;
	MyDataPtr pData;
	
	//code
	printf("\n\n");
	pData = (MyDataPtr)malloc(sizeof(struct MyData));
	if(pData == NULL)
	{
		printf("\nMemory allocation failure for pData. Hence, exiting\n");
		exit(0);
	}
	
	pData->i = 11;
	pData->f = 0.44f;
	pData->d = 11.36273;
	
	printf("\nDisplaying values\n");
	printf("i = %d",pData->i);
	printf("f = %f",pData->f);
	printf("d = %lf",pData->d);
	
	//calculating sizes
	iSize = sizeof(pData->i);
	fSize = sizeof(pData->f);
	dSize = sizeof(pData->d);
	struct_MyData_size = sizeof(struct MyData);
	ptr_to_struct_MyData_Size = sizeof(MyDataPtr);
	
	printf("\n\nDisplaying sizes : \n");
	printf("\niSize = %d\n",iSize);
	printf("\nfSize = %d\n",fSize);
	printf("\ndSize = %d\n",dSize);
	printf("\n struct_MyData_size = %d\n",struct_MyData_size);
	printf("\n ptr_to_struct_MyData_Size= %d\n",ptr_to_struct_MyData_Size);

	if(pData)
	{
		free(pData);
		pData = NULL;
		printf("\nMemory for pData freed successfully\n");
	}
	
	
}
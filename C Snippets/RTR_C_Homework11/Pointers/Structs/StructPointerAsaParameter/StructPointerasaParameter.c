#include<stdio.h>

//defining struct

struct MyData
{
	int i;
	float f;
	double d;
};

int main()
{
	//function prototypes
	void ChangeValues(struct MyData *);
	
	//variable declarations
	struct MyData *pData = NULL;
	
	//code
	printf("\n\n");
	
	pData = (struct MyData *)malloc(sizeof(struct MyData));
	if(pData == NULL)
	{
		printf("\nMemory allocation for pData failed. Hence, exiting..\n");
		exit(0);
	}
	
	pData->i = 30;
	pData->f = 3.23f;
	pData->d = 30.687678;
	
	printf("\n\n");
	printf("\ni = %d\n",pData->i);
	printf("\nf = %f\n",pData->f);
	printf("\nd = %lf\n",pData->d);
	
	ChangeValues(pData);
	
	printf("\n\n");
	printf("\ni = %d\n",pData->i);
	printf("\nf = %f\n",pData->f);
	printf("\nd = %lf\n",pData->d);
	
	if(pData)
	{
		free(pData);
		pData = NULL;
		printf("\nMemory deallocated for pData successfully.");
	}
}

void ChangeValues(struct MyData *pParam_Data)
{
	//code
	pParam_Data -> i = 9;
	pParam_Data -> f = 56.78f;
	pParam_Data -> d = 990.232323;
	
	//can also do
	//(*pParam_Data).i = 9;
	//(*pParam_Data).f = 56.78f;
	//(*pParam_Data).d = 990.787798;
	
}
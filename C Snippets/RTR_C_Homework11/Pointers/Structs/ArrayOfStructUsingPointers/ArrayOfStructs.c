#include<stdio.h>
#include<ctype.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char gender;
	float salary;
	char marital_status;
};

int main()
{
	//function prototype
	void MyGetString(char [],int);
	
	//variable declarations
	struct Employee *pEmployeeRecord = NULL;
	int num_employees, i;
	
	//code
	printf("\n\n");
	printf("\nEnter the number of Employees: \n");
	scanf("%d",&num_employees);
	
	pEmployeeRecord = (struct Employee *)malloc(sizeof(struct Employee));
	if(pEmployeeRecord == NULL)
	{
		printf("\nMemory allocation for pEmployeeRecord failed. Hence, exiting!\n");
		exit(0);
	}
	
	for(i = 0; i < num_employees; i++)
	{
		printf("\n*******DATA ENTRY FOR EMPLOYEE NUMBER %d********\n",i);
		printf("\nEnter employee name : \n");
		MyGetString(pEmployeeRecord[i].name, NAME_LENGTH);
		
		printf("\n\n");
		printf("Enter Employee's age:\n");
		scanf("%d",&pEmployeeRecord[i].age);
		
		printf("\n\n");
		printf("\nEnter Employee's salary:\n");
		scanf("%f",&pEmployeeRecord[i].salary);
		
		printf("\n\n");
		printf("Enter Employee's gender. (M/m for Male and F/f for Female)\n");
		pEmployeeRecord[i].gender = getch();
		printf("\n%c",pEmployeeRecord[i].gender);
		
		printf("\n\n");
		printf("Enter Employee's gender. (Y/y for Married and N/n for Unmarried)\n");
		pEmployeeRecord[i].marital_status = getch();
		printf("\n%c",pEmployeeRecord[i].marital_status);
		
	}
	
	//diaplaying employees information
	for(i = 0; i < num_employees; i++)
	{
		printf("\n*****Disaplying Employee Information*****\n");
		printf("\n\n");
		printf("\nName : %s\n",pEmployeeRecord[i].name);
		printf("\nAge : %d\n",pEmployeeRecord[i].age);
		printf("\nSalary : %f\n",pEmployeeRecord[i].salary);
		if(pEmployeeRecord[i].gender == 'F' || pEmployeeRecord[i].gender == 'f')
		{
			printf("\nGender : Female");
		}
		else if(pEmployeeRecord[i].gender == 'M' || pEmployeeRecord[i].gender == 'm')
		{
			printf("\nGender : Male");
		}
		
		if(pEmployeeRecord[i].marital_status == 'Y' || pEmployeeRecord[i].marital_status == 'y')
		{
			printf("\nMarital Status : Married");
		}
		else if(pEmployeeRecord[i].marital_status == 'N' || pEmployeeRecord[i].marital_status == 'n')
		{
			printf("\nMarital status : Unmarried");
		}
		
	}
	
	if(pEmployeeRecord)
	{
		free(pEmployeeRecord);
		pEmployeeRecord = NULL;
		printf("\nMemory allocated to pEmployeeRecord is freed..\n");
	}
	
	return(0);
}

void MyGetString(char str[], int strSize)
{
	//variable declarations
	int i;
	char ch = '\0';
	
	//code
	i = 0;
	do
	{
		ch = getch();
		str[i] = ch;
		printf("%c",str[i]);
		i++;
	}while((ch != '\r') && (i < strSize));
	
	if(i == strSize)
	{
		str[i - 1] = '\0';
	}
	else
	{
		str[i] = '\0';
	}
}
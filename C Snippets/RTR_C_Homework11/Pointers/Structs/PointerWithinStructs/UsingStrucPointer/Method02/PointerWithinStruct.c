#include<stdio.h>

//Defining struct
struct MyData
{
	int *iptr;
	int i;
	
	float *fptr;
	float f;
	
	double *dptr;
	double d;
};

int main()
{
	//variable declarations
	struct MyData *pData = NULL;
	
	//code
	printf("\n\n");
	pData = (struct MyData *)malloc(sizeof(struct MyData));
	
	if(pData == NULL)
	{
		printf("\n\nMemory allocation for pData failed.Hence exiting..");
		exit(0);
	}
	
	pData->i = 9;
	pData->iptr = &(pData->i);
	
	pData->f = 9.89f;
	pData->fptr = &(pData->f);
	
	pData->d = 9.897979;
	pData->dptr = &(pData->d);

	printf("\n\n");
	printf("i = %d\n", *(pData->iptr));
	printf("\nAddress of 'i' = %p\n",pData->iptr);

	printf("\n\n");
	printf("f = %f\n", *(pData->fptr));
	printf("\nAddress of 'f' = %p\n",pData->fptr);

	printf("\n\n");
	printf("d = %lf\n", *(pData->dptr));
	printf("\nAddress of 'd' = %p\n",pData->dptr);	
		
	if(pData)
	{
		free(pData);
		pData = NULL;
		printf("\nMemory for pData has been freed\n");
	}
	
	return(0);
}
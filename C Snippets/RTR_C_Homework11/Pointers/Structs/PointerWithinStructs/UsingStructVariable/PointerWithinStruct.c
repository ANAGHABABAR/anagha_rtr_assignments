#include<stdio.h>

//Defining struct
struct MyData
{
	int *iptr;
	int i;
	
	float *fptr;
	float f;
	
	double *dptr;
	double d;
};

int main()
{
	//variable declarations
	struct MyData data;
	
	//code
	data.i = 9;
	data.iptr = &data.i;
	
	data.f = 3.14f;
	data.fptr = &data.f;
	
	data.d = 3.432543;
	data.dptr = &data.d;
	
	printf("\n\n");
	printf("i = %d\n",*(data.iptr));
	printf("\nAddress of 'i' = %p\n",data.iptr);
	
	printf("\n\n");
	printf("f = %f\n",*(data.fptr));
	printf("\nAddress of 'f' = %p\n",data.fptr);
	
	printf("\n\n");
	printf("d = %lf\n",*(data.dptr));
	printf("\nAddress of 'd' = %p\n",data.dptr);

	return(0);
	
}
#include<stdio.h>

//Defining structs
struct MyData
{
	int i;
	float f;
	double d;
};

int main()
{
	//variable declarations
	int iSize;
	int fSize;
	int dSize;
	int struct_MyData_size;
	int ptr_to_struct_MyData_size;
	
	struct MyData *pData = NULL;
	
	//code
	pData = (struct MyData *)malloc(sizeof(struct MyData));
	if(pData == NULL)
	{
		printf("\nFailure in allocating memory to struct MyData\n");
		exit(0);
	}
	
	//Assigning data values to MyData struct members
	pData->i = 11;
	pData->f = 21.67f;
	pData->d = 11.1234566;

	//displaying above values
	printf("Data members of Struct MyData are:\n");
	printf("\ni = %d",pData->i);
	printf("\nf = %f",pData->f);
	printf("\nd = %lf",pData->d);
	
	//calculating the sizes of data memebers of struct Mydata
	iSize = sizeof(pData->i);
	fSize = sizeof(pData->f);
	dSize = sizeof(pData->d);
	
	printf("\niSize = %d",iSize);
	printf("\nfSize = %d",fSize);
	printf("\ndSize = %d",dSize);
	
	struct_MyData_size = sizeof(struct MyData);
	ptr_to_struct_MyData_size = sizeof(struct MyData *);
	
	printf("\nstruct_MyData_size = %d",struct_MyData_size);
	printf("\nptr_to_struct_MyData_size = %d",ptr_to_struct_MyData_size);
	
	if(pData)
	{
		free(pData);
		pData = NULL;
		printf("\nMemory for pData freed scuccessfully..");
	}
	return(0);
}
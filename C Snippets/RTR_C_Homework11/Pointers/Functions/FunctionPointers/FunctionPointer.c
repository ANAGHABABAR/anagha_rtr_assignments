#include<stdio.h>

int main()
{
	//fun declarations
	int AddIntegers(int, int);
	int SubtractIntegers(int, int);
	float AddFloats(float, float);
	
	//variable declarations
	typedef int (*AddIntsFnPtr)(int, int);
	AddIntsFnPtr ptrAddTwoIntegers = NULL;
	AddIntsFnPtr ptrFunc = NULL;
	
	typedef float (*AddFloatsFnPtr)(float, float);
	AddFloatsFnPtr ptrAddTwoFloats = NULL;
	
	int iAnswer =0 ;
	float fAnswer = 0.0f;
	
	//code
	ptrAddTwoIntegers = AddIntegers;
	iAnswer = ptrAddTwoIntegers(10,20);
	printf("\n\nSum of Integers is : %d",iAnswer);
	
	ptrFunc = SubtractIntegers;
	iAnswer = ptrFunc(30,20);
	printf("\n\nDiffrence of Integers is : %d",iAnswer);
	
	ptrAddTwoFloats = AddFloats;
	fAnswer = ptrAddTwoFloats(20.5f,20.0f);
	printf("\n\nSum of Floats is : %f",fAnswer);
	
	return(0);
}

int AddIntegers(int a, int b)
{
	return(a+b);
}

int SubtractIntegers(int a,int b)
{
	if(a > b)
		return(a - b);
	else
		return(b - a);
}

float AddFloats(float x, float y)
{
	return(x + y);
}
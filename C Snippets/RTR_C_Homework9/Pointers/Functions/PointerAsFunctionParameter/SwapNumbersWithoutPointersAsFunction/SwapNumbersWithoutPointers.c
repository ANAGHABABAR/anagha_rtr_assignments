#include<stdio.h>

int main(void)
{
	//function declarations
	void SwapNumbers(int, int);
	
	//variable declarations
	int a,b;
	
	//code
	printf("\n\n");
	printf("Enter a value of A: \n");
	scanf("%d",&a);
	
	printf("\n\n");
	printf("Enter a value of B: \n");
	scanf("%d",&b);
	
	printf("\n\n");
	printf("\n***Before Swapping the numbers of A and B***\n");
	printf("\nA : %d,\t B: %d\n",a,b);
	
	SwapNumbers(a,b);//Arguments passed by value
	
	printf("\n***After Swapping the numbers of A and B***\n");
	printf("\nA : %d,\t B: %d\n",a,b);

	return(0);
}

void SwapNumbers(int x, int y)
{
	//variable declarations
	int temp;
	
	//code
	printf("\n\n");
	printf("\n***Before Swapping the numbers of X and Y***\n");
	printf("\nX : %d,\t Y: %d\n",x,y);
	
	temp = x;
	x = y;
	y = temp;
	
	printf("\n\n");
	printf("\n***After Swapping the numbers of X and Y ***\n");
	printf("\nX : %d,\t Y: %d\n",x,y);
	
}
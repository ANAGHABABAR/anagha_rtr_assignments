#include<stdio.h>

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

int main()
{
	//function declarations
	int Difference(int, int, int *);
	
	//variable declarations
	int a, b;
	int answer, ret;
	
	//code
	printf("\n\n");
	printf("Enter a value of A: \n");
	scanf("%d",&a);
	
	printf("\n\n");
	printf("Enter a value of B: \n");
	scanf("%d",&b);
	
	ret = Difference(a,b,&answer);
	
	printf("\n\n");
	printf("Difference between %d and %d is %d",a,b,answer);
	
	if(ret == POSITIVE)
	{
		printf("The difference is POSITIVE");
	}
	else if(ret == NEGATIVE)
	{
		printf("The difference is NEGATIVE");
	}
	else
	{
		printf("The Difference is ZERO");
	}
	
	return(0);
}


int Difference(int x, int y, int *diff)
{
	//code
	*diff = x - y;
	
	if(*diff > 0)
	{
		return(POSITIVE);
	}
	else if(*diff < 0)
	{
		return(NEGATIVE);
	}
	else
	{
		return(ZERO);
	}
}
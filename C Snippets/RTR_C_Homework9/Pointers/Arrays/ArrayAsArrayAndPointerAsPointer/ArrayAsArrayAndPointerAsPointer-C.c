#include<stdio.h>

int main(void)
{
	//variable declaratiosn
	int iArray[] = {10,20,30,40,50,60,70,80,90,100};
	int *ptr_iArray = NULL;
	
	//code
	printf("\n\n");
	printf("\nValue of xth Element of iArray : iArray[x] AND Address of xth Element of iArray\n");
	
	printf("\nInteger elements and their addresses: \n");
	printf("\niArray[0] = %d \t\tAt address &iArray[0] : %p\n",iArray[0],&iArray[0]);
	printf("\niArray[1] = %d \t\tAt address &iArray[1] : %p\n",iArray[1],&iArray[1]);
	printf("\niArray[2] = %d \t\tAt address &iArray[2] : %p\n",iArray[2],&iArray[2]);
	printf("\niArray[3] = %d \t\tAt address &iArray[3] : %p\n",iArray[3],&iArray[3]);
	printf("\niArray[4] = %d \t\tAt address &iArray[4] : %p\n",iArray[4],&iArray[4]);
	printf("\niArray[5] = %d \t\tAt address &iArray[5] : %p\n",iArray[5],&iArray[5]);
	printf("\niArray[6] = %d \t\tAt address &iArray[6] : %p\n",iArray[6],&iArray[6]);
	printf("\niArray[7] = %d \t\tAt address &iArray[7] : %p\n",iArray[7],&iArray[7]);
	printf("\niArray[8] = %d \t\tAt address &iArray[8] : %p\n",iArray[8],&iArray[8]);
	printf("\niArray[9] = %d \tAt address &iArray[9] : %p\n",iArray[9],&iArray[9]);
	
	//Assigning base address of Integer array 'iArray' To Integer Pointer
	//NAME OF AN ARRAY IS ITS OWN BASE ADDRESS
	ptr_iArray = iArray; //Same as ptr_iArray = &iArray[0]
	
	//Using Pointer As Pointer i.e. : Value of xth Element of iArray : *(ptr_iArray + x) AND Address of xth Element of iArray : (ptr_iArray + x)
	printf("\n\n");
	printf("***Using Pointer As Pointer i.e : Value of xth Element of iArray : *(ptr_iArray + x) AND Address of xth Element of iArray : (ptr_iArray + x)\n\n");
	
	printf("*(ptr_iArray + 0) = %d \t\t At address (ptr_iArray + 0) : %p\n",*(ptr_iArray + 0),(ptr_iArray + 0));
	printf("*(ptr_iArray + 1) = %d \t\t At address (ptr_iArray + 1) : %p\n",*(ptr_iArray + 1),(ptr_iArray + 1));
	printf("*(ptr_iArray + 2) = %d \t\t At address (ptr_iArray + 2) : %p\n",*(ptr_iArray + 2),(ptr_iArray + 2));
	printf("*(ptr_iArray + 3) = %d \t\t At address (ptr_iArray + 3) : %p\n",*(ptr_iArray + 3),(ptr_iArray + 3));
	printf("*(ptr_iArray + 4) = %d \t\t At address (ptr_iArray + 4) : %p\n",*(ptr_iArray + 4),(ptr_iArray + 4));
	printf("*(ptr_iArray + 5) = %d \t\t At address (ptr_iArray + 5) : %p\n",*(ptr_iArray + 5),(ptr_iArray + 5));
	printf("*(ptr_iArray + 6) = %d \t\t At address (ptr_iArray + 6) : %p\n",*(ptr_iArray + 6),(ptr_iArray + 6));
	printf("*(ptr_iArray + 7) = %d \t\t At address (ptr_iArray + 7) : %p\n",*(ptr_iArray + 7),(ptr_iArray + 7));
	printf("*(ptr_iArray + 8) = %d \t\t At address (ptr_iArray + 8) : %p\n",*(ptr_iArray + 8),(ptr_iArray + 8));
	printf("*(ptr_iArray + 9) = %d \t At address (ptr_iArray + 9) : %p\n",*(ptr_iArray + 9),(ptr_iArray + 9));
	
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[10];
	int *ptr_iArray = NULL;
	int i;
	
	//code
	for(i = 0;i < 10; i++)
	{
		iArray[i] = (i + 1) * 5;
	}
	
	//Name of an array is its base address
	//Hence, 'iArray' is the base address of ARRAY iArray[] OR 'iArray' is the address of element iArray[0]
	//** Assigning base address of array 'iArray[]' to INTEGER POINTER *ptr_iArray
	
	ptr_iArray = iArray; //ptr_iArray = &iArray[0]

	printf("\n\n");
	printf("Elements of Integer array : \n\n");
	for(i = 0;i < 10; i++)
	{
		printf("iArray[%d] = %d\n",i,*(ptr_iArray + i));
	}
	
	printf("\n\n");
	printf("\nElements of the Integer array :\n\n");
	for(i = 0; i < 10; i++)
	{
		printf("\niArray[%d] = %d\t\t Address = %p",i,*(ptr_iArray + i),(ptr_iArray + i));
	}
	
	printf("\n\n");
	
	return(0);
}
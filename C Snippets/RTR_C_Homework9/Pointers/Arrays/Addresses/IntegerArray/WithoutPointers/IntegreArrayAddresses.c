#include<stdio.h>

int main()
{
	//variable declarations
	int iArray[10];
	int i;
	
	//code
	for(i = 0; i < 10; i++)
	{
		iArray[i] = (i+1) * 3;
	}
	
	printf("\n\n");
	printf("\nElements of Integer Array: \n");
	for(i = 0; i < 10; i++)
	{
		printf("\niArray[%d] = %d\t\t Address = %p\n",i,iArray[i],&iArray[i]);
	}
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	double dArray[10];
	double *ptr_dArray = NULL;
	int i;
	
	//code
	for(i = 0;i < 10; i++)
	{
		dArray[i] = (float)(i + 1) * 1.7879998f;
	}
	
	//Name of an array is its base address
	//Hence, 'dArray' is the base address of ARRAY dArray[] OR 'dArray' is the address of element dArray[0]
	//** Assigning base address of array 'dArray[]' to DOUBLE POINTER *ptr_dArray
	
	ptr_dArray = dArray; //ptr_dArray = &dArray[0]

	printf("\n\n");
	printf("Elements of Double array : \n\n");
	for(i = 0;i < 10; i++)
	{
		printf("dArray[%d] = %f\n",i,*(ptr_dArray + i));
	}
	
	printf("\n\n");
	printf("\nElements of the Double array :\n\n");
	for(i = 0; i < 10; i++)
	{
		printf("\ndArray[%d] = %f\t\t Address = %p",i,*(ptr_dArray + i),(ptr_dArray + i));
	}
	
	printf("\n\n");
	
	return(0);
}
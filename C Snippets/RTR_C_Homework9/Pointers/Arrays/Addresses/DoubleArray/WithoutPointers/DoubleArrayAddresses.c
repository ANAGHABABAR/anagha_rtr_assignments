#include<stdio.h>

int main()
{
	//variable declarations
	double dArray[10];
	int i;
	
	//code
	for(i = 0; i < 10; i++)
	{
		dArray[i] = (float)(i + 1) * 1.333333f;
	}
	
	printf("\n\n");
	printf("\nElements of Double Array: \n");
	for(i = 0; i < 10; i++)
	{
		printf("\ndArray[%d] = %lf\t\t Address = %p\n",i,dArray[i],&dArray[i]);
	}
	
	printf("\n\n");
	
	return(0);
}
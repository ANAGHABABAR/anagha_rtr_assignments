#include<stdio.h>

int main()
{
	//variable declarations
	float fArray[10];
	int f;
	
	//code
	for(f = 0; f < 10; f++)
	{
		fArray[f] = (float)(f + 1) * 1.5f;
	}
	
	printf("\n\n");
	printf("\nElements of Float Array: \n");
	for(f = 0; f < 10; f++)
	{
		printf("\nfArray[%d] = %f\t\t Address = %p\n",f,fArray[f],&fArray[f]);
	}
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	float fArray[10];
	float *ptr_fArray = NULL;
	int i;
	
	//code
	for(i = 0;i < 10; i++)
	{
		fArray[i] = (float)(i + 1) * 1.3f;
	}
	
	//Name of an array is its base address
	//Hence, 'fArray' is the base address of ARRAY fArray[] OR 'fArray' is the address of element fArray[0]
	//** Assigning base address of array 'fArray[]' to Float POINTER *ptr_fArray
	
	ptr_fArray = fArray; //ptr_fArray = &fArray[0]

	printf("\n\n");
	printf("Elements of Float array : \n\n");
	for(i = 0;i < 10; i++)
	{
		printf("fArray[%d] = %f\n",i,*(ptr_fArray + i));
	}
	
	printf("\n\n");
	printf("\nElements of the Integer array :\n\n");
	for(i = 0; i < 10; i++)
	{
		printf("\nfArray[%d] = %f\t\t Address = %p",i,*(ptr_fArray + i),(ptr_fArray + i));
	}
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	char cArray[10];
	char *ptr_cArray =NULL;
	int i;
	
	//code
	for(i = 0; i < 10; i++)
	{
		cArray[i] = (char)(i + 65);
	}
	
	//Name of an Array is its Base address 
	//Hence, cArray is the base address of array 'cArray[]' or cArray is the address of element cArray[0]
	//Assigning base address of an array 'cArray' to Char pointer
	
	ptr_cArray = cArray;//ptr_cArray = &cArray[0];
	
	printf("\n\n");
	printf("Elements of character array : \n");
	for(i = 0; i < 10; i++)
	{
		printf("cArray[%d] = %c\t\t Address = %p\n",i,*(ptr_cArray + i),(ptr_cArray + i));
	}
	
	printf("\n\n");
	
	return(0);
}
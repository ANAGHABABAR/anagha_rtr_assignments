#include<stdio.h>

int main()
{
	//variable declarations
	int iArray[] = {12, 24, 36, 48, 60, 72, 84, 96, 108, 120};
	float fArray[] = {9.8f, 8.7f, 7.6f, 6.5f, 5.4f, 4.3f, 3.2f, 2.1f, 1.0f};
	double dArray[] = {1.222222, 2.333333, 3.4444444};
	char cArray[] = {'A','S','T','R','O','M','E','D','I','C','O','M','P','\0'};
	
	//code
	printf("\n\n");
	printf("\nInteger elements stored at respective addresses are as follows: \n\n");
	printf("\n iArray[0] = %d at Address : %p",*(iArray + 0),(iArray + 0));
	printf("\n iArray[1] = %d at Address : %p",*(iArray + 1),(iArray + 1));
	printf("\n iArray[2] = %d at Address : %p",*(iArray + 2),(iArray + 2));
	printf("\n iArray[3] = %d at Address : %p",*(iArray + 3),(iArray + 3));
	printf("\n iArray[4] = %d at Address : %p",*(iArray + 4),(iArray + 4));
	printf("\n iArray[5] = %d at Address : %p",*(iArray + 5),(iArray + 5));
	printf("\n iArray[6] = %d at Address : %p",*(iArray + 6),(iArray + 6));
	printf("\n iArray[7] = %d at Address : %p",*(iArray + 7),(iArray + 7));
	printf("\n iArray[8] = %d at Address : %p",*(iArray + 8),(iArray + 8));
	printf("\n iArray[9] = %d at Address : %p",*(iArray + 9),(iArray + 9));
	
	printf("\n\n");
	printf("\nFloat elements stored at respective addresses are as follows: \n\n");
	printf("\nfArray[0] = %f is stored at address : %p",*(fArray + 0),(fArray + 0));
	printf("\nfArray[1] = %f is stored at address : %p",*(fArray + 1),(fArray + 1));
	printf("\nfArray[2] = %f is stored at address : %p",*(fArray + 2),(fArray + 2));
	printf("\nfArray[3] = %f is stored at address : %p",*(fArray + 3),(fArray + 3));
	printf("\nfArray[4] = %f is stored at address : %p",*(fArray + 4),(fArray + 4));
	printf("\nfArray[5] = %f is stored at address : %p",*(fArray + 5),(fArray + 5));
	printf("\nfArray[6] = %f is stored at address : %p",*(fArray + 6),(fArray + 6));
	printf("\nfArray[7] = %f is stored at address : %p",*(fArray + 7),(fArray + 7));
	printf("\nfArray[8] = %f is stored at address : %p",*(fArray + 8),(fArray + 8));
	
	printf("\n\n");
	printf("\nDouble elements stored at respective addresses are as follows: \n\n");
	printf("\ndArray[0] = %lf is stored at address : %p",*(dArray + 0),(dArray + 0));
	printf("\ndArray[1] = %lf is stored at address : %p",*(dArray + 1),(dArray + 1));
	printf("\ndArray[2] = %lf is stored at address : %p",*(dArray + 2),(dArray + 2));
	
	printf("\n\n");
	printf("\nChar elements stored at respective addresses are as follows: \n\n");
	printf("\ncArray[0] = %c is stored at address : %p",*(cArray + 0),(cArray + 0));
	printf("\ncArray[1] = %c is stored at address : %p",*(cArray + 1),(cArray + 1));
	printf("\ncArray[2] = %c is stored at address : %p",*(cArray + 2),(cArray + 2));
	printf("\ncArray[3] = %c is stored at address : %p",*(cArray + 3),(cArray + 3));
	printf("\ncArray[4] = %c is stored at address : %p",*(cArray + 4),(cArray + 4));
	printf("\ncArray[5] = %c is stored at address : %p",*(cArray + 5),(cArray + 5));
	printf("\ncArray[6] = %c is stored at address : %p",*(cArray + 6),(cArray + 6));
	printf("\ncArray[7] = %c is stored at address : %p",*(cArray + 7),(cArray + 7));
	printf("\ncArray[8] = %c is stored at address : %p",*(cArray + 8),(cArray + 8));
	printf("\ncArray[9] = %c is stored at address : %p",*(cArray + 9),(cArray + 9));
	printf("\ncArray[10] = %c is stored at address : %p",*(cArray + 10),(cArray + 10));
	printf("\ncArray[11] = %c is stored at address : %p",*(cArray + 11),(cArray + 11));
	printf("\ncArray[12] = %c is stored at address : %p",*(cArray + 12),(cArray + 12));
	printf("\ncArray[13] = %c is stored at address : %p",*(cArray + 13),(cArray + 13));
	

	return(0);
}
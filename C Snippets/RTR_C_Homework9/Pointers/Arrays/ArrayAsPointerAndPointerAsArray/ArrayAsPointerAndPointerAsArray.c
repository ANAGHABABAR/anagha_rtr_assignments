#include<stdio.h>

int main()
{
	//variable declarations
	int iArray[] = {10,20,30,40,50,60,70,80,90,100};
	int *ptr_iArray = NULL;//Integer pointer
	
	//code
	//Using array name as a Pointer i.e.Value of xth Element of iArray : *(iArray + x) AND Address of xth Element of iArray : (iArray + x)
	
	printf("\n\n");
	printf("\nUsing array name as a Pointer i.e.Value of xth Element of iArray : *(iArray + x) AND Address of xth Element of iArray : (iArray + x)\n\n");
	
	printf("Integer array elements and their addresses :\n\n");
	printf("\n*(iArray + 0) = %d\t At Address (iArray + 0) : %p\n",*(iArray +0),(iArray + 0));
	printf("\n*(iArray + 1) = %d\t At Address (iArray + 1) : %p\n",*(iArray +1),(iArray + 1));
	printf("\n*(iArray + 2) = %d\t At Address (iArray + 2) : %p\n",*(iArray +2),(iArray + 2));
	printf("\n*(iArray + 3) = %d\t At Address (iArray + 3) : %p\n",*(iArray +3),(iArray + 3));
	printf("\n*(iArray + 4) = %d\t At Address (iArray + 4) : %p\n",*(iArray +4),(iArray + 4));
	printf("\n*(iArray + 5) = %d\t At Address (iArray + 5) : %p\n",*(iArray +5),(iArray + 5));
	printf("\n*(iArray + 6) = %d\t At Address (iArray + 6) : %p\n",*(iArray +6),(iArray + 6));
	printf("\n*(iArray + 7) = %d\t At Address (iArray + 7) : %p\n",*(iArray +7),(iArray + 7));
	printf("\n*(iArray + 8) = %d\t At Address (iArray + 8) : %p\n",*(iArray +8),(iArray + 8));
	printf("\n*(iArray + 9) = %d\t At Address (iArray + 9) : %p\n",*(iArray +9),(iArray + 9));
	
	//Assigning base address of integer array 'iArray' to Integre pointer 'ptr_iArray'
	//Name of any array is its own base address
	ptr_iArray = iArray;// same as ptr_iArray = &iArray[0];
	
	//Using pointer as an array name i.e. : Value of xth Element of iArray : ptr_iArray[x] AND address of xth Element of iArray : &ptr_iArray[x]
	printf("\nUsing pointer as an array name i.e. : Value of xth Element of iArray : ptr_iArray[x] AND address of xth Element of iArray : &ptr_iArray[x]\n\n");
	printf("\nInteger array elements and their addresses are : \n\n");
	printf("\nptr_iArray[0] = %d \t at address &ptr_iArray[0] : %p\n",ptr_iArray[0],&ptr_iArray[0]);
	printf("\nptr_iArray[1] = %d \t at address &ptr_iArray[1]: %p\n",ptr_iArray[1],&ptr_iArray[1]);
	printf("\nptr_iArray[2] = %d \t at address &ptr_iArray[2]: %p\n",ptr_iArray[2],&ptr_iArray[2]);
	printf("\nptr_iArray[3] = %d \t at address &ptr_iArray[3]: %p\n",ptr_iArray[3],&ptr_iArray[3]);
	printf("\nptr_iArray[4] = %d \t at address &ptr_iArray[4]: %p\n",ptr_iArray[4],&ptr_iArray[4]);
	printf("\nptr_iArray[5] = %d \t at address &ptr_iArray[5]: %p\n",ptr_iArray[5],&ptr_iArray[5]);
	printf("\nptr_iArray[6] = %d \t at address &ptr_iArray[6]: %p\n",ptr_iArray[6],&ptr_iArray[6]);
	printf("\nptr_iArray[7] = %d \t at address &ptr_iArray[7]: %p\n",ptr_iArray[7],&ptr_iArray[7]);
	printf("\nptr_iArray[8] = %d \t at address &ptr_iArray[8]: %p\n",ptr_iArray[8],&ptr_iArray[8]);
	printf("\nptr_iArray[9] = %d \t at address &ptr_iArray[9]: %p\n",ptr_iArray[9],&ptr_iArray[9]);
	
	
	return(0);
}
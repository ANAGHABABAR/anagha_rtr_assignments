#include<stdio.h>

struct Employee
{
	char name[100];
	int age;
	float salary;
	char gender;
	char marital_status;
};

int main(void)
{
	//code
	printf("\n\n");
	printf("Sizes of Data Types and Pointers of respective Data Types are : ");
	printf("\nSize of (int) = %d,\t size of (int*) = %d\n",sizeof(int),sizeof(int*));
	printf("\nSize of (float) = %d,\t size of (float*) = %d\n",sizeof(float),sizeof(float*));
	printf("\nSize of (double) = %d,\t size of (double*) = %d\n",sizeof(double),sizeof(double*));
	printf("\nSize of (char) = %d,\t size of (char*) = %d\n",sizeof(char),sizeof(char*));
	printf("\nSize of (struct Employee) = %d,\t Size of (struct Employee *)= %d\n",sizeof(struct Employee),sizeof(struct Employee *));
	
	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	int num;
	int *ptr;
	int ans;
	
	//code
	num = 5;
	ptr = &num;
	
	printf("\n\n");
	printf("\nnum = %d",num);
	printf("\n&num = %p",&num);
	printf("\n*(&num) = %d",*(&num));
	printf("\nptr = %p",ptr);
	printf("\n*ptr = %d",*ptr);

	printf("\n\n");
	
	//Add 10 to pointer which is address of 'num'
	//Hence, 10 will be addded to the address of 'num' and the resultant address will be displayed
	printf("\nAnswer of (ptr+10) = %p",(ptr+10));

	//Add 10 to 'ptr' which is the address on 'num' and give value at the new address
	//Hence 10 will be added to the address of 'num' and the value at resultant address will be displayed
	printf("\nAnswer of *(ptr + 10) = %d",*(ptr + 10));
	
	//Add 10 to *ptr  which is the value at the address of 'num' (num i.e. 5) and give new value . Without any change in any address
	//Hence, 10 will be added to the *ptr (num = 5) and the resultant value will be given (*ptr + 10) = (num + 10) = (5 + 10) = 15
	printf("\nAnswer of (*ptr + 10) = %d",(*ptr + 10));
	
	//******Associativity of * (Value at Adrress) AND ++ AND -- OPERATORS Is from RIGHT TO LEFT
	
	//(RIGHT TO LEFT) Consider value *ptr ..Pre-increment *ptr ..That is value at address 'ptr' i.e. *ptr is pre-incremented (++*ptr)
	++*ptr;//after execution of this statement *ptr = 6
	printf("\nAnswer of ++*ptr = %d",*ptr);
	
	//(RIGHT TO LEFT) Post-increment address ptr ,That is, address 'ptr' i.e. ptr is post-incremented (ptr++) and then the value at the new address is displayed
	*ptr++;
	printf("\nAnswer of *ptr++ is %d",*ptr);
	
	//RIGHT TO LEFT post-increment value of *ptr, that is the value at address 'ptr', i.e. *ptr is post-incremented(*ptr)++
	ptr = &num;
	(*ptr)++;//Correct method of post-incrementing a value using a pointer . At this increment, the *ptr = 7 is going to be an answer
	
	printf("\nAnswer of (*ptr)++ : %d\n\n", *ptr);//Brackets are necessary for Post increment
	
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	int num;
	int *ptr = NULL;
	int *copy_ptr = NULL;
	
	//code
	num = 5;
	ptr = &num;
	
	printf("\n*********BEFORE copy_ptr = ptr********\n");
	printf("\nnum = %d",num);
	printf("\n&num = %p",&num);
	printf("\n*(&num) = %d",*(&num));
	printf("\nptr = %p",ptr);
	printf("\n*ptr = %d",*ptr);

	copy_ptr = ptr;//copy_ptr = ptr = &num
	printf("\n\n");
	printf("\n*********AFTER copy_ptr = ptr********\n");
	printf("\nnum = %d",num);
	printf("\n&num = %p",&num);
	printf("\n*(&num) = %d",*(&num));
	printf("\nptr = %p",ptr);
	printf("\n*ptr = %d",*ptr);
	printf("\ncopy_ptr = %p",copy_ptr);
	printf("\n*copy_ptr = %d",*copy_ptr);
	
	return(0);
}
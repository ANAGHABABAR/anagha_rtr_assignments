#include<stdio.h>

int main(void)
{
	//variable declarations
	double dNum;
	double *dptr = NULL;// dptr is a variable of type double*
	
	//code
	dNum = 10.3424242424;
	
	printf("\n\n");
	printf("\n*******Before dptr = &dNum********\n");
	printf("\nvalue of 'dNum' = %f\n",dNum);
	printf("\nAddress of dNum = %p\n",&dNum);
	printf("\nValue at Address of fNum is = %f",*(&dNum));
	
	//assigning address of variable 'num' to pointer variable 'ptr' 
	//ptr now contains address of num
	dptr = &dNum;
	
	printf("\n*******After dptr = &dNum********\n");
	printf("\nValue of dNum = %f\n",dNum);
	printf("\nAddress of dNum = %p\n",dptr);
	printf("\nValue at address of dNum = %f\n",*dptr);
	
	return(0);
}
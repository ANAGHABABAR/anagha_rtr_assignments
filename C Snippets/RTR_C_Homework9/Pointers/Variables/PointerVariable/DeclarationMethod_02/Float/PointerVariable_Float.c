#include<stdio.h>

int main()
{
	float fNum;
	float* fptr = NULL;//fptr is a variable of type float*
	
	fNum = 31.24f;
	
	printf("\n\n");
	
	printf("\n*******Before fptr = &fNum********\n");
	printf("\nvalue of 'fNum' = %f\n",fNum);
	printf("\nAddress of fNum = %p\n",&fNum);
	printf("\nValue at Address of fNum is = %f",*(&fNum));
	
	//assigning address of fNum to fptr as a value
	fptr = &fNum;
	
	printf("\n\n");
	printf("\n*******After fptr = &fNum********\n");
	printf("\nValue of fNum = %fn",fNum);
	printf("\nAddress of fNum = %p\n",fptr);
	printf("\nValue at address of fNum = %f\n",*fptr);
	
	return(0);
}
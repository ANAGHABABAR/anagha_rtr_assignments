#include<stdio.h>

int main(void)
{
	//variable declarations
	int num;
	int *ptr = NULL;// *ptr is a variable of type integer
	
	//code
	num = 10;
	
	printf("\n\n");
	printf("\n*******Before ptr = &num********\n");
	printf("\nvalue of 'num' = %d\n",num);
	printf("\nAddress of num = %p\n",&num);
	printf("\nValue at Address of num is = %d",*(&num));
	
	//assigning address of variable 'num' to pointer variable 'ptr' 
	//ptr now contains address of num
	ptr = &num;
	
	printf("\n*******After ptr = &num********\n");
	printf("\nValue of num = %d\n",num);
	printf("\nAddress of num = %p\n",ptr);
	printf("\nValue at address of num = %d\n",*ptr);
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	char ch;
	char *cptr = NULL;// *cptr is a variable of type integer
	
	//code
	ch = 'A';
	
	printf("\n\n");
	printf("\n*******Before cptr = &ch********\n");
	printf("\nvalue of 'ch' = %c\n",ch);
	printf("\nAddress of ch = %p\n",&ch);
	printf("\nValue at Address of ch is = %c",*(&ch));
	
	//assigning address of variable 'ch' to pointer variable 'cptr' 
	//cptr now contains address of ch
	cptr = &ch;
	
	printf("\n*******After cptr = &ch********\n");
	printf("\nValue of ch = %c\n",ch);
	printf("\nAddress of ch = %p\n",cptr);
	printf("\nValue at address of ch = %c\n",*cptr);
	
	return(0);
}
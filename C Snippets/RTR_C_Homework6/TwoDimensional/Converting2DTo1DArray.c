#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//variable declarations
	int iArray_2D[NUM_ROWS][NUM_COLUMNS];// Total number or elements = Total number or rows * Total number of columns
	int iArray_1D[NUM_ROWS * NUM_COLUMNS];
	
	int i, j, num;
	
	//code
	printf("\nEnter the elements for 2D array\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		printf("for Row number %d : \n",i);
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("Enter number elemrnt for : [%d][%d] = \n",i,j);
			scanf("%d",&num);
			iArray_2D[i][j] = num;
		}
		printf("\n\n");
	}
	
	//Display of 2D array
	printf("\n\n");
	printf("\n Two Dimensional (2D) Array of Integers : \n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		printf("\n**********Row %d***********\n",i);
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("\nArray[%d][%d] = %d\n",i,j,iArray_2D[i][j]);
		}
	}
	
	//Converting 2D integer array to 3D integer array
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			iArray_1D[(i * NUM_COLUMNS) + j] = iArray_2D[i][j];
		}
	}
	
	
	//printing array
	printf("\n\n");
	printf("One-Dimensional (1D) Array of Integers ");
	for(i = 0; i < NUM_ROWS * NUM_COLUMNS; i++)
	{
		printf("\niArray_1D[%d] = %d",i,iArray_1D[i]);
	}
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main()
{
	//variable declarations
	int i, j, k;
	
	//Inline initialization
	int iArray[NUM_ROWS][NUM_COLUMNS][DEPTH] = {{{9, 18}, {27, 36}, {45, 54}},
											   {{8, 16}, {24, 32}, {40, 48}},
											   {{7, 14}, {21, 28}, {35, 42}},
											   {{6, 12}, {18, 24}, {30, 36}},
											   {{5, 10}, {15, 20}, {25, 30}}};
	int iArray1D[NUM_ROWS * NUM_COLUMNS * DEPTH];//5 * 3 * 2 Elements => 30 Elements in 1D Array
	
	//code
	printf("\n\n");
	printf("\nElements in 3D array : \n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		printf("\n***********Row %d*************\n",i);
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("\n***********Column %d***********\n",j);
			for(k = 0; k < DEPTH; k++)
			{
				printf("\niArray[%d][%d][%d] = %d\n",i,j,k,iArray[i][j][k]);
			}
		}
	}
	
	//Converting 3D to 1D
	for(i = 0; i < NUM_ROWS; i++)
	{
		printf("\n***********Row %d*************\n",i);
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("\n***********Column %d***********\n",j);
			for(k = 0; k < DEPTH; k++)
			{
				iArray1D[(i * NUM_COLUMNS * DEPTH) + (j * DEPTH) + k] = iArray[i][j][k];
			}
		}
	}
	
	printf("\nConverted 3D to 1D array is :\n");
	for(i = 0; i < (NUM_ROWS * NUM_COLUMNS * DEPTH); i++)
	{
		printf("\niArray1D[%d] = %d\n",i,iArray1D[i]);
	}
	
	
	
	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	
	//In-line Initialization
	int iArray[5][3][2] = {{{9, 18}, {27, 36}, {45, 54}},
						   {{8, 16}, {24, 32}, {40, 48}},
						   {{7, 14}, {21, 28}, {35, 42}},
						   {{6, 12}, {18, 24}, {30, 36}},
						   {{5, 10}, {15, 20}, {25, 30}}};
	int iSize;
	int iArraySize;
	int iArrayNumElements, iArrayWidth, iArrayHeight, iArrayDepth = 1;
	
	//code
	printf("\n\n");
	
	iSize = sizeof(int);
	
	iArraySize =  sizeof(iArray);
	printf("Size of Three Dimensional Integer array is = %d\n\n",iArraySize);
	
	iArrayWidth = iArraySize / sizeof(iArray[0]);
	printf("\nNumber of Rows (width) in Three Dimensional (3D) Integer array is : %d\n\n",iArrayWidth);
	
	iArrayHeight = sizeof(iArray[0])/sizeof(iArray[0][0]);
	printf("\nsizeof (iArray[0] : %d) / sizeof(iArray[0][0]) : %d)\n",sizeof(iArray[0]),sizeof(iArray[0][0]));
	printf("\nNumber of Columns (Height) in Three Dimensional (3D) Integer array is : %d\n\n",iArrayHeight);
	
	iArrayNumElements = iArrayWidth * iArrayHeight * iArrayDepth;
	printf("\nNumber of Elements In Three Dimensional (3D) Integer Array is = %d \n\n",iArrayNumElements);
	
	printf("\n\n");
	printf("\nElements In Integer 3D Array : \n");
	
	//Piece meal Display
	//Row 1
	printf("\n********Row 1**********\n");
	printf("\n********Column 1*********\n");
	printf("\niArray[0][0][0] = %d \n",iArray[0][0][0]);
	printf("\niArray[0][0][1] = %d \n", iArray[0][0][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[0][1][0] = %d \n",iArray[0][1][0]);
	printf("\niArray[0][1][1] = %d \n", iArray[0][1][1]);
	printf("\n");
	
	printf("\n********Column 3*********\n");
	printf("\niArray[0][2][0] = %d \n",iArray[0][2][0]);
	printf("\niArray[0][2][1] = %d \n", iArray[0][2][1]);
	printf("\n");
	
	//Row 2
	printf("\n********Row 2**********\n");
	printf("\n********Column 1*********\n");
	printf("\niArray[1][0][0] = %d \n", iArray[1][0][0]);
	printf("\niArray[1][0][1] = %d \n", iArray[1][0][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[1][1][0] = %d \n", iArray[1][1][0]);
	printf("\niArray[1][1][1] = %d \n", iArray[1][1][1]);
	printf("\n");
	
	printf("\n********Column 3*********\n");
	printf("\niArray[1][2][0] = %d \n", iArray[1][2][0]);
	printf("\niArray[1][2][1] = %d \n", iArray[1][2][1]);
	printf("\n");
	
	//Row 3
	printf("\n********Row 3**********\n");
	printf("\n********Column 1*********\n");
	printf("\niArray[2][0][0] = %d \n", iArray[2][0][0]);
	printf("\niArray[2][0][1] = %d \n", iArray[2][0][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[2][1][0] = %d \n", iArray[2][1][0]);
	printf("\niArray[2][1][1] = %d \n", iArray[2][1][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[2][2][0] = %d \n", iArray[2][2][0]);
	printf("\niArray[2][2][1] = %d \n", iArray[2][2][1]);
	printf("\n");
	
	//Row 4
	printf("\n********Row 4**********\n");
	printf("\n********Column 1*********\n");
	printf("\niArray[3][0][0] = %d \n", iArray[3][0][0]);
	printf("\niArray[3][0][1] = %d \n", iArray[3][0][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[3][1][0] = %d \n", iArray[3][1][0]);
	printf("\niArray[3][1][1] = %d \n", iArray[3][1][1]);
	printf("\n");
	
	printf("\n********Column 3*********\n");
	printf("\niArray[3][2][0] = %d \n", iArray[3][2][0]);
	printf("\niArray[3][2][1] = %d \n", iArray[3][2][1]);
	printf("\n");
	
	printf("\n********Row 5**********\n");
	printf("\n********Column 1*********\n");
	printf("\niArray[4][0][0] = %d \n", iArray[4][0][0]);
	printf("\niArray[4][0][1] = %d \n", iArray[4][0][1]);
	printf("\n");
	
	printf("\n********Column 2*********\n");
	printf("\niArray[4][1][0] = %d \n", iArray[4][1][0]);
	printf("\niArray[4][1][1] = %d \n", iArray[4][1][1]);
	printf("\n");

	printf("\n********Column 3*********\n");
	printf("\niArray[4][2][0] = %d \n", iArray[4][2][0]);
	printf("\niArray[4][2][1] = %d \n", iArray[4][2][1]);
	printf("\n\n");


	return(0);
}
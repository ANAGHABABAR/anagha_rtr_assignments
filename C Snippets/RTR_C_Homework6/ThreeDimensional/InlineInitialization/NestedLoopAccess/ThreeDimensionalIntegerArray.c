#include<stdio.h>

int main(void)
{
	//variable declarations
	
	//Inline initialization
	int iArray[5][3][2] = {{{9, 18}, {27, 36}, {45, 54}},
						   {{8, 16}, {24, 32}, {40, 48}},
						   {{7, 14}, {21, 28}, {35, 42}},
						   {{6, 12}, {18, 24}, {30, 36}},
						   {{5, 10}, {15, 20}, {25, 30}}};
						   
	int iSize;
	int iArraySize;
	int iArrayNumElements, iArrayWidth, iArrayHeight, iArrayDepth;
	int i, j, k;
	
	//code
	printf("\n\n");
	
	iSize = sizeof(int);
	
	iArraySize = sizeof(iArray);
	printf("\nSize of Three Dimensional array is = %d\n", iArraySize);
	
	iArrayWidth = iArraySize / sizeof(iArray[0]);
	printf("\nThe number of Rows in Three Dimensional Array are : %d\n", iArrayWidth);
	
	iArrayHeight = sizeof(iArray[0]) / sizeof(iArray[0][0]);// 
	printf("\nThe number of Columns in Three Dimensional Array are : %d\n", iArrayHeight);
	
	iArrayDepth = sizeof(iArray[0][0]) / iSize;
	printf("\nDepth in Three Dimensional array is : %d\n", iArrayDepth);
	
	iArrayNumElements = iArrayWidth * iArrayHeight * iArrayDepth;
	printf("\nThe total number of elements are : %d\n",iArrayNumElements);
	
	printf("\n\n");
	printf("\nElements in 3D array are :");
	for( i = 0; i < iArrayWidth; i++)
	{
		printf("\n*********Row %d********\n",i);
		for(j = 0; j < iArrayHeight; j++)
		{
			printf("\n*********Column %d********\n",j);
			for(k = 0; k < iArrayDepth; k++)
			{
				printf("\niArray[%d][%d][%d] = %d\n",i,j,k,iArray[i][j][k]);
			}
		}
	}
	
	return(0);
}
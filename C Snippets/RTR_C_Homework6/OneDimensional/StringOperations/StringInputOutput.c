#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//variable declarations
	char chArray[MAX_STRING_LENGTH];//A character Array is a String
	
	//code
	printf("\n\n");
	printf("Enter a String : \n");
	//The gets_s function reads a line from the standard input stream stdin and stores it in buffer. The line consists of all characters up to and including the first newline character ('\n'). gets_s then replaces the newline character with a null character ('\0') before returning the line. 
	gets_s(chArray,MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("String Entered by you is : \n");
	printf("%s\n",chArray);
		
	return(0);
}
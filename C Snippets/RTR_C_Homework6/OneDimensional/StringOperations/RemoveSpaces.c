#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype
	int MyStrlen(char []);
	
	//variable declarations
	char chArray[MAX_STRING_LENGTH], chSpaceRemoved[MAX_STRING_LENGTH];
	int iStrLength;
	int i = 0, j = 0;
	
	printf("\n\n");
	printf("\nEnter a String :\n");
	gets_s(chArray,MAX_STRING_LENGTH);
		
	iStrLength = MyStrlen(chArray);
	
	j = 0;
	for(i = 0; i < iStrLength; i++)
	{
		if(chArray[i] == ' ')
		{
			continue;
		}
		else
		{
			chSpaceRemoved[j] = chArray[i];
			j++;
		}
	}
	
	
	printf("\n\n");
	printf("\nThe Original String that you entered is : %s\n",chArray);
	printf("\nThe String with Removed Spaces is : %s\n",chSpaceRemoved);
	
	return(0);
}


int MyStrlen(char String[])
{
	int iStrLength = 0;
	int i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	return(iStrLength);
}
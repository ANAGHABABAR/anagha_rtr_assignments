//This program replaces all the vowels in the string

#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//funcion prototype
	int MyStrlen(char[]);
	void MyStrcpy(char[],char[]);
	
	//variable declarations
	char chArrayOriginal[MAX_STRING_LENGTH],chArrayReplaced[MAX_STRING_LENGTH];//A character array is a string
	int iStrLength = 0;
	int i;
	
	//code
	//String input
	printf("\n\n");
	printf("\nEnter a String\n");
	
	gets_s(chArrayOriginal,MAX_STRING_LENGTH);
	MyStrcpy(chArrayReplaced,chArrayOriginal);
	
	iStrLength = MyStrlen(chArrayOriginal);
	
	for(i = 0; i < iStrLength; i++)
	{
		switch(chArrayReplaced[i])
		{
			case 'A':
			case 'a':
			case 'E':
			case 'e':
			case 'I':
			case 'i':
			case 'O':
			case 'o':
			case 'U':
			case 'u':
					chArrayReplaced[i] = '*';
					break;
			default:
					break;
		}//switch ends
	}//for ends
	
	
	printf("\n\n");
	printf("\nString Entered by You is \n");
	printf("\n %s",chArrayOriginal);
	
	printf("\n\n");
	printf("\nString with replaced Vowels is : \n%s\n",chArrayReplaced);
	
	return(0);
}


void MyStrcpy(char Destination[], char Source[])
{
	int iStrLength = MyStrlen(Source);
	int i = 0;
	
	for(i = 0; i < iStrLength; i++)
	{
		Destination[i] = Source[i];
	}
	Destination[i] = '\0';
}

int MyStrlen(char String[])
{
	int iStrLength = 0;
	int i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	printf("\nINSIDE MyStrlen : %d\n",iStrLength);
	return(iStrLength);
}
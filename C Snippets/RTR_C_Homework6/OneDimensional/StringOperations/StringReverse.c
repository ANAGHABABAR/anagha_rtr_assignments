#include<stdio.h>
#include<string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArrayOriginal[MAX_STRING_LENGTH];
	
	//code
	printf("\n\n");
	printf("Enter a string : \n\n");
	gets_s(chArrayOriginal,MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("The original string entered by you i.e. 'chArrayOriginal' : %s\n",chArrayOriginal);
	printf("%s\n",chArrayOriginal);
	
	printf("\n\n");
	printf("The Reversed String is %s",strrev(chArrayOriginal));
	printf("\n\n");
	
	return(0);	
	
}
//Program to Capitalize first letter of each word
#include<stdio.h>
#include<ctype.h>//for upper()

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char []);
	
	//variable declarations
	char chArray[MAX_STRING_LENGTH],chCapitalize[MAX_STRING_LENGTH];
	int iStrLength;
	int i, j;
	
	//code
	
	//string input
	printf("\n\n");
	printf("\nEnter a String : \n");
	gets_s(chArray,MAX_STRING_LENGTH);
	
	iStrLength = MyStrlen(chArray);
	j = 0;
	for(i = 0; i < iStrLength; i++)
	{
		if(i == 0)
		{
			chCapitalize[j] = toupper(chArray[i]);
		}
		else if(chArray[i] == ' ')
		{
			chCapitalize[j] = chArray[i];
			chCapitalize[j+1] = toupper(chArray[i+1]);
			//Since already two characters at indices 'i' and 'i+1' have been considered in this else-if Block.. We are extra incrementing i and j by +1
			j++;
			i++;
		}
		else
		{
			chCapitalize[j] = chArray[i];
		}
		j++;
		
	}
	chCapitalize[i] = '\0';
	
	printf("\n\n");
	printf("\nThe Original String that was entered by You is : %s\n",chArray);
	printf("\n\n");
	printf("\nThe String After Capitalizing each word : %s\n",chCapitalize);
	
	return(0);
}



int MyStrlen(char String[])
{
	int iStrLength = 0;
	int i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	return(iStrLength);
}
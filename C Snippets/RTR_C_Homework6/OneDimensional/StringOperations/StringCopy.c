#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArrayOriginal[MAX_STRING_LENGTH], chArrayCopy[MAX_STRING_LENGTH];
	int iStrLength = 0;
	
	//code
	printf("\n\n");
	printf("Enter a String : \n");
	gets_s(chArrayOriginal,MAX_STRING_LENGTH);
	
	//inbuilt strcpy()
	strcpy(chArrayCopy, chArrayOriginal);
	printf("\nCopied String chArrayCopy is : %s\n",chArrayCopy);

	return(0);
}


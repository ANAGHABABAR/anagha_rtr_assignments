#include<stdio.h>

#define MAX_STRING_LENGTH 512

#define SPACE ' '
#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	//function prototype
	int MyStrlen(char []);
	char ToMyUpper(char);
	
	//ariable declarations
	char chArray[MAX_STRING_LENGTH], chArrayCapitalizeFirstLetter[MAX_STRING_LENGTH];
	int iStringLength;
	int i, j;
	
	//code
	
	//String input
	printf("\n\n");
	printf("\nEnter a String :\n");
	gets_s(chArray,MAX_STRING_LENGTH);
	
	iStringLength = MyStrlen(chArray);
	
	j = 0;
	for(i = 0; i < iStringLength; i++)
	{
		if(i == 0)
		{
			chArrayCapitalizeFirstLetter[j] = ToMyUpper(chArray[i]);
		}
		else if(chArray[i] == SPACE)//First letter of Every word in the senence must be a CAPITAL LETTER. Words are separated by Spaces.
		{
			chArrayCapitalizeFirstLetter[j] = chArray[i];
			chArrayCapitalizeFirstLetter[j+1] = ToMyUpper(chArray[i+1]);
			//Since already two characters at indices 'i' and 'i+1' have been considered in this else-if Block.. We are extra incrementing i and j by 1
			j++;
			i++;
		}
		else if((chArray[i] == FULLSTOP) || (chArray[i] == COMMA) || (chArray[i] == EXCLAMATION) || (chArray[i] == QUESTION_MARK))//First letter of Every word After punctuation mark, in the sentence must be a capital letter. Words are separated by punctuations 
		{
			chArrayCapitalizeFirstLetter[j] = chArray[i];
			chArrayCapitalizeFirstLetter[j + 1] = SPACE;
			chArrayCapitalizeFirstLetter[j + 2] = ToMyUpper(chArray[i+1]);
			//Since, already 2 characters (at indices 'i' and i+1 have been considered in else-if block )
			//Since, 3 characters (At indices j, j+1 and j+2) have been considered in this else-if block we are extra incrementing else-if block by 2
			j = j + 2;
			i++;			
		}
		else
		{
			chArrayCapitalizeFirstLetter[j] = chArray[i];
		}
		j++;
	}
	
	printf("\n\n");
	printf("\nThe Original String is  : %s\n",chArray);
	printf("\nString after Capitalizing is : %s\n",chArrayCapitalizeFirstLetter);
	
	return(0);
}


int MyStrlen(char String[])
{
	int iStrLength = 0;
	int i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	return(iStrLength);
}

char ToMyUpper(char ch)
{
	//variable declarations
	int num;
	int c;
	
	//code
	//ASCII value of 'a' : 97 - 'A' : 65 == 32 
	//This subtraction will give exact difference between Upper case and lower case each alphabet
	//If this difference is subtracted from the ASCII value of the lower case letter, the resultant ascii value will be that of its uppercase. 
	//a - z = 97 To 122
	//A - Z = 65 To 90
	
	num = 'a' - 'A';
	
	if((ch >= 97) && (ch <=122))
	{
		c = (int)ch - num;
		return((char)c);
	}
	else
	{
		return(ch);
	}
}
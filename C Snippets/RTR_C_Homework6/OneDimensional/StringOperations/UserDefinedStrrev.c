#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function declaration
	void MyStrrev(char [], char []);
	
	//variable declarations
	char chArrayOriginal[MAX_STRING_LENGTH], chArrayReversed[MAX_STRING_LENGTH];
	
	//string input
	printf("\n\n");
	printf("Enter a string :");
	gets_s(chArrayOriginal,MAX_STRING_LENGTH);
	
	MyStrrev(chArrayOriginal,chArrayReversed);
	
	printf("\nThe reversed string is :%s\n",chArrayReversed);

	
	return(0);
}

void MyStrrev(char Original[], char Reversed[])
{
	int i = 0,j = 0, iLength = 0;
	
	iLength = (MyStrlen(Original)) - 1;
	
	for(i = 0,j = iLength; i <= iLength , j >= 0; i++, j--)
	{
		Reversed[i] = Original[j];
	}
	Reversed[i] = '\0';
		
}

int MyStrlen(char *String)
{
	
	int i = 0, iCnt = 0;
	
	for(i = 0; i <= MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	
	return(iCnt);
}
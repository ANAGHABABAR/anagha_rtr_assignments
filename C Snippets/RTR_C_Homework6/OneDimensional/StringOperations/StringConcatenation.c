#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArrayOriginal1[MAX_STRING_LENGTH], chArrayOriginal2[MAX_STRING_LENGTH],chArrayConcatenated[MAX_STRING_LENGTH];
	
	printf("\n\n");
	printf("\nEnter first String\n");
	gets_s(chArrayOriginal1, MAX_STRING_LENGTH);
	
	printf("\nEnter second String\n");
	gets_s(chArrayOriginal2, MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("\nThe Concatenation result of %s and %s :\n",chArrayOriginal1,chArrayOriginal2);
	strcat(chArrayOriginal1,chArrayOriginal2);
	printf("%s\n\n",chArrayOriginal1);
	printf("%s\n\n",chArrayOriginal2);

	
	return(0);
}
#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype
	int MyStrlen(char []);
	
	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStrLength;
	int i = 0;
	int iWordCount = 0, iSpaceCount = 0;
	
	printf("\n\n");
	printf("\nEnter a String \n");
	gets_s(chArray,MAX_STRING_LENGTH);
	 
	iStrLength = MyStrlen(chArray);
	
	for(i = 0; i < iStrLength; i++)
	{
		switch(chArray[i])
		{
			case 32:	//32 is an ASCII value for ' '(space)
					iSpaceCount++;
					break;
			default:
					break;
		}
	}
	
	iWordCount = iSpaceCount + 1;
	//we have added '1' explicitly here because, number of words are always going to be +1 than number of spaces e.g. Anagha Ramdas Babar
	//Here, if we don't add +1 explicitly then number of words as an output will be given as 2. But when we add +1 that time it is considered as 3 words.
	
	printf("\nThe String you entered is : %s\n", chArray);
	printf("\n\n");
	printf("\nNumber of spaces in the Given string are : %d\n", iSpaceCount);
	printf("\nThe number of words in a given string are : %d \n", iWordCount);	
	
	return(0);
}

int MyStrlen(char String[])
{
	int iStrLength = 0;
	int i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	return(iStrLength);
}
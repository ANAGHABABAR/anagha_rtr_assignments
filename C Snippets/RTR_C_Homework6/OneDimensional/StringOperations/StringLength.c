#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStrLength = 0;
	
	//code
	printf("\n\n");
	printf("Enter a String : \n");
	gets_s(chArray,MAX_STRING_LENGTH);
	
	//string output
	printf("\nString you entered is :\n");
	printf("%s ",chArray);
	
	printf("\n");
	
	//String length check
	iStrLength = strlen(chArray);
	printf("\nThe length of the Sting you entered is : %d\n",iStrLength);

	return(0);
}
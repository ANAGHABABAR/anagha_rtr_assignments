#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStrLength = 0;
	
	//code
	printf("\n\n");
	printf("Enter a String : \n");
	gets_s(chArray,MAX_STRING_LENGTH);
	
	//string output
	printf("\nString you entered is :\n");
	printf("%s ",chArray);
	
	printf("\n");
	
	//String length check
	iStrLength = MyStrlen(chArray);
	printf("\nThe length of the Sting you entered is : %d\n",iStrLength);

	return(0);
}

int MyStrlen(char String[])
{
	//variable declarations
	int j;
	int StrLength = 0;
	
	//code
	
	for(j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if(String[j] == '\0')
		{
			break;
		}
		else
		{
			StrLength++;
		}
	}
	
	return(StrLength);
}
#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcat(char[], char[]);
	
	//variable declarations
	char chArrayOne[MAX_STRING_LENGTH], chArrayTwo[MAX_STRING_LENGTH];
	
	printf("\n\n");
	printf("\nEnter first string:\n");
	gets_s(chArrayOne,MAX_STRING_LENGTH);
	
	printf("\nEnter second string:\n");
	gets_s(chArrayTwo,MAX_STRING_LENGTH);
	
	printf("\n\n");
	printf("\nThe original strings before concatenations are \n");
	printf("\nchArrayOne : %s\n chArrayTwo : %s\n\n",chArrayOne,chArrayTwo);
	
	MyStrcat(chArrayOne,chArrayTwo);
	
	printf("\n\n");
	printf("\nThe strings after concatenation are : \n");
	printf("\nchArrayOne : %s\n chArrayTwo : %s",chArrayOne,chArrayTwo);
	
	
	return(0);
}

void MyStrcat(char Destination[], char Source[])
{
	int MyStrLen(char[]);
	
	int i = 0, j = 0;
	int iSrcLength = 0, iDestLength = 0;
	
	iSrcLength = MyStrLen(Source);
	iDestLength = MyStrLen(Destination);
	
	for(i = iDestLength, j = 0; i < iSrcLength+iDestLength; i++,j++)
	{
		Destination[i] = Source[j];
	}
	
	Destination[i] = '\0';
	
}

int MyStrLen(char String[])
{
	int iStrLength = 0;
	int iCnt = 0;
	
	for(iCnt = 0; iCnt < MAX_STRING_LENGTH; iCnt++)
	{
		if(String[iCnt] == '\0')
		{
			break;
		}
		else
		{
			iStrLength++;
		}
	}
	
	
	return(iStrLength);
}
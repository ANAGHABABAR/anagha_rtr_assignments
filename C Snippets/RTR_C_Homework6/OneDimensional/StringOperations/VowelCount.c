#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//funtion prototype
	int MyStrlen(char []);
	
	//variable declarations
	char chArray[MAX_STRING_LENGTH];
	int iStrLength = 0;
	int cntA = 0, cntE = 0, cntI = 0, cntO = 0, cntU = 0;
	int i;
	
	//String input
	printf("\n\n");
	printf("\nEnter a string  : \n");
	gets_s(chArray,MAX_STRING_LENGTH);
	
	iStrLength = MyStrlen(chArray);
	
	for(i = 0; i < iStrLength; i++)
	{
		switch(chArray[i])
		{
			case 'A':
			case 'a':
					cntA++;
					break;
					
			case 'E':
			case 'e':
					cntE++;
					break;
					
			case 'I':
			case 'i':
					cntI++;
					break;
					
			case 'O':
			case 'o':
					cntO++;
					break;
					
			case 'U':
			case 'u':
					cntU++;
					break;
			default :
					break;
					
		}
	}
	
	printf("\n\n");
	printf("\nThe vowel 'A' or 'a' has appeared %d times \n",cntA);
	printf("\nThe vowel 'E' or 'e' has appeared %d times \n",cntE);
	printf("\nThe vowel 'I' or 'i' has appeared %d times \n",cntI);
	printf("\nThe vowel 'O' or 'o' has appeared %d times \n",cntO);
	printf("\nThe vowel 'U' or 'u' has appeared %d times \n",cntU);
	
	return(0);
}

int MyStrlen(char String[])
{
	int i = 0, strCount = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			strCount++;
		}
	}
	return(strCount);
}
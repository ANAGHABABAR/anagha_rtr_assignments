#include<stdio.h>

#define MAX_STRING_LENGTH 19

int main(void)
{
	//function declaration
	void MyStrCpy(char [], char []);
	
	//variable declarations
	char chArrayOriginal[MAX_STRING_LENGTH], chArrayCopy[MAX_STRING_LENGTH];
	
	//code
	printf("\n\n");
	printf("\nEnter a string :\n");
	gets_s(chArrayOriginal,MAX_STRING_LENGTH);
	
	MyStrCpy(chArrayCopy, chArrayOriginal);
	
	//string output
	printf("\nThe original string entered by you : %s\n",chArrayOriginal);
	
	printf("\n\n");
	printf("\nThe copied string is : %s\n",chArrayCopy);
	
	return(0);
}

void MyStrCpy(char Copy[],char Original[])
{
	//funtion prototype
	int MyStrlen(char []);
	
	//variable declaration
	int i = 0, StrLength = 0;
	
	StrLength = MyStrlen(Original);
	
	for(i = 0; i < StrLength; i++)
	{
		Copy[i] = Original[i];
	}
	
	Copy[i] = '\0';	//if this '\0' is not entered explicitly then compiler would not know where to stop and either a garbage value will be read or program will exit abruptly due to access of a restricted memory area. This is a must step
	
	
}

int MyStrlen(char String[])
{
	int iCnt = 0, i = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(String[i] == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	return(iCnt);
}
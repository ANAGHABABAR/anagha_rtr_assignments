#include<stdio.h>

#define MAX_NUMBER(a, b) ((a > b) ? a : b)

int main(int argc, char *argv[], char *envp[])
{
	//variable declarations
	int iNum_01;
	int iNum_02;
	int iRet;
	
	float fNum_01 = 0.0f;
	float fNum_02 = 0.0f;
	float fRet;
	
	//code
	//comparing integer values
	printf("\n\n");
	printf("\nEnter an integer number : \n");
	scanf("%d",&iNum_01);

	printf("\n\n");
	printf("\nEnter Another integer number : \n");
	scanf("%d",&iNum_02);
	
	iRet = MAX_NUMBER(iNum_01, iNum_02);
	printf("\n\n");
	printf("Result of Macro function MAX_NUMBER() = %d\n",iRet);
	
	printf("\n******************************\n");
	
	// comparing float values 
	printf("\n\n");
	printf("\nEnter an Float number : \n");
	scanf("%f",&fNum_01);

	printf("\n\n");
	printf("\nEnter Another Float number : \n");
	scanf("%f",&fNum_02);
	
	fRet = MAX_NUMBER(fNum_01, fNum_02);
	printf("\n\n");
	printf("Result of Macro function MAX_NUMBER() = %f\n",fRet);
	
	printf("\n\n");
	
}
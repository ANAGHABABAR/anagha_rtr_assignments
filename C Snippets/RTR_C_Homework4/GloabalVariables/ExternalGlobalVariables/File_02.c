//global_count is a global variable declared in source code file ExternalGlobalVariableMultipleFiles.c
// To access it in this file, it must be re-declared as an external varible in the global scope of this variable of this file along with the 'extern' keyword and its proper datatype
//Then it can be used as any ordinary global variable throughout this file as well

#include<stdio.h>

extern int global_count;

extern void change_count_two(void)
{
	//code
	global_count += 11;
		
	printf("\nchange_count_two(): value of change_count = %d\n",global_count);
}

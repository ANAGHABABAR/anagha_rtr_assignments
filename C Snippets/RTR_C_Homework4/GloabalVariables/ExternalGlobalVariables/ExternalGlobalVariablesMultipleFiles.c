#include<stdio.h>

// ***Global Scope ***
int global_count = 0;

int main(void)
{
	//function prototypes
	void change_count(void);
	void change_count_one(void);
	void change_count_two(void);
	
	//code
	printf("\n");
	
	change_count();
	change_count_one();
	change_count_two();
		
	return(0);
}

void change_count()
{
	global_count += 1;
	printf("\nThe value of global_count is = %d\n", global_count);
}
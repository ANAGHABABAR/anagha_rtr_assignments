#include<stdio.h>

int main()
{
	//function prototypes
	void change_count(void);
	
	//variable declarations
	extern int global_count;
	
	//code
	printf("\n\n");
	printf("\nValue of global_count before change_count() = %d\n", global_count);
	change_count();
	printf("\nValue of global_count after change_count() = %d\n", global_count);
	printf("\n");
		
	return(0);
}

// ***Global Scope ***
//global_count is a global variable 
//since it is declared before change_count(), it can be accessed and used as any ordinary global variable in change_count()
//Since, it has been declared after main(), it must be first re-declared in main() as an external global variable by means of the 'extern' keyword and the type of the variable
//Once this is done, it can be used as an ordinary global variable in main as well.

int global_count = 0;

void change_count()
{
	//code
	global_count = 11;
	printf("\nThe value of global_count has now been changed to = %d\n", global_count);
}


#include<stdio.h>

//Global Scope
// *** If not initialized by us, global variables are initialized to their zero values (with respect to //their data types i.e. : 0 for int, 0.0 for float and double, '\0' for char, etc)
//but still for good programming practice we should initialize global variables

int global_count = 0;

int main(void)
{
	//function prototypes
	void change_count_one(void);
	void change_count_two(void);
	void change_count_three(void);
	
	//code
	printf("main() : Value of global_count = %d\n", global_count);
	
	change_count_one();
	change_count_two();
	change_count_three();
	
	return(0);
}

//Global scope

void change_count_one()
{
	global_count = 100;
	printf("change_count_one : Value of global_count = %d\n",global_count);
	
}

void change_count_two()
{
	global_count += 1 ;
	printf("change_count_two : Value of global_count = %d\n",global_count);
	
}

void change_count_three()
{
	global_count += 100;
	printf("change_count_three : Value of global_count = %d\n",global_count);
	
}


#include<stdio.h> // 'stdio.h' contains declaration to 'printf()'
#include<ctype.h> // 'ctype.h' contains declaration to 'atoi()'
#include<stdlib.h> // 'stdlib.h' contains declaration of 'exit()'

int main(int argc, char *argv[], char *envp[])
{
	//variable declarations
	int i;
	int num;
	int sum = 0;
	
	//code
	if(argc == 1)
	{
		printf("\nNo numbers given for addition. Exiting..\n");
		printf("\nUsage : CommandLineArgumentsApplication <first number> <second number>\n");
		exit(0);
	}
	
	// ***this program adds all command line arguments given in integer form only and output the sum
	// ***Due to the use of atoi(), All Command line arguments of types other than 'int' are ignored
	
	printf("\n\n");
	printf("Sum of All Command line arguments is : \n");
	for(i = 1; i < argc; i++ )//loop starts with i = 1 because argv[0] is the name of the program
	{
		num = atoi(argv[i]);
		sum = sum + num;
	}
	
	printf("Sum = %d\n\n",sum);
	
	return(0);
}
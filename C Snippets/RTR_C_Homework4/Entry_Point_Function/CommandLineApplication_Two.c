#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//variable declaration
	int i;
	
	//code
	if(argc != 4) //Program name+ first name + middle name + surname = 4
	{
		printf("\n\n");
		printf("Invalid Usage !!! Exiting Now ...");
		printf("Usage : CommandLineArgument <first name> <middle name> <surname>");
		exit(0);
	}
	
	// ***THIS PROGRAMS PRINTS YOUR FULL NAME AS ENTERED IN THE COMMAND LINE ARGUMENTS
	printf("\n\n");
	printf("Your full name is : ");
	
	for(i = 1; i < argc; i++)//loop starts from i = 1 beacause, i = 0 will result in argv[i] = argv[0]
						    //which is the name of the program itself i.e. 'CommandLineArgumentsApplication.exe'
	{
		printf("%s ",argv[i]);
	}
	
	printf("\n\n");
	
	return(0);
}
#include<stdio.h> //'stdio.h' contains declaration of 'printf()'

int main(int argc, char *argv[])
{
	//variable declaration
	int i;
	
	//code
	printf("\n\n");
	printf("Hello Anagha..\n\n");
	printf("\nNumber of Command Line Arguments : %d\n",argc);
	printf("Command Line Arguments passed to this program are : \n");
	for(i = 0; i < argc; i++)
	{
		printf("\nCommand Line Argument Number %d = %s \n",(i+1),argv[i]);
	}
	printf("\n\n");	
	
	return(0);
}

///////////////////////////////////////////////////////////////////////////////
//Input:
//G:\RTR_VisualStudio_2019\RTR_C_Homework4\Entry_Point_Function>EntryPointFun_CmdLineArg2.exe anagha ramdas babar RTR 2020 batch

//Output:
//Hello Anagha..


//Number of Command Line Arguments : 7
//Command Line Arguments passed to this program are :

//Command Line Argument Number 1 = EntryPointFun_CmdLineArg2.exe

//Command Line Argument Number 2 = anagha

//Command Line Argument Number 3 = ramdas

//Command Line Argument Number 4 = babar

//Command Line Argument Number 5 = RTR

//Command Line Argument Number 6 = 2020

//Command Line Argument Number 7 = batch
#include<stdio.h>

int main(int argc, char **argv, char **envp)
{
	//variable declarations
	int i;
	
	//code
	printf("\nHello Anagha\n");
	printf("Number of Command Line arguments = %d\n\n",argc);
	printf("Command Line arguments passed to this program are :\n");
	for(i = 0; i < argc; i++)
	{
		printf("\nCommand Line Argument Number %d =  %s\n",i,argv[i]);
	}
	printf("\n\n");
	
	printf("\nFirst 20 Environment variables Passed to this program are : \n\n");
	for(i = 0; i < 20 ; i++)
	{
		printf("\nEnvironment variable number %d = %s\n",i,envp[i]);
	}
	printf("\n\n");
	
	return(0);
}

//////////////////////////////////////////////////////////////////////////////////


// G:\RTR_VisualStudio_2019\RTR_C_Homework4\Entry_Point_Function>FullFledgeEntryPointFunction.exe

//Output
// Hello Anagha
// Number of Command Line arguments = 1

// Command Line arguments passed to this program are :

// Command Line Argument Number 0 =  FullFledgeEntryPointFunction.exe



// First 20 Environment variables Passed to this program are :


// Environment variable number 0 = ALLUSERSPROFILE=C:\ProgramData

// Environment variable number 1 = APPDATA=C:\Users\Anagha\AppData\Roaming

// Environment variable number 2 = CommandPromptType=Native

// Environment variable number 3 = CommonProgramFiles=C:\Program Files (x86)\Common Files

// Environment variable number 4 = CommonProgramFiles(x86)=C:\Program Files (x86)\Common Files

// Environment variable number 5 = CommonProgramW6432=C:\Program Files\Common Files

// Environment variable number 6 = COMPUTERNAME=LENOVO-PC

// Environment variable number 7 = ComSpec=C:\WINDOWS\system32\cmd.exe

// Environment variable number 8 = configsetroot=C:\WINDOWS\ConfigSetRoot

// Environment variable number 9 = DevEnvDir=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE\

// Environment variable number 10 = DriverData=C:\Windows\System32\Drivers\DriverData

// Environment variable number 11 = easyplussdk="C:\Program Files (x86)\Common Files\lenovo\easyplussdk\bin"

// Environment variable number 12 = ExtensionSdkDir=C:\Program Files (x86)\Microsoft SDKs\Windows Kits\10\ExtensionSDKs

// Environment variable number 13 = FPS_BROWSER_APP_PROFILE_STRING=Internet Explorer

// Environment variable number 14 = FPS_BROWSER_USER_PROFILE_STRING=Default

// Environment variable number 15 = FP_NO_HOST_CHECK=NO

// Environment variable number 16 = Framework40Version=v4.0

// Environment variable number 17 = FrameworkDir=C:\windows\Microsoft.NET\Framework\

// Environment variable number 18 = FrameworkDIR32=C:\windows\Microsoft.NET\Framework\

// Environment variable number 19 = FrameworkVersion=v4.0.30319



// G:\RTR_VisualStudio_2019\RTR_C_Homework4\Entry_Point_Function>
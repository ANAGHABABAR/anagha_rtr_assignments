#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//function prototypes
	void displayInfo(void);
	void Country(void);
	
	//code
	displayInfo();
	Country();
	
	return(0);
}

void displayInfo()
{
	///function prototypes
	void Func_My(void);
	void Func_Name(void);
	void Func_is(void);
	void Func_fName(void);
	void Func_mName(void);
	void Func_lName(void);
	void Func_OfAMC(void);
	
	//code
	
	//Function calls
	Func_My();
	Func_Name();
	Func_is();
	Func_fName();
	Func_mName();
	Func_lName();
	Func_OfAMC();
	
}

void Func_My()
{
	printf("\n\n");
	printf("My ");
	
}

void Func_Name()
{
	printf("Name ");
}


void Func_is()
{
	printf("is ");
}

void Func_fName()
{
	printf("Anagha ");
}

void Func_mName()
{
	printf("Ramdas ");
}

void Func_lName()
{
	printf("Babar ");
}

void Func_OfAMC()
{
	printf("of RTR 2020 Batch.");
}

void Country()
{
	printf(" I live in INDIA.\n");
}
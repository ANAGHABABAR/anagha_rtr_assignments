#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//functiom prototype
	int MyAddition(void);
	
	//variable declarations
	int result;
	
	//code
	result = MyAddition();
	
	printf("\nSum = %d\n", result);

	return(0);
}

int MyAddition(void)
{
	int a, b, sum;
	
	printf("\nEnter an Integer value for A : ");
	scanf("%d",&a);
	
	printf("\nEnter an integer value for B : ");
	scanf("%d",&b);
	
	sum = a + b;
	
	return sum;
	
}
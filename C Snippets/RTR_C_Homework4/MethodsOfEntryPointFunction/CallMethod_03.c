#include<stdio.h>

int main(int argc, char *argv[],char *envp[])
{
	//function prototypes
	void Func_Country();
	
	//code
	Func_Country();
	
	return(0);
}

void Func_Country(void)
{
	//function declaration
	void Func_OfAMC(void);
	
	//functio call
	Func_OfAMC();
	
	//code
	printf("\n\n");
	
	printf("I live in India.");
	
	printf("\n\n");
}

void Func_OfAMC()
{
	//functio prototype
	void Func_lName(void);
	
	//function call
	Func_lName();
	
	printf("\n\n");
	
	printf("RTR Batch 2020");

}

void Func_lName()
{
	//function prototype
	void Func_mName(void);
	
	//function call
	Func_mName();
	
	printf(" Babar");
}

void Func_mName()
{
	//function prototype
	void Func_fName(void);
	
	//function call
	Func_fName();
	
	printf(" Ramdas");
}

void Func_fName()
{
	//function prototype
	void Func_is(void);
	
	//function call	
	Func_is();
	
	printf(" Anagha");
}

void Func_is()
{
	//function prototype
	void Func_name(void);
	
	//function call
	Func_name();

	printf(" is");
}

void Func_name()
{
	//fun prototype
	void Func_My(void);
	
	Func_My();
	
	printf(" name");
}

void Func_My()
{
	printf("\n\n");
	printf(" My");
}
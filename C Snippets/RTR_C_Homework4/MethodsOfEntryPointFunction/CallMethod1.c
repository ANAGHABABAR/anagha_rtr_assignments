#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//function prototype
	void Addition(void);
	int Subtraction(void);
	void Multiplication(int, int);
	int MyDivision(int, int);

	//variable declaration
	int iSubResult = 0;
	int iMult1, iMult2;
	int iDiv1, iDiv2, iDivResult;
	
	//code
	//call to Addition()
	Addition();
	
	//Subtraction
	iSubResult = Subtraction();
	printf("\nThe Subtraction result is %d\n",iSubResult);
	printf("\n****************************************\n");

	//multiplication
	printf("\nEnter 1st Number for multiplication :\n");
	scanf("%d",&iMult1);
	
	printf("\nEnter 2nd Number for multiplication :\n");
	scanf("%d",&iMult2);
	
	Multiplication(iMult1, iMult2);
	
	//Division
	printf("\nEnter 1st Number for Division :\n");
	scanf("%d",&iDiv1);
	
	printf("\nEnter 2nd Number for Division :\n");
	scanf("%d",&iDiv2);
	
	iDivResult = Division(iDiv1, iDiv2);
	printf("\nDivision of %d and %d gives %d(Quotient)\n",iDiv1, iDiv2, iDivResult);
	printf("\n*************************************n");
	
	return(0);
}

void Addition()
{
	int a, b, sum;
	
	printf("\nEnter 1st Number for Addition : \n");
	scanf("%d",&a);
	
	printf("\nEnter 2nd Number for Addition: \n");
	scanf("%d",&b);
	
	sum = a + b;
	
	printf("\nThe sum of Given %d and %d is %d\n",a, b, sum);
	printf("\n****************************************\n");
	
}

int Subtraction()
{
	int a, b, iSub;
	
	printf("\nEnter 1st Number for Subtraction : \n");
	scanf("%d",&a);
	
	printf("\nEnter 2nd Number for Subtraction : \n");
	scanf("%d",&b);
	
	iSub = a - b;
	
	return(iSub);
}

void Multiplication(int a, int b)
{
	int iMult = 0;
	
	iMult = a * b;
	
	printf("\nThe multiplication of %d and %d gives %d\n", a, b, iMult);
	printf("\n****************************************\n");	
}

int Division(int a,int b)
{
	int iDiv = 0;
	
	if(a > b)
	{
		iDiv = a/b;
	}
	else
	{
		iDiv = b/a;
	}
	
	return(iDiv);
}
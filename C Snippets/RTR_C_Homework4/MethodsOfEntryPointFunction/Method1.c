#include<stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	//function prototype/ declaration / signature
	void MyAddition(void);
	
	//code
	MyAddition();// function call
	
	return(0);
}

// ****USER DEFINED FUNCTION : METHOD OF DEFINITION 1 *******
// **** NO RETURN VALUE, NO PARAMETERS *****

void MyAddition(void)
{
	//variable declaration : local variable to MyAddition()
	int a, b, sum;
	
	//code
	printf("\n\n");
	printf("Enter Integer Value For 'A' : ");
	scanf("%d",&a);
	
	printf("Enter Integer Value For 'B' : ");
	scanf("%d",&b);
	
	sum = a+b;
	
	printf("\n\n");
	printf("\nSum of %d And %d = %d\n\n", a, b, sum);
}
#include<stdio.h>

int main(int argc, char * argv[], char * envp)
{
	//function prototype
	int Addition(int, int);
	
	//variable declaration
	int iNum1, iNum2, iRet;
	
	
	printf("\nEnter an Integer value for A : ");
	scanf("%d",&iNum1);
	
	printf("\nEnter an integer value for B : ");
	scanf("%d",&iNum2);
	
	iRet = Addition(iNum1, iNum2);
	printf("\nThe additin of given 2 numbers is %d\n",iRet);
	
	return(0);
}

int Addition(int a, int b)
{
	int sum = 0;
	
	sum = a + b;
	
	return(sum);
}
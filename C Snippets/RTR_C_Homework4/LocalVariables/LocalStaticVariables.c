#include<stdio.h>

int main(void)
{
	// Local scope of main begins
	
	//variable declarations
	//'a' is a local variable. It is local to main() only
	int a = 11;
	
	//function prototypes
	void change_count(void);
	
	//code
	printf("\n");
	printf("A = %d\n\n", a);
	
	//loacl count is initialized to 0
	
	//first call
	printf("\nFirst call to change_count() gives : \n");
	change_count();
	
	//second call
	printf("\nSecond call to change_count() gives : \n");
	change_count();
	
	//third call
	printf("\nThird call to change_count() gives : \n");
	change_count();
	
	return(0);
}

void change_count()
{
	
	//when we make variable as static, previous value is preserved in the variable
	static int local_count = 0;
	
	local_count += 1;
	
	printf("\nlocal_count = %d\n",local_count);
}
#include<stdio.h>

int main(void)
{
	//function prototypes
	void PrintbinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a;
	unsigned int b;
	unsigned int result;
	
	//code
	printf("\n\n");
	printf("\nEnter First Integer : \n");
	scanf("%d",&a);
	
	printf("\nEnter Second Integer : \n");
	scanf("%d",&b);
	
	printf("\n*********************OUTPUT********************\n");
	result = a ^ b;
	printf("\nBitwise XOR-ing of a = %d and b = %d gives the Decimal output as result = %d\n",a,b,result);
	
	PrintbinaryFormOfNumber(a);
	PrintbinaryFormOfNumber(b);
	PrintbinaryFormOfNumber(result);
	
	return 0;
}

void PrintbinaryFormOfNumber(unsigned int decimal_number)
{
	//variable declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;
	
	//code
	for(i = 0; i < 8; i++)
		binary_array[i] = 0;
	printf("\nThe binary form of Decimal Integer %d is \t = \t",decimal_number);
		
	num = decimal_number;
	i = 7;
	while(num != 0)
	{
		quotient = num/2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}
	
	for(i = 0; i < 8; i++)
		printf("\n%u", binary_array[i]);

	printf("\n");
}

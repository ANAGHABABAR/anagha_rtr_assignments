#include<stdio.h>

int main(void)
{
	//variable declarations
	int a,b, result;
	
	//code
	printf("\n");
	printf("\nEnter first number : \n");
	scanf("%d",&a);
	
	printf("\nEnter Second Number : \n");
	scanf("%d",&b);
	
	printf("\nIf the Anser = 0, it is FALSE.\n If the Answer = 1, it is TRUE\n");
	
	result = (a < b);
	printf("\na < b is %d\n", result);
	
	result = (a > b);
	printf("\n a > b is %d\n", result);
	
	result = (a <= b);
	printf("\n a <= b is %d\n", result);
	
	result = (a >= b);
	printf("\n a >= b is %d\n", result);
	
	result = (a == b);
	printf("\n a == b is %d\n", result);
	
	result = (a != b);
	printf("\n a != b is %d\n", result);
	

	return(0);
}
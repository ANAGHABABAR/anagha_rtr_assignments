#include<stdio.h>

int main(void)
{
	//function prototypes
	void PrintbinaryFormOfNumber(unsigned int);
	
	//variable declarations
	unsigned int a;
	unsigned int b;
	unsigned int result;
	
	//code
	printf("\n\n");
	printf("\nEnter First Integer : \n");
	scanf("%d",&a);
	
	printf("\n*********************OUTPUT********************\n");
	result = ~a;
	printf("\nOne's Complement of a = %d is %d\n",a,result);
	
	PrintbinaryFormOfNumber(a);
	PrintbinaryFormOfNumber(result);
	
	return 0;
}

void PrintbinaryFormOfNumber(unsigned int decimal_number)
{
	//variable declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;
	
	//code
	for(i = 0; i < 8; i++)
		binary_array[i] = 0;
	printf("\nThe binary form of Decimal Integer %d is \t = \t",decimal_number);
		
	num = decimal_number;
	i = 7;
	while(num != 0)
	{
		quotient = num/2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}
	
	for(i = 0; i < 8; i++)
		printf("\n%u", binary_array[i]);

	printf("\n");
}

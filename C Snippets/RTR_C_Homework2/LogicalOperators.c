#include <stdio.h>

int main(void)
{
	//variable declarations
	int a, b, c, result;
	
	//code
	printf("\n\n");
	printf("\nEnter First Integer : \n");
	scanf("%d", &a);
	
	printf("\nEnter Second Integer : \n");
	scanf("%d", &b);
	
	printf("\nEnter Third Integer : \n");
	scanf("%d", &c);
	printf("\n\n");
	
	printf("\n If Answer is = 0, it is FALSE.\n If Answer is = 1, it is TRUE\n");

	result = ((a <= b) && (b != c));
	printf("a = %d is less than equal to b = %d is %d AND b = %d Not equal to c = %d is %d.\nHence the final Answer is %d.\n ",a,b,a<=b,b,c,b!=c,result);
	
	result = ((b >= a) || (a == c));
	printf("\n b = %d is Greater than Equal to a = %d is %d OR a = %d is Equal to c = %d is %d.\nHence The final result is %d\n\n",b,a,b>=a,a,c,a==c,result);
	
	result = !a;
	printf("\nAfter negating the value for a =%d the result is %d\n",a,result);
	
	result = !b;
	printf("\nAfter negating the value for b =%d the result is %d\n",b,result);
	
	result = !c;
	printf("\nAfter negating the value for c =%d the result is %d\n\n",a,result);
	
	result = (!(a <= b) && !(b != c));
	printf("\nAfter negating  =>a = %d is less than equal to b = %d is %d AND After negating => b = %d Not equal to c = %d is %d.\nHence the final Answer is %d.\n ",a,b,a<=b,b,c,b!=c,result);
	
	result = (!(b >= a) || !(a == c));
	printf("\nAfter negating => b = %d is Greater than Equal to a = %d is %d OR After negating => a = %d is Equal to c = %d is %d.\nHence The final result is %d\n",b,a,b>=a,a,c,a==c,result);
	
	return(0);
}
#include<stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declarations
	struct MyStruct s;
	union MyUnion u;
	
	//Struct members' initializations
	s.i = 21;
	s.f = 3.14f;
	s.d = 45.5435435;
	s.c = 'S';
	
	//displaying the members of Struct MyStruct
	printf("\nThe values of struct MyStruct are :\n");
	printf("\nThe value of s.i = %d\n",s.i);
	printf("\nThe value of s.f = %f\n",s.f);
	printf("\nThe value of s.d = %lf\n",s.d);
	printf("\nThe value of s.c = %c\n",s.c);
	
	//displaying the addresses of Struct MyStruct
	printf("\nThe Addresses of struct members are : \n");
	printf("\nThe address of s.i = %p\n",&s.i);
	printf("\nThe address of s.f = %p\n",&s.f);
	printf("\nThe address of s.d = %p\n",&s.d);
	printf("\nThe address of s.c = %p\n",&s.c);
	
	//union member initializations
	printf("\nThe member values of Union are :\n");
	
	u.i = 51;
	printf("\nThe value of u.i = %d\n",u.i);
	
	u.f = 45.78f;
	printf("\nThe value of u.f = %f\n",u.f);
	
	u.d = 23.56534343;
	printf("\nThe value of u.d = %lf\n",u.d);
	
	u.c = 'U';
	printf("\nThe value of u.i = %c\n",u.c);
	
	printf("\nThe address values of Union are :\n");
	printf("\nThe address value of u.i is: %p\n",&u.i);
	printf("\nThe address value of u.f is: %p\n",&u.f);
	printf("\nThe address value of u.d is: %p\n",&u.d);
	printf("\nThe address value of u.c is: %p\n",&u.c);
	
	printf("\nThe address value of Union u is: %p\n",&u);
	
	return(0);
}
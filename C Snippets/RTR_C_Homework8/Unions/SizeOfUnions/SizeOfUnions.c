#include<stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;	
};

int main(void)
{
	//variable declarations
	struct MyStruct s;
	union MyUnion u;
	
	//code
	printf("\n\n");
	printf("\nSize of MyStruct %d\n",sizeof(s));
	printf("\nSize of MyUnion %d\n",sizeof(u));
	printf("\n\n");
	
	return(0);
}
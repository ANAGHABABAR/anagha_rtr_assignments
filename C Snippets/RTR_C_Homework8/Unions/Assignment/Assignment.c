#include<stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declarations
	union MyUnion u1, u2;
	
	//code
	//u1 members initialization
	u1.i = 21;
	u1.f = 3.14f;
	u1.d = 45.657667756;
	u1.c = 'U';
	
	//displaying members of u1
	printf("\n\n");
	printf("\nu1.i = %d\n",u1.i);
	printf("\nu1.f = %f\n",u1.f);
	printf("\nu1.d = %lf\n",u1.d);
	printf("\nu1.c = %c\n",u1.c);
	
	//displaying the addresses for u1's members
	printf("\n\n");
	printf("\nAddress of u1.i = %p\n",&u1.i);
	printf("\nAddress of u1.f = %p\n",&u1.f);
	printf("\nAddress of u1.d = %p\n",&u1.d);
	printf("\nAddress of u1.c = %p\n",&u1.c);
	
	printf("\nAddress of union u1 is = %p\n",&u1);
	
	//MyUnion u2
	printf("\n\n");
	
	u2.i = 51;
	printf("\nValue of u2.i is = %d\n",u2.i);
	
	u2.f = 51.67f;
	printf("\nValue of u2.f is = %f\n",u2.f);
	
	u2.d = 89.323423423;
	printf("\nValue of u2.d is = %lf\n",u2.d);
	
	u2.c = 'Y';
	printf("\nValue of u2.c is = %c\n",u2.c);
	
	//displaying the addresses for u1's members
	printf("\n\n");
	printf("\nAddress of u2.i = %p\n",&u2.i);
	printf("\nAddress of u2.f = %p\n",&u2.f);
	printf("\nAddress of u2.d = %p\n",&u2.d);
	printf("\nAddress of u2.c = %p\n",&u2.c);
	
	printf("\nAddress of u2 is = %p\n",&u2);
	printf("\n\n");
	return(0);
}
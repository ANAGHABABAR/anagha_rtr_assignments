#include<stdio.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEGINING 65//'A'

struct MyDataone
{
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo
{
	char cArray[CHAR_ARRAY_SIZE];
	char strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main()
{
	//variable declarations
	struct MyDataone data_one;
	struct MyDataTwo data_two;
	int i;
	
	//code 
	//piece meal assignment
	data_one.fArray[0] = 0.1f;
	data_one.fArray[1] = 1.2f;
	data_one.fArray[2] = 2.3f;
	data_one.fArray[3] = 3.4f;
	data_one.fArray[4] = 4.5f;
	
	//Loop assignment user input
	printf("\nEnter %d Integers : \n",INT_ARRAY_SIZE);
	for(i = 0; i < INT_ARRAY_SIZE; i++)
	{
		scanf("%d",&data_one.iArray[i]);
	}
	
	//Loop assignment hard coded
	for(i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		data_two.cArray[i] = (char)(i + ALPHABET_BEGINING);
	}
	
	//Piece meal assignment hard coded
	strcpy(data_two.strArray[0],"Welcome !!!");
	strcpy(data_two.strArray[1],"This ");
	strcpy(data_two.strArray[2],"Is ");
	strcpy(data_two.strArray[3],"ASTROMEDICOMP's ");
	strcpy(data_two.strArray[4],"Real ");
	strcpy(data_two.strArray[5],"Time ");
	strcpy(data_two.strArray[6],"Rendering ");
	strcpy(data_two.strArray[7],"Batch ");
	strcpy(data_two.strArray[8],"of ");
	strcpy(data_two.strArray[9],"Year 2020. ");
	
	//Displaying Data memebers of Struct DataOne and their values
	printf("\n\n");
	printf("\nMembers of 'struct DataOne' Along with their Assigned values are : \n");
	printf("\n\n");
	for(i = 0; i < INT_ARRAY_SIZE; i++)
	{
		printf("%d\t",data_one.iArray[i]);
	}
	
	printf("\n\n");
	printf("\nFloating point array (data_one.fArray[]) : \n");
	for(i = 0; i < FLOAT_ARRAY_SIZE; i++)
	{
		printf("%f\t",data_one.fArray[i]);
	}
	
	printf("\n\n");
	for(i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		printf("%c\t",data_two.cArray[i]);
	}
	

	printf("\n\n");
	for(i = 0; i < NUM_STRINGS; i++)
	{
		printf("%s ",data_two.strArray[i]);
	}
	
	printf("\n\n");
	return(0);
}
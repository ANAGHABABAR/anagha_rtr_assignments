#include<stdio.h>

struct MyPoint
{
	int x;
	int y;
};

struct Rectangle
{
	struct MyPoint point_01;
	struct MyPoint point_02;
};

struct Rectangle rect = {{2, 3},{5, 6}};


int main(void)
{
	//variable declarations
	int length, breadth, area;

	//code
	
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
	{
		length = -(length);
	}
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
	{
		breadth = -(breadth);
	}
	
	area = length * breadth;
	
	printf("\n\n");
	printf("\nLength of the Rectangle is : %d\n",length);
	printf("\nBreadth of the Rectangle is : %d\n",breadth);
	printf("\nArea of Rectangle is : %d\n",area);
	
	return(0);
}

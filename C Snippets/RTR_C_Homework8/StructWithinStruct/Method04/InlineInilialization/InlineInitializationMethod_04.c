#include<stdio.h>

int main()
{
	//variable declarations
	int length, breadth, area;
	
	struct MyPoint
	{
		int x;
		int y;
	};
	
	struct Rectangle
	{
		struct MyPoint point_01;
		struct MyPoint point_02;
	};
	
	struct Rectangle rect = { {3, 5}, {7,9}};
	

	printf("\n\n");
	
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
	{
		length = -(length);
	}
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
	{
		breadth = -(breadth);
	}
	
	area = length * breadth;
	
	printf("\n\n");
	printf("\nThe length of the Rectangle is = %d\n",length);
	printf("\nThe breadth of the Rectangle is = %d\n",breadth);
	printf("\nThe area of rectangle is = %d\n",area);
	return(0);
}
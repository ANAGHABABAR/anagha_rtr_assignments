#include<stdio.h>

int main(void)
{
	//variable declarations
	int length, breadth, area;

	struct Rectangle
	{
			struct MyPoint
			{
				int x;
				int y;
			}point_01,point_02;
	}rect = {{9 , 8},{7 , 6}};
	//code
	
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
	{
		length = -(length);
	}
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
	{
		breadth = -(breadth);
	}

	area = length * breadth;
	
	printf("\n\n");
	printf("\nLength of the Rectangle is : %d\n",length);
	printf("\nBreadth of the Rectangle is : %d\n",breadth);
	printf("\nArea of Rectangle is : %d\n",area);
	
	return(0);
}

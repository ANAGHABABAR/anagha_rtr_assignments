#include<stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int x;
		int y;
	}point_01, point_02;
}rect = {{2,3},{5,6}};

int main(void)
{
	//variable declarations
	int length, breadth, area;
	
	//code
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
	{
		length = -(length); //length = -1 * length;		
	}
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
	{
		breadth = -(breadth);
	}
	
	area = length * breadth;
	
	printf("\n\n");
	printf("\nLength of a rectangle is = %d\n",length);
	printf("\nBreadth of a rectangle is = %d\n",breadth);
	printf("\nArea of a rectangle is = %d\n",area);
	return(0);
}
#include<stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int x;
		int y;
	}point_01, point_02;
}rect;

int main()
{
	//variable declartions
	int length, breadth, area;
	
	//code
	printf("\n\n");
	printf("\nEnter leftmost X co-ordinate of rectangle\n");
	scanf("%d",&rect.point_01.x);
	
	printf("\nEnter Bottom most Y co-ordinate of rectangle\n");
	scanf("%d",&rect.point_01.y);
	
	printf("\n\n");
	printf("\nEnter Rightmost X co-ordinate of rectangle\n");
	scanf("%d",&rect.point_02.x);
	
	printf("\nEnter Top most Y co-ordinate of rectangle\n");
	scanf("%d",&rect.point_02.y);
	
	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
	{
		length = -(length); // i.e. length = -1 * length
	}
	
	breadth = rect.point_02.x - rect.point_01.x;
	if(breadth < 0)
	{
		breadth = -(breadth);
	}
	
	area = length * breadth;
	
	printf("\n\n");
	printf("\nLength of a rectangle is = %d\n",length);
	printf("\nBreadth of a rectangle is = %d\n",breadth);
	printf("\nArea of a rectangle is = %d\n",area);
	
	return(0);
}
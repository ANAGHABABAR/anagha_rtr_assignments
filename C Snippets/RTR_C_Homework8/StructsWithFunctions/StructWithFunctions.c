#include<stdio.h>

//Defining struct
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	//function prototype
	struct MyData AddStructMembers(struct MyData, struct MyData, struct MyData);
	
	//variable declarations
	struct MyData data1, data2, data3, answer_data;
	
	//code
	//*********data 1***********
	printf("\n\n\n\n");
	printf("\n*********DATA 1*********\n");
	printf("\nEnter integer value for 'i' of data1\n");
	scanf("%d",&data1.i);
	
	printf("\nEnter float value for 'f' of data1\n");
	scanf("%f",&data1.f);
	
	printf("\nEnter double value for 'd' of data1\n");
	scanf("%lf",&data1.d);
	
	printf("\nEnter char value for 'c' of data1\n");
	data1.c = getch();
	printf("%c",data1.c);
	
	printf("\n\n\n\n");
	printf("\n*********DATA 2*********\n");
	printf("\nEnter integer value for 'i' of data2\n");
	scanf("%d",&data2.i);
	
	printf("\nEnter float value for 'f' of data2\n");
	scanf("%f",&data2.f);
	
	printf("\nEnter double value for 'd' of data2\n");
	scanf("%lf",&data2.d);
	
	printf("\nEnter char value for 'c' of data2\n");
	data2.c = getch();
	printf("%c",data2.c);	
	
	printf("\n\n\n\n");
	printf("\n*********DATA 3*********\n");
	printf("\nEnter integer value for 'i' of data3n");
	scanf("%d",&data3.i);
	
	printf("\nEnter float value for 'f' of data3\n");
	scanf("%f",&data3.f);
	
	printf("\nEnter double value for 'd' of data3\n");
	scanf("%lf",&data3.d);
	
	printf("\nEnter char value for 'c' of data3\n");
	data3.c = getch();
	printf("%c",data3.c);	
	
	answer_data = AddStructMembers(data1, data2, data3);
	
	printf("\n\n\n\n");
	printf("\nThe addition of struct members is : \n");
	printf("\nThe addition of integers of data1, data2, data3 : %d\n",answer_data.i);
	printf("\nThe addition of float of data1, data2, data3 : %f\n",answer_data.f);
	printf("\nThe addition of double of data1, data2, data3 : %lf\n",answer_data.d);
	
	answer_data.c = data1.c;
	printf("\nanswer_data.c (from data1) = %c\n",answer_data.c);
	
	answer_data.c = data2.c;
	printf("\nanswer_data.c (from data2) = %c\n",answer_data.c);
	
	answer_data.c = data3.c;
	printf("\nanswer_data.c (from data3) = %c\n",answer_data.c);
	
	return(0);
}

struct MyData AddStructMembers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
	//variable declarations
	struct MyData answer;
	
	answer.i = md_one.i + md_two.i + md_three.i;	
	answer.f = md_one.f + md_two.f + md_three.f;
	answer.d = md_one.d + md_two.d + md_three.d;
	
	return(answer);
}
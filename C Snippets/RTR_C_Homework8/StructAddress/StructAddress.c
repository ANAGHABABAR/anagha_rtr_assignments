#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	//variable declarations
	struct MyData data;
	
	//code
	//Assigning data values to the members of struct MyData
	data.i = 11;
	data.f = 3.14f;
	data.d = 64.123456789;
	data.c = 'A';
	
	//Displaying the Data members' values
	printf("\nThe value of 'i' is = %d\n",data.i);
	printf("\nThe value of 'f' is = %f\n",data.f);
	printf("\nThe value of 'd' is = %lf\n",data.d);
	printf("\nThe value of 'c' is = %c\n",data.c);
	
	//Displaying addresses of above assigned values
	printf("\nThe value of 'i' is = %p\n",&data.i);
	printf("\nThe value of 'f' is = %p\n",&data.f);
	printf("\nThe value of 'd' is = %p\n",&data.d);
	printf("\nThe value of 'c' is = %p\n",&data.c);
	
	printf("\nAddress of Struct MyData starting at = %p\n",&data);
	
	
	return(0);
}
#include<stdio.h>
#include<ctype.h>

#define NUM_EMPLOYEES 5

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char gender;
	float salary;
	char marital_status;
};

int main(void)
{
	//function prototype
	void MyGetString(char[], int);
	
	//variable declarations
	struct Employee EmployeeRecord[NUM_EMPLOYEES];//An array of NUM_EMPLOYEES structs - Each being a type of 'struct Employee'
	int i;
	
	//code
	for(i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("\n\n");
		printf("\n** Data Entry for Employee Number %d\n",(i+1));
		printf("\n\n");
		printf("\nEnter Employee name : \n");
		MyGetString(EmployeeRecord[i].name,NAME_LENGTH);
		
		printf("\n\n");
		printf("\nEnter Employee's Age in years : ");
		scanf("%d",&EmployeeRecord[i].age);
		
		printf("\n\n");
		printf("\nEnter employee's gender as (M/m for Male, F/f for Female) : \n");
		EmployeeRecord[i].gender = getch();
		printf("%c",EmployeeRecord[i].gender);//We are printing this bcz input will be accepted but will not be displayed while accepting. In order to display the accepted input we have printed it. e.g if entered F or f on the input screen, it won't get printed while accepting.
		EmployeeRecord[i].gender = toupper(EmployeeRecord[i].gender);
		
		printf("\n\n");
		printf("\nEnter Employee's salary in INR \n");
		scanf("%f",&EmployeeRecord[i].salary);
		
		printf("\n\n");
		printf("\nIs Employee married? (Y/y for Yes, N/n for No) : \n");
		EmployeeRecord[i].marital_status = getch();
		printf("%c",EmployeeRecord[i].marital_status);
		EmployeeRecord[i].marital_status = toupper(EmployeeRecord[i].marital_status);	
		
	}//End of Input Acceptance loop
	
	//Display accepted records
	for(i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("\n\n");
		printf("Name of an Employee : %s\n",EmployeeRecord[i].name);
		printf("Age : %d\n",EmployeeRecord[i].age);
		
		if(EmployeeRecord[i].gender == 'M' || EmployeeRecord[i].gender == 'm')
		{
			printf("Gender : Male\n");
		}
		else if(EmployeeRecord[i].gender == 'F' || EmployeeRecord[i].gender == 'f')
		{
			printf("Gender : Female\n");
		}
		
		if(EmployeeRecord[i].marital_status == 'Y' || EmployeeRecord[i].marital_status == 'y')
		{
			printf("Marital status : Married");
		}
		else if(EmployeeRecord[i].marital_status == 'N' || EmployeeRecord[i].marital_status == 'n')
		{
			printf("Marital status : Unmarried\n");
		}
		
		printf("Salary : %f\n",EmployeeRecord[i].salary);
	}
	
	return(0);
}

//Simple rudimentary implementation of gets_s()
//Implemented due to different behaviour of gets_s()/ fgets() / fscanf() on different platforms
//Backspace / character deletion and Arow key cursor movement not implemented

void MyGetString(char str[], int strSize)
{
	//variable declarations
	int i;
	char ch = '\0';
	
	//code
	i = 0;
	do
	{
		ch = getch();
		str[i] = ch;
		printf("%c",str[i]);
		i++;
	}while((ch != '\r') && (i < strSize));
	
	if(i == strSize)
	{
		str[i - 1] = '\0';
	}
	else
	{
		str[i] = '\0';
	}
}
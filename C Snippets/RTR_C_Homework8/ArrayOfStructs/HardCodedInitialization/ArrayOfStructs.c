#include<stdio.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char gender;
	char marital_status[MARITAL_STATUS];
	
};

int main(void)
{
	//variaable declarations
	struct Employee EmployeeRecord[5];//An array of 5 structs - Each being type struct Employee
	
	char Employee_Anagha[] = "Anagha";
	char Employee_Sarojini[] = "Sarojini";
	char Employee_Surya[] = "Surya";
	char Employee_Paulo[] = "Paulo";
	char Employee_Sundar[] = "Sundar";
	
	int i;
	
	//code
	//hard coded initialization of Struct Employee
	
	//Employee 1
	strcpy(EmployeeRecord[0].name,Employee_Anagha);
	EmployeeRecord[0].age = 24;
	EmployeeRecord[0].salary = 900000.0f;
	EmployeeRecord[0].gender = 'F';
	strcpy(EmployeeRecord[0].marital_status,"Single");
	
	
	//Employee 2
	strcpy(EmployeeRecord[1].name,Employee_Sarojini);
	EmployeeRecord[1].age = 24;
	EmployeeRecord[1].salary = 600000.0f;
	EmployeeRecord[1].gender = 'F';
	strcpy(EmployeeRecord[1].marital_status,"Single");
	
	//Employee 3
	strcpy(EmployeeRecord[2].name,Employee_Surya);
	EmployeeRecord[2].age = 35;
	EmployeeRecord[2].salary = 80000.0f;
	EmployeeRecord[2].gender = 'M';
	strcpy(EmployeeRecord[2].marital_status,"Married");
	
	//Employee 4
	strcpy(EmployeeRecord[3].name,Employee_Paulo);
	EmployeeRecord[3].age = 39;
	EmployeeRecord[3].salary = 7000.0f;
	EmployeeRecord[3].gender = 'M';
	strcpy(EmployeeRecord[3].marital_status,"Married");
	
	//Employee 5
	strcpy(EmployeeRecord[4].name,Employee_Sundar);
	EmployeeRecord[4].age = 54;
	EmployeeRecord[4].salary = 79000.0f;
	EmployeeRecord[4].gender = 'M';
	strcpy(EmployeeRecord[4].marital_status,"Married");
	
	//displaying the employee details
	printf("\n\n");
	printf("\nDisplaying Employee Records\n");
	for(i = 0; i < 5; i++)
	{
		printf("\nName : %s",EmployeeRecord[i].name);
		if(EmployeeRecord[i].gender == 'F')
		{
			printf("\n Gender : Female");
		}
		else if(EmployeeRecord[i].gender == 'M')
		{
			printf("\nGender : Male");
		}
		
		printf("\nAge : %d",EmployeeRecord[i].age);
		printf("\nSalary : %f",EmployeeRecord[i].salary);
		printf("\nMarital status : %s",EmployeeRecord[i].marital_status);
		printf("\n******************************************************\n");
	}
	
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	int a,b,x;
	
	//code
	printf("\nEnter 2 numbers : ");
	scanf("%d %d",&a,&b);
	
	//We compound assignement operators when operator on LHS of assignement is immediately repeated on RHS of assignement
	//We are using compound assignement operators as +=, -= , *=, /=, %= 
	
	x = a;
	a += b; //(a =a+b)
	
	printf("\nThe addition of %d & %d is %d\n",x,b,a);
	
	x = a;
	a -= b;
	printf("\nThe subtraction of %d & %d is %d\n",x,b,a);
	
	x = a;
	a *= b;
	printf("\nThe multiplication of %d & %d is %d\n",x,b,a);
	
	x = a;
	a /= b;
	printf("\nThe Division of %d & %d is %d\n",x,b,a);
	
	x =a;
	a %= b;
	printf("\nThe Division of %d & %d gives Remainder %d\n",x,b,a);


	
	return(0);
}
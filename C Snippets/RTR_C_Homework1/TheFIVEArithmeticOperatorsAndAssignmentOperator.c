#include<stdio.h>

int main(void)
{
	//variable declarations	
	int a;
	int b;
	int result;
	
	//code
	printf("\n\n");
	printf("Enter a number : ");
	scanf("%d",&a);
	
	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d",&b);
	
	printf("\n\n");
	//following are the five arithmetic operators as +, -, *, /, %
	
	result = a+b;
	printf("\nThe addition is %d",result);
	
	result = a-b;
	printf("\nThe Subtraction is %d",result);
	
	result = a*b;
	printf("\nThe Multiplication is %d",result);
	
	result = a/b;
	printf("\nThe Division is %d",result);
	
	result = a%b;
	printf("\nThe remainder is %d",result);
	
	printf("\n");
	return(0);
}
#include<stdio.h>

#define ARB_PI 3.1415926535897932

#define ARB_STRING "AstroMediComp Real Time Rendering 3.0"

//If first constant has no assigned value then it is by deafult considered as 0. i.i.''SUNDAY' will be 0
//And the rest of the constants are assigned Consecutive Integer Values From 0 onwards  .i.e. MONDAY will be assumed to be 1, 'TUESADAY' as 2 and so on

//un-named enums
enum
{
		SUNDAY,
		MONDAY,
		TUESDAY,
		WEDNESDAY,
		THURSDAY,
		FRIDAY,
		SATURDAY
		
};

enum
{
	JANUARY =1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

//Named enums
enum Numbers
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};


enum boolean
{
	TRUE = 1,
	FALSE = 0
};

int main(void)
{
	//local constant declarations
	const double epsilon = 0.000001;
	
	//code
	printf("\n\n");
	printf("Local Constant Epsilon = %lf\n\n",epsilon);
	
	printf("SUNDAY is DAY NUMBER = %d\n",SUNDAY);
	printf("MONDAY is DAY NUMBER = %d\n",MONDAY);
	printf("TUESDAY is DAY NUMBER = %d\n",TUESDAY);
	printf("WEDNESDAY is DAY NUMBER = %d\n",WEDNESDAY);
	printf("THURSDAY is DAY NUMBER = %d\n",THURSDAY);
	printf("FRIDAY is DAY NUMBER = %d\n",FRIDAY);
	printf("SATURDAY is DAY NUMBER = %d\n",SATURDAY);
	printf("\n");
	printf("ONE is ENUM NUMBER = %d\n",ONE);
	printf("TWO is ENUM NUMBER = %d\n",TWO);
	printf("THREE is ENUM NUMBER = %d\n",THREE);
	printf("FOUR is ENUM NUMBER = %d\n",FOUR);
	printf("FIVE is ENUM NUMBER = %d\n",FIVE);
	printf("SIX is ENUM NUMBER = %d\n",SIX);
	printf("SEVEN is ENUM NUMBER = %d\n",SEVEN);
	printf("EIGHT is ENUM NUMBER = %d\n",EIGHT);
	printf("NINE is ENUM NUMBER = %d\n",NINE);
	printf("TEN is ENUM NUMBER = %d\n",TEN);
	printf("\n");
	printf("JANUARY is MONTH NUMBER = %d\n",JANUARY);
	printf("FEBRUARY is MONTH NUMBER = %d\n",FEBRUARY);
	printf("MARCH is MONTH NUMBER = %d\n",MARCH);
	printf("APRIL is MONTH NUMBER = %d\n",APRIL);
	printf("MAY is MONTH NUMBER = %d\n",MAY);
	printf("JUNE is MONTH NUMBER = %d\n",JUNE);
	printf("JULY is MONTH NUMBER = %d\n",JULY);
	printf("AUGUST is MONTH NUMBER = %d\n",AUGUST);
	printf("SEPTEMBER is MONTH NUMBER = %d\n",SEPTEMBER);
	printf("OCTOBER is MONTH NUMBER = %d\n",OCTOBER);
	printf("NOVEMBER is MONTH NUMBER = %d\n",NOVEMBER);
	printf("DECEMBER is MONTH NUMBER = %d\n",DECEMBER);
	printf("\n");
	printf("The value of TRUE is = %d\n",TRUE);
	printf("The Value of FALSE is = %d\n",FALSE);
	printf("\n");
	printf("ARB_PI Macro value is = %lf\n",ARB_PI);
	printf("Area of Circle of Radius 4 units = %f\n\n", (ARB_PI * 4.0f * 4.0f));
	
	printf("\n\n");
	printf("ARB_STRING is : %s\n",ARB_STRING);
	printf("\n\n");
	
	return(0);
}
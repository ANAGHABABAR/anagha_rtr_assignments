#include<stdio.h>

int main(void)
{
	//variable declarations
	int a = 5;
	int b = 10;
	
	//code
	printf("\n");
	printf("\nOriginal a = %d",a);
	printf("\nValue of a++ = %d",a++);
	printf("\nAfter a++ completion a = %d",a);
	printf("\nValue of ++a = %d",++a);
	printf("\n");
	
	printf("\n");
	printf("\nOriginal b = %d",a);
	printf("\nValue of a-- = %d",a--);
	printf("\nAfter a-- completion a = %d",a);
	printf("\nValue of --a = %d",--a);
	printf("\n");
	


	return(0);
}
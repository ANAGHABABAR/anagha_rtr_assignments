#include<stdio.h>
#include<stdlib.h>

int main()
{
	//variable declarations
	int **ptr_iArray = NULL;//A pointer-to-pointer to integer but can also hold base address of a 2D array which will can have any number of rows and any number of columns
	
	int i,j;
	int num_rows, num_columns;
	
	//code
	//Accept size of rows from user
	printf("\nEnter how many rows do you want? :");
	scanf("%d",&num_rows);
	
	//Accept size of columns from user
	printf("\nEnter how many columns do you want? :");
	scanf("%d",&num_columns);
	
	//allocating memory to 1D array cnsisting of base address of rows
	printf("\n\n");
	printf("\n****Memory allocation to 2D integer array****\n");
	
	ptr_iArray = (int **)malloc(sizeof(int *) * num_rows);
	if(ptr_iArray == NULL)
	{
		printf("\nMemory allocation failure occurred..for %d rows",num_rows);
		exit(0);
	}
	else
	{
		printf("\nMemory allocation was successful for %d rows..",num_rows);
	}
	
	for(i = 0; i < num_rows; i++)
	{
		ptr_iArray[i] = (int *)malloc(sizeof(int) * num_columns);
		if(ptr_iArray[i] == NULL)
		{
			printf("\nMemory allocation for %d column failed..",i);
			exit(0);
		}
		else
		{
			printf("\nMemory allocation for %d column was successful.",i);
		}
	}
	
	//filling values
	for(i = 0; i< num_rows; i++)
	{
		for(j = 0; j < num_columns; j++)
		{
			ptr_iArray[i][j] = (i + 1) * (j + 1);
		}
	}
	
	//displaying values
	for(i = 0; i < num_rows; i++)
	{
		printf("\nBase address of Row %d : ptr_iArray[%d] = %p\t at address : %p\n",i ,i,ptr_iArray,&ptr_iArray[i]);
	}
	
	printf("\n\n");
	
	for(i = 0; i < num_rows; i++ )
	{
		for(j = 0; j < num_columns; j++)
		{
			printf("\nptr_iArray[%d][%d] = %d \t at address : %p\n",i,j,ptr_iArray[i][j],&ptr_iArray[i][j]);//can also use *(*(ptr_iArray + i) + j) for value and *(ptr_iArray + i) + j for address 
		}
		printf("\n\n");
	}
	
	//freeing allocated memory
	for(i = (num_rows - 1); i >= 0; i--)
	{
		if(ptr_iArray[i])
		{
			free(ptr_iArray[i]);
			ptr_iArray[i] = NULL;
			printf("\nMemory successfully freed for % row",ptr_iArray[i]);
		}
	}
	
	if(ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		printf("\nMemory successfully freed..");
		
	}
	
	return(0);
}
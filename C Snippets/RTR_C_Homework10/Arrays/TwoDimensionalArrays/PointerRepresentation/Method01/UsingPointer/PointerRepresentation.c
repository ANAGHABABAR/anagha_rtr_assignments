#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//variable declarations
	int iArray[NUM_ROWS][NUM_COLUMNS];
	int i,j;
	
	int *ptr_iArray_Row = NULL;
	
	//code
	//Every row of a 2D array is an integer array itself comprising of 'NUM_COLUMNS' integer elements
	//There are 5 rows and 3 columns in a 2D integer array. Each of 5 rows is a 1D Array of 3 integers
	//hence, each of these 5 rows themselves being arrays, will be the base addresses of their respective rows
	
	for(i = 0; i < NUM_ROWS; i++)
	{
		ptr_iArray_Row = iArray[i]; //iArray[i] is the base address of ith row
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			*(ptr_iArray_Row + j) = (i + 1) * (j + 1);//ptr_iArray_Row (iArray[i] can be treated as 1D array using pointers)
		}
	}
	
	printf("\n\n");
	printf("\n2D integer array elements along with their addresses are :\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		ptr_iArray_Row = iArray[i];
		printf("\nptr_iArray_Row = %p\n",ptr_iArray_Row);
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("*(ptr_iArray_Row + %d) = %d \t\tat adress (ptr_iArray_Row + j) : %p\n",j,*(ptr_iArray_Row + j),(ptr_iArray_Row + j));
		}
		printf("\n\n");
	}
	
	return(0);
}
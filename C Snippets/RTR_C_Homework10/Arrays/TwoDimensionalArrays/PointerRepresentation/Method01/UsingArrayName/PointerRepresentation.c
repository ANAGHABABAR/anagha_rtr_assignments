#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main()
{
	//variable declarations
	int iArray[NUM_ROWS][NUM_COLUMNS];
	int i,j;
	
	//code
	//Evry row of a 2D array is an integer array itself comprising of NUM_COLUMNS integer elements
	//there are 5 rows and 3 columns. each of the 5 rows is a 1d array of 3 integers
	//hence, each of these 5 rows themselves being arrays, will be the base addresses of their respective rows
	
	for(i = 0; i <NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			*(iArray[i] + j) = (i + 1) * (j + 1);//'iArray[i]' can be treated as 1D array using pointers
		}
	}
	
	printf("\n\n");
	printf("2D Integer array elements along with addresses : \n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("*(iArray[%d] + %d) = %d \t\t At address (iArray[i] + j) : %p \n",i,j,*(iArray[i]+j), (iArray[i] + j));
		}
		printf("\n\n");
	}
	
	return(0);
}
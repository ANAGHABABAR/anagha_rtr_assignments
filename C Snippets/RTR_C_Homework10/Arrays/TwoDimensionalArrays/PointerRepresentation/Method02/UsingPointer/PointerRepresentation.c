#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main()
{
	//variable declarations
	int i, j;
	int **ptr_iArray =NULL;
	
	//code
	printf("\n\n");
	printf("Memory allocation\n");
	
	ptr_iArray = (int **)malloc(sizeof(int *) * NUM_ROWS);
	if(ptr_iArray == NULL)
	{
		printf("\nMemory allocation for rows has been failed");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation to %d rows was successful!!",NUM_ROWS);
	}
	
	//allocating memory to each row
	for(i = 0; i < NUM_ROWS; i++)
	{
		ptr_iArray[i] = (int *)malloc(sizeof(int) * NUM_COLUMNS);
		if(ptr_iArray == NULL)
		{
			printf("\nMemory allocation for Columns has been failed!");
		}
		else
		{
			printf("\nMemory allocation for %d columns of each row was successful!!",NUM_COLUMNS);
		}
	}
	
	//assigning values
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			*(*(ptr_iArray + i) + j) = (i + 1) * (j + 1);//ptr_iArray[i][j] = (i + 1) * (j + 1);
		}
	}		
	
	//displaying values
	printf("\n\n");
	printf("2D Integer array elements with their addresses are as follows:");
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("\nptr_iArray[%d][%d] = %d\tAt address &ptr_iArray[%d][%d]: %p",i,j,ptr_iArray[i][j],i,j,&ptr_iArray[i][j]);
		}
	}

	//freeing the allocated memory
	for(i = 0; i < NUM_ROWS; i++)
	{
		if(*(ptr_iArray + i))//if(ptr_iArray[i])
		{
			free(*(ptr_iArray + i));
			*(ptr_iArray + i) = NULL;
			
			printf("\nMemory for %d row freed successfully..\n",i);
		}
	}
	
	if(ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		
		printf("\nMemory for ptr_iArray freed successfully..");
	}
	
	return(0);
}
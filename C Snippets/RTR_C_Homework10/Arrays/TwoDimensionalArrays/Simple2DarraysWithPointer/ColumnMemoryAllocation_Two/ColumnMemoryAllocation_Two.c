#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS_ONE 3
#define NUM_COLUMNS_TWO 8

int main()
{
	//variable declarations
	int *iArray[NUM_ROWS];
	int i,j;
	
	//code
	//ONE (Allocatng memory for an array of 3 integers per row)
	printf("\n\n");
	printf("\n**First memory allocation to 2D integer array**\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		iArray[i] = (int *)malloc(sizeof(int) * NUM_COLUMNS_ONE);
		if(iArray[i] == NULL)
		{
			printf("\nMemory allocation failed for %d row",i);
			exit(0);
		}
		else
		{
			printf("\nMemory allocation for %d was successful..",i);
		}
	}
	
	//Assigning values 2D array
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS_ONE; j++)
		{
			iArray[i][j] = (i + 1) * (j + 1);
		}
	}
	
	//Displaying 2D array elements
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS_ONE; j++)
		{
			printf("iArray[%d][%d] = %d",i,j,iArray[i][j]);
		}
		printf("\n\n");
	}
	printf("\n\n");
	
	
	//freeing above alllocated memory(freeing of memory must be done in reverese order)
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		printf("iArray[%d]..freed",i);
		free(iArray[i]);
		iArray[i] = NULL;
		printf("\nMemory allocated to %d row has been deallocated successfully..\n",i);
	}
	
	//TWO (Allocatng memory for an array of 3 integers per row)
	printf("\n\n");
	printf("\n**Second memory allocation to 2D integer array**\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		iArray[i] = (int *)malloc(sizeof(int) * NUM_COLUMNS_TWO);
		if(iArray[i] == NULL)
		{
			printf("\nMemory allocation failed for %d row",i);
		}
		else
		{
			printf("\nMemory allocation for %d was successful..",i);
		}
	}
	
	//Assigning values 2D array
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS_TWO; j++)
		{
			iArray[i][j] = (i + 1) * (j + 1);
		}
	}
	
	//Displaying 2D array elements
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS_TWO; j++)
		{
			printf("iArray[%d][%d] = %d",i,j,iArray[i][j]);
		}
		printf("\n\n");
	}
	printf("\n\n");
	
	
	//freeing above alllocated memory(freeing of memory must be done in reverese order)
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		printf("iArray[%d]..freed",i);
		free(iArray[i]);
		iArray[i] = NULL;
		printf("\nMemory allocated to %d row has been deallocated successfully..\n",i);
	}
	
	return(0);
}
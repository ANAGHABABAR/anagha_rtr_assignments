#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main()
{
	//variable declarations
	int *iArray[NUM_ROWS]; //A 2D array which has 5 rows and number of rows can be decided later on..
	int i,j;
	
	//code
	printf("\n\n");
	for(i = 0; i < NUM_ROWS; i++)
	{
		iArray[i] = (int *)malloc(sizeof(int) * NUM_COLUMNS);
		if(iArray[i] == NULL)
		{
			printf("\nMemory allocation failed for %d rows..",NUM_ROWS);
			exit(0);
		}
		else
		{
			printf("\nMemory allocation for %d row was successful..",i);
		}
	}
	
	//assigning values to 2D arrays
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			iArray[i][j] = (i + 1) * (j + 1);
		}
	}
	
	//Displaying 2D array elements
	for(i = 0; i < NUM_ROWS; i++)
	{
		for(j = 0; j < NUM_COLUMNS; j++)
		{
			printf("iArray[%d][%d] = %d",i,j,iArray[i][j]);
		}
		printf("\n\n");
	}
	printf("\n\n");
	
	//freeing above alllocated memory(freeing of memory must be done in reverese order)
	
	for(i = (NUM_ROWS - 1); i >= 0; i--)
	{
		printf("iArray[%d]..freed",i);
		free(iArray[i]);
		iArray[i] = NULL;
		printf("\nMemory allocated to %d row has been deallocated successfully..\n",i);
	}

	return(0);
}
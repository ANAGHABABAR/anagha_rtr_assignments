#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 5

int main()
{
	//variable declarations
	 int *iArray[NUM_ROWS];
	 int i,j;
	 
	 //code
	 printf("\n\n");
	 printf("\n***MEMORY ALLOCATION TO AN INTEGER ARRAY***\n");
	 for(i = 0; i < NUM_ROWS; i++)
	 {
		 //Row 0 will have (NUM_COLUMNS - 0) = 5 - 0 = 5 rows
		 //Row 1 will have (NUM_COLUMNS - 1) = 5 - 1 = 4 rows
		 //Row 2 will have (NUM_COLUMNS - 2) = 5 - 2 = 3 rows
		 //Row 3 will have (NUM_COLUMNS - 3) = 5 - 3 = 2 rows
		 //Row 4 will have (NUM_COLUMNS - 4) = 5 - 4 = 1 rows
		 
		 //Because of this there is no contiguous memory allocation . Hence, although we may use the data as a 2D array , it is not really a 2D array in memory
		 
		 iArray[i] = (int*)malloc(sizeof(int) * (NUM_COLUMNS - i));
		 if(iArray[i] == NULL)
		 {
			 printf("\nMemory allocation failed for row %d. ",i);
			 exit(0);
		 }
		 else
		 {
			 printf("\nMemory allocation for %d row was successful.",i);
		 }
	 }
	 
	 //Assigning values 2D array
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < (NUM_COLUMNS - i); j++)
		{
			iArray[i][j] = (i + 1) * (j + 1);
		}
	}
	
	//Displaying values 2D array
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < (NUM_COLUMNS - i); j++)
		{
			printf("iArray[%d][%d] = %d \t at Address : %p\n",i,j,iArray[i][j],&iArray[i][j]);
		}
		printf("\n");
	}
	
	for(i = (NUM_ROWS - 1); i >= 0 ; i--)
	{
		if(iArray)
		{
			free(iArray[i]);
			iArray[i] = NULL;
			printf("\nMemory deallocation was successful!!\n");
		}	
	}
	
	return(0);
}
#include<stdio.h>
#include<stdlib.h>//contains prototypes of malloc and free

int main()
{
	//variable declarations
	int *ptr_iArray = NULL;
	
	unsigned int iArrayLength = 0;
	int i;
	
	//code
	printf("\n\n");
	printf("Enter the size if an Integer Array : \n");
	scanf("%d",&iArrayLength);
	
	///malloc will allocate the said amount of memory and will return the INITIAL/STARTING/BASE address, which must be captured in a Pointer Variable. Using this base address an array can be accessed and used

	ptr_iArray = (int*)malloc(sizeof(int) * iArrayLength);
	
	if(ptr_iArray == NULL)
	{
		printf("\nMemory Allocation failed\n\n Exiting on Failure\n");
		exit(0);
	}
	
	else//memory allocation was successful and base address is returned by a malloc into the '*ptr_iArray' pointer which is of type Integer
	{
		printf("\nMemory allocation was successful!\n");
		printf("\nMemory Addresses from %p to %p have been allocated to integer array!\n\n",ptr_iArray,(ptr_iArray + (iArrayLength - 1)));
	}
	
	printf("\n\n");
	printf("\nEnter %d elements for Integer array: \n\n",iArrayLength);
	for(i = 0; i < iArrayLength; i++)
	{
		scanf("%d",&ptr_iArray[i]);
	}
	
	printf("\n\n");
	printf("\nElements that you have entered are :\n",iArrayLength);
	for(i = 0; i < iArrayLength; i++)
	{
		printf("\n*(ptr_iArray + %d) = %d\t\t At address (ptr_iArray + %d )= %p\n",i,*(ptr_iArray + i),i,(ptr_iArray + i));
	}
	
	
	//freeing above allocated memory
	if(ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		
		printf("\n\n");
		printf("\nMemory has been successfully freed for above allocation!\n");
	}
	
	return(0);
}
#include<stdio.h>
#include<stdlib.h>

#define INT_SIZE sizeof(int)
#define FLOAT_SIZE sizeof(float)
#define DOUBLE_SIZE sizeof(double)
#define CHAR_SIZE sizeof(char)

int main()
{
	int *ptr_iArray = NULL;
	unsigned int iArrayLength = 0;
	
	float *ptr_fArray = NULL;
	unsigned int fArrayLength = 0;
	
	double *ptr_dArray = NULL;
	unsigned int dArrayLength = 0;
	
	char *ptr_cArray = NULL;
	unsigned int cArrayLength = 0;
	
	int i;
	
	//code
	
	//Integer Array
	printf("\n\n");
	printf("\nEnter the size of an integer array :\n");
	scanf("%d",&iArrayLength);
	
	ptr_iArray = (int *)malloc(INT_SIZE * iArrayLength);
	
	if(ptr_iArray == NULL)
	{
		printf("\nMemory Allocation for Integer Array failure occurred and hence exiting..\n");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation was successful..!\n");
	}
	
	printf("\n\n");
	printf("Enter %d elements in an Integer Array : ",iArrayLength);
	for(i = 0; i < iArrayLength; i++)
	{
		scanf("%d",(ptr_iArray + i));
	}
	
	//Float array
	printf("\n\n");
	printf("\nEnter the size of Float array : ");
	scanf("%d",&fArrayLength);
	
	ptr_fArray = (float *)malloc(FLOAT_SIZE * fArrayLength);
	if(ptr_fArray == NULL)
	{
		printf("\nMemory Allocation for Floating point Array failure occurred and hence exiting..\n");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation to Float array was successful..!\n");
	}
	
	printf("\nEnter %d elements for Float array : ",fArrayLength);
	for(i = 0; i < fArrayLength; i++)
	{
		scanf("%f",(ptr_fArray + i));
	}
	
	//Double array
	printf("\n\n");
	printf("\nEnter the size of Double array : ");
	scanf("%d",&dArrayLength);
	
	ptr_dArray = (double *)malloc(DOUBLE_SIZE * dArrayLength);
	if(ptr_dArray == NULL)
	{
		printf("\nMemory Allocation for Double Array failure occurred and hence exiting..\n");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation to Double array was successful..!\n");
	}
	
	printf("\nEnter %d elements for Double array : ",dArrayLength);
	for(i = 0; i < dArrayLength; i++)
	{
		scanf("%lf",(ptr_dArray + i));
	}
	
	//CHAR array
	printf("\n\n");
	printf("\nEnter the size of Character array : ");
	scanf("%d",&cArrayLength);
	
	ptr_cArray = (char *)malloc(CHAR_SIZE * cArrayLength);
	if(ptr_cArray == NULL)
	{
		printf("\nMemory Allocation for Character Array failure occurred and hence exiting..\n");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation to character array was successful..!\n");
	}
	
	printf("\nEnter %d elements for Character array : ",cArrayLength);
	for(i = 0; i < cArrayLength; i++)
	{
		*(ptr_cArray + i) = getch();
		printf("%c",*(ptr_cArray + i));
	}
	
	//Display of arrays
	
	//Integer Array
	printf("\n\n");
	printf("\n %d elements in an integer array are as :\n",iArrayLength);
	for(i = 0; i < iArrayLength; i++)
	{
		printf("\n%d \t\t at Address : %p \n",*(ptr_iArray + i),(ptr_iArray + i));
	}
	
	//FLOAT ARRAY
	printf("\n %d elements in an floating point array are as :\n",fArrayLength);
	for(i = 0; i < fArrayLength; i++)
	{
		printf("\n%f \t\t at Address : %p \n",*(ptr_fArray + i),(ptr_fArray + i));
	}
	
	//DOUBLE ARRAY
	printf("\n %d elements in an double array are as :\n",dArrayLength);
	for(i = 0; i < dArrayLength; i++)
	{
		printf("\n%lf \t\t at Address : %p \n",*(ptr_dArray + i),(ptr_dArray + i));
	}
	
	//CHARACTER ARRAY
	printf("\n %d elements in an character array are as :\n",cArrayLength);
	for(i = 0; i < cArrayLength; i++)
	{
		printf("\n%c \t\t at Address : %p \n",*(ptr_cArray + i),(ptr_cArray + i));
	}
	
	//FREEING above allocated memory
	if(ptr_cArray)
	{
		free(ptr_cArray);
		ptr_cArray = NULL;
		
		printf("\nMemory allocated for Character array is freed!!");
	}
	
	if(ptr_dArray)
	{
		free(ptr_dArray);
		ptr_dArray = NULL;
		
		printf("\nMemory allocated for Double array is freed!!");
	}
	
	if(ptr_fArray)
	{
		free(ptr_fArray);
		ptr_fArray = NULL;
		
		printf("\nMemory allocated to Float array is freed!!");
	}
	
	if(ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		
		printf("\nMemory allocated to Integer array is freed!!");
	}
	
	return(0);
}
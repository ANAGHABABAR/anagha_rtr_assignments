#include<stdio.h>

int main(void)
{
	//function declarations
	void MathOperations(int, int, int*, int*, int*, int*, int*);
	
	//variable declarations
	int a,b;
	int answer_sum, answer_difference,answer_product,answer_quotient,answer_remainder;

	//code
	printf("\n\n");
	printf("\n\n");
	printf("Enter a value of A: \n");
	scanf("%d",&a);
	
	printf("\n\n");
	printf("Enter a value of B: \n");
	scanf("%d",&b);
	
	MathOperations(a, b, &answer_sum, &answer_difference, &answer_product, &answer_quotient, &answer_remainder);
	
	printf("\n\n");
	printf("\n*********** Results ************\n");	 
	printf("The sum of %d and %d is = %d \n",a,b,answer_sum);
	printf("The difference of %d and %d is = %d \n",a,b,answer_difference);
	printf("The product of %d and %d is = %d \n",a,b,answer_product);
	printf("The quotient of %d and %d on Dividing is = %d , \n",a,b,answer_quotient);
	printf("The remainder of %d and %d is = %d \n",a,b,answer_remainder);
		
	return(0);
}

void MathOperations(int x, int y,int *sum, int *difference,int *product,int *quotient, int *remainder, )
{
	//code
	*sum = x+y;
	*difference = x-y;
	*product = x*y;
	*quotient = x/y;
	*remainder = x%y;	
}
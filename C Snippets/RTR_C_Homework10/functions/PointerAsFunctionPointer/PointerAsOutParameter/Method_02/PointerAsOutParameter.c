#include<stdio.h>
#include<stdlib.h>

int main()
{
	//function declarations
	void MathOperations(int, int, int*, int*, int*, int*, int*);
	
	//variable declarations
	int a,b;
	int *answer_sum = NULL, *answer_difference = NULL, *answer_product = NULL, *answer_quotient = NULL, *answer_remainder = NULL;
	
	//code
	printf("\n\n");
	printf("\n\n");
	printf("Enter a value of A: \n");
	scanf("%d",&a);
	
	printf("\n\n");
	printf("Enter a value of B: \n");
	scanf("%d",&b);
	
	answer_sum = (int *)malloc(1 * sizeof(int));
	if(answer_sum == NULL)
	{
		printf("\nCould not allocate for 'answer_sum'. Exiting Now..\n\n");
		exit(0);
	}
	
	answer_difference = (int *)malloc(1 * sizeof(int));
	if(answer_difference == NULL)
	{
		printf("\nCould not allocate for 'answer_difference'. Exiting Now..\n\n");
		exit(0);
	}
	
	answer_product = (int *)malloc(1 * sizeof(int));
	if(answer_product == NULL)
	{
		printf("\nCould not allocate for 'answer_product'. Exiting Now..\n\n");
		exit(0);
	}
	
	answer_quotient = (int *)malloc(1 * sizeof(int));
	if(answer_quotient == NULL)
	{
		printf("\nCould not allocate for 'answer_quotient'. Exiting Now..\n\n");
		exit(0);
	}
	
	answer_remainder = (int *)malloc(1 * sizeof(int));
	if(answer_remainder == NULL)
	{
		printf("\nCould not allocate for 'answer_remainder'. Exiting Now..\n\n");
		exit(0);
	}
	
	MathOperations(a,b,answer_sum,answer_difference,answer_product,answer_quotient,answer_remainder);
	
	printf("\n\n");
	printf("\n*********** Results ************\n");	 
	printf("The sum of %d and %d is = %d \n",a,b,*answer_sum);
	printf("The difference of %d and %d is = %d \n",a,b,*answer_difference);
	printf("The product of %d and %d is = %d \n",a,b,*answer_product);
	printf("The quotient of %d and %d on Dividing is = %d , \n",a,b,*answer_quotient);
	printf("The remainder of %d and %d is = %d \n",a,b,*answer_remainder);
	
	if(answer_remainder)
	{
		free(answer_remainder);
		answer_remainder = NULL;
		printf("\nMemory successfully Deallocated for answer_remainder");
	}
	
	if(answer_quotient)
	{
		free(answer_quotient);
		answer_quotient = NULL;
		printf("\nMemory successfully Deallocated for answer_quotient");
	}
	
	if(answer_product)
	{
		free(answer_product);
		answer_product = NULL;
		printf("\nMemory successfully Deallocated for answer_product");
	}
	
	if(answer_difference)
	{
		free(answer_difference);
		answer_difference = NULL;
		printf("\nMemory successfully Deallocated for answer_difference");
	}
	
	if(answer_sum)
	{
		free(answer_sum);
		answer_sum = NULL;
		printf("\nMemory successfully Deallocated for answer_sum");
	}
	
	return(0);
}

void MathOperations(int x, int y, int *sum,int *difference,int *product,int *quotient, int *remainder)
{
	//code
	*sum = x+y;
	*difference = x-y;
	*product = x*y;
	*quotient = x/y;
	*remainder = x%y;	
}
#include<stdio.h>
#include<stdlib.h>

int main()
{
	//function declarartions
	void MultiplyElementsByNumber(int *, int, int);
	
	//variable declarations
	int *iArray = NULL;
	int num_elements;
	int i, num;
	
	//code
	printf("\n\n");
	printf("\nEnter the size of elements in an Integer array : \n");
	scanf("%d",&num_elements);
	
	iArray = (int *)malloc(sizeof(int) * num_elements);
	if(iArray == NULL)
	{
		printf("\nMemory allocation failure..exiting\n");
		exit(0);
	}
	
	printf("\nEnter elements for an integer array : \n");
	for(i = 0; i <num_elements; i++)
	{
		scanf("%d",&iArray[i]);
	}
	
	printf("\nBefore passing the elements to function MultiplyElementsByNumber()\n");
	for(i = 0; i < num_elements; i++)
	{
		printf("\niArray[%d] = %d",i,iArray[i]);
	}
	
	printf("\n\n");
	printf("\nEnter a number to which each array element should be multilied \n");
	scanf("%d",&num);
	
	MultiplyElementsByNumber(iArray,num_elements,num);
	
	printf("\n\nArray returned after multplication of each elements with %d is :\n\n",num);
	
	for(i = 0; i < num_elements;i++)
	{
		printf("\niArray[%d] = %d",i,iArray[i]);
	}
	
	if(iArray)
	{
		free(iArray);
		iArray = NULL;
		printf("\nMemory for iArray deallocated successfully..");
	}
	
	return(0);
}


void MultiplyElementsByNumber(int *arr, int iNumElements, int iNum)
{
	int i ;
	for(i = 0 ; i < iNumElements; i++)
	{
		arr[i] = arr[i] * iNum;
	}
}
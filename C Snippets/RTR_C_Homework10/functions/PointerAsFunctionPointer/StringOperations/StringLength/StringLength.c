#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype
	int MyStrlen(char *);
	
	//variable declarations
	char *chArray = NULL;
	int iStringLength = 0;
	
	//code
	printf("\n\n");
	chArray = (char *)malloc(sizeof(char) * MAX_STRING_LENGTH);
	
	if(chArray == NULL)
	{
		printf("\nMemory allocation for Character array failed..\n");
		exit(0);
	}
	else
	{
		printf("\nMemory allocation for character aaray was successful..\n");
	}
	
	//string input 
	printf("Enter a string :\n")
	gets_s(chArray,MAX_STRING_LENGTH);
	
	
	//string output
	printf("\n\n");
	printf("\nString Entered by you is : %s\n",chArray);
	
	//string length
	printf("\n\n");
	iStringLength = MyStrlen(chArray);
	printf("Length of the string is = %d\n",iStringLength);
	
	if(chArray)
	{
		free(chArray);
		chArray = NULL;
	}
	
	return(0);
}

int MyStrlen(char *Str)
{
	int i, iCnt = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(*(Str + i) == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	
	return(iCnt);
}
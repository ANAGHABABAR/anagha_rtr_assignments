#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype
	void Mystrcat(char *, char *);
	int MyStrlen(char *);
	
	//var declarations
	char *chArray_one = NULL, *chArray_two = NULL;
	
	
	//code
	printf("\n\n");
	chArray_one = (char *)malloc(sizeof(char) * MAX_STRING_LENGTH);
	if(chArray_one == NULL)
	{
		printf("Memory allocation failure for an first string\n");
		exit(0);
	}
	
	printf("\n\n");
	chArray_two = (char *)malloc(sizeof(char) * MAX_STRING_LENGTH);
	if(chArray_two == NULL)
	{
		printf("Memory allocation failure for an second string\n");
		exit(0);
	}
	
	printf("\nEnter first string :\n");
	gets_s(chArray_one,MAX_STRING_LENGTH);
	
	printf("\nEnter second string :\n");
	gets_s(chArray_two,MAX_STRING_LENGTH);
	
	Mystrcat(chArray_one,chArray_two);
	
	printf("\nString after concatenation is : %s\n",chArray_one);
	
	if(chArray_one)
	{
		free(chArray_one);
		chArray_one = NULL;
		printf("chArray_one freed\n");
	}
	
	
	if(chArray_two)
	{
		free(chArray_two);
		chArray_one = NULL;
		printf("chArray_two freed\n");
	}
	return(0);
}

int MyStrlen(char *Str)
{
	int i, iCnt = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(*(Str + i) == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	return(iCnt);
}

void Mystrcat(char *destination, char *source)
{
	//fun declarations
	int MyStrlen(char *);
	
	//var declarations
	int iStrLength_Source = 0, iStrLength_Dest = 0;
	int i , j ;
	
	//code
	iStrLength_Dest = MyStrlen(destination);
	iStrLength_Source = MyStrlen(source);
	
	for(i = iStrLength_Dest , j = 0; j < iStrLength_Source; i++,j++)
	{
		*(destination + i) = *(source + j);
	}
	*(destination + i) = '\0';
	

}
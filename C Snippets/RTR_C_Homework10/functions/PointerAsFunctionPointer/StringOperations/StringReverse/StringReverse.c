#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype 
	void MyStrrev(char *,char *);
	int MyStrlen(char *);
	
	//variable declarations
	char *chArray_Original = NULL, *chArray_Reversed = NULL;
	
	int orig_string_length = 0;
	
	//code
	printf("\n\n");
	chArray_Original = (char *)malloc(sizeof(char) * MAX_STRING_LENGTH);
	if(chArray_Original == NULL)
	{
		printf("Memory allocation failure for an original string\n");
		exit(0);
	}
	
	printf("\nEnter a string : ");
	gets_s(chArray_Original, MAX_STRING_LENGTH);
	
	//string reverse
	orig_string_length = MyStrlen(chArray_Original);
	chArray_Reversed = (char *)malloc(sizeof(char) * orig_string_length);
	
	if(chArray_Reversed == NULL)
	{
		printf("\nMemory allocation failure for Reversed array..\n");
	}
	
	MyStrrev(chArray_Reversed,chArray_Original);
	
	//string output
	printf("\n\n");
	printf("The reversed array of an original string is : %s\n\n",chArray_Reversed);
	
	//freeing memory for original as well as copied array
	if(chArray_Reversed)
	{
		free(chArray_Reversed);
		chArray_Reversed = NULL;	
	}
	
	if(chArray_Original)
	{
		free(chArray_Original);
		chArray_Original = NULL;		
	}
	
	return(0);
}


int MyStrlen(char *Str)
{
	int i, iCnt = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(*(Str + i) == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	
	return(iCnt);
}

void MyStrrev(char *destination,char *source)
{
	//function prototype
	int MyStrlen(char *);
	
	//variable declarations
	int iStrlength = 0;
	int i, j, len;
	
	iStrlength = MyStrlen(source);
	
	len = iStrlength - 1;
	
	for(i = 0, j = len; i < iStrlength; i++,j--)
	{
		*(destination + i) = *(source + j);
	}
	*(destination + i) = '\0';

}
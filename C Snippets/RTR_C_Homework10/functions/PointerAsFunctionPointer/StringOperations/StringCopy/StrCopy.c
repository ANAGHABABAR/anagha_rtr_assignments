#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//function prototype
	void MyStrcpy(char *,char *);
	int MyStrlen(char *);
	
	//varible declarations
	char *chArray_Original = NULL, *chArray_Copy = NULL;
	int orig_str_length = 0;
	
	printf("\n\n");
	chArray_Original = (char*)malloc(sizeof(char) * 512);
	if(chArray_Original == NULL)
	{
		printf("\nMemory allocation failure..Exiting!");
		exit(0);
	}
	
	printf("\nEnter a string :\n");
	gets_s(chArray_Original,MAX_STRING_LENGTH);
	orig_str_length = MyStrlen(chArray_Original);
	
	chArray_Copy = (char *)malloc(sizeof(char) * orig_str_length);
	if(chArray_Copy == NULL)
	{
		printf("\nMemory allocation for copied array size failure..Exiting");
		exit(0);
	}
	
	//string copy
	MyStrcpy(chArray_Copy,chArray_Original);
	
	printf("\nThe copied array string is %s\n",chArray_Copy);
	
	if(chArray_Copy)
	{
		free(chArray_Copy);
		chArray_Copy = NULL;
	}
	
	if(chArray_Original)
	{
		free(chArray_Original);
		chArray_Original = NULL;
	}
	
	return(0);
}

int MyStrlen(char *Str)
{
	int i, iCnt = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(*(Str + i) == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	
	return(iCnt);
}

void MyStrcpy(char *str_destination, char *str_source)
{
	//function prototype
	int MyStrlen(char *);
	
	//variable declarations
	int iStrLength = 0;
	int i;
	
	//code
	iStrLength = MyStrlen(str_source);
	for(i = 0; i < iStrLength; i++)
	{
		*(str_destination + i) = *(str_source + i);
	}
	*(str_destination + i) = '\0';
}
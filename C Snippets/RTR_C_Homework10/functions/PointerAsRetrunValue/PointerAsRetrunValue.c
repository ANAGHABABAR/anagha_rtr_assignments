#include<stdio.h>
#include<stdlib.h>

#define MAX_STRING_LENGTH 512

int main()
{
	//fun declarations
	char * ReplaceVowelsWithHashSymbol(char *);
	
	//variable declarations
	char string[MAX_STRING_LENGTH];
	char *replaced_string = NULL;
	
	//code
	printf("\n\n");
	printf("\nEnter a string \n");
	gets_s(string, MAX_STRING_LENGTH);
	
	replaced_string = ReplaceVowelsWithHashSymbol(string);
	if(replaced_string == NULL)
	{
		printf("\nReplaceVowelsWithHashSymbol() function has failed..Exiting\n");
		exit(0);
	}
	
	printf("\nThe replaced string is : %s\n",replaced_string);
	
	if(replaced_string)
	{
		free(replaced_string);
		replaced_string = NULL;
		printf("\nMemory for replaced_string has been deallocated successfully\n");
	}
	
	return(0);
}

char *ReplaceVowelsWithHashSymbol(char *s)
{
	//function prototype
	void MyStrcpy(char *, char *);
	int MyStrlen(char *);
	
	//variable declarations
	char *new_string = NULL;
	int i;
	
	//code
	new_string = (char *)malloc(sizeof(char) * MyStrlen(s));
	if(new_string ==NULL)
	{
		printf("\nMemory allocation for new string failed..Exiting!\n");
		exit(0);
	}
	MyStrcpy(new_string,s);
	for(i = 0; i < MyStrlen(new_string); i++)
	{
		switch(new_string[i])
		{
			case 'A':
			case 'a':
			case 'E':
			case 'e':
			case 'I':
			case 'i':
			case 'O':
			case 'o':
			case 'U':
			case 'u':
					new_string[i] = '#';
					break;
			default:
					break;
		}
	}
	return(new_string);
}

void MyStrcpy(char *str_destination, char *str_source)
{
	//function prototype
	int MyStrlen(char *);
	
	//variable declarations
	int iStrLength = 0;
	int i;
	
	//code
	iStrLength = MyStrlen(str_source);
	for(i = 0; i < iStrLength; i++)
	{
		*(str_destination + i) = *(str_source + i);
	}
	*(str_destination + i) = '\0';
}


int MyStrlen(char *Str)
{
	int i, iCnt = 0;
	
	for(i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if(Str[i] == '\0')
		{
			break;
		}
		else
		{
			iCnt++;
		}
	}
	return(iCnt);
}

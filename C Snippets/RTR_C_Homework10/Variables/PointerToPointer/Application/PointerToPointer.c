#include<stdio.h>

int main()
{
	//variable declarations
	int num;
	int *ptr = NULL;
	int **pptr = NULL;//Declaration method 1 : **pptr is a pointer of type int
	
	//code
	num = 10;
	
	printf("\n\n");
	
	printf("\n***********BEFORE ptr = &num***********\n");
	printf("Value of 'num' = %d\n ",num);
	printf("Address of 'num' = %p\n",&num);
	printf("Value at Address of 'num' = %d",*(&num));
	printf("\n\n");
	
	ptr = &num;
	
	printf("\n\n");
	printf("\n***********AFTER ptr = &num***********\n");
	printf("Value of 'num' = %d\n ",num);
	printf("Address of 'num' = %p\n",ptr);
	printf("Value at Address of 'num' = %d",*(&num));
	printf("\n\n");
	
	//Assigning address of Variable 'ptr' to pointer-to-pointer variable 'pptr'
	//'pptr' now contains the address of 'ptr' which in turn contains the address of 'num'
	//hence, 'pptr' is same as &ptr
	//'ptr' is same as '&num'
	//Hence, pptr = &ptr = &(&num);
	//If ptr = &num and *ptr = *(&num) = value at address of 'num'
	//Then, pptr = &ptr and *pptr = *(&ptr) = ptr = value at address of 'ptr' i.e. : address of 'num'
	//Then, **pptr = **(&ptr) = *(*(&ptr)) = *ptr = *(&num) = num = 10
	//Hence, num = *(&num) *ptr = *(*pptr) = **pptr
	
	pptr = &ptr;
	
	printf("\n\n");
	printf("\n********After pptr = &ptr**********\n");
	printf("Value of 'num' = %d\n ",num);
	printf("Address of 'num' = %p\n",pptr);
	printf("Value at Address of ptr = %p\n",*pptr);
	printf("Value at Address of num (*ptr)(*pptr) = %d\n",**pptr);
	printf("\n\n");

	return(0);
}
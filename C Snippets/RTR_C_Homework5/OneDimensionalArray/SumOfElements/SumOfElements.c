#include<stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int iArray[NUM_ELEMENTS];
	int i, num, sum = 0;
	
	//code
	printf("\n\n");
	printf("\nInteger Elements for Array : \n");
	for(i = 0;i < NUM_ELEMENTS; i++)
	{
		scanf("%d",&num);
		iArray[i] = num;
	}
	
	for(i = 0; i < NUM_ELEMENTS; i++)
	{
		sum = sum + iArray[i];
	}		
	
	printf("\nSum of ALL elements in iArray = %d\n",sum);
	
	return(0);
}
#include<stdio.h>

//MACRO CONSTANT USED as Array Size in Subscript and as Array Length
//Hence, this program's arrays' sizes can be simply changed by Changing these following 3 Global Macro Constant Values, Before compiling, Linking and executing the program.

#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 15

int main(void)
{
	//variable declarations
	int iArray[INT_ARRAY_NUM_ELEMENTS];
	float fArray[FLOAT_ARRAY_NUM_ELEMENTS];
	char cArray[CHAR_ARRAY_NUM_ELEMENTS];
	int i;
	
	// Array Elements Input
	printf("\n\n");
	printf("\nEnter Elements for 'Integer' Array : \n");
	for(i = 0; i < INT_ARRAY_NUM_ELEMENTS; i++)
	{
		scanf("%d",&iArray[i]);
	}
	
	printf("\n\n");
	printf("\nEnter Elements for 'Float' Array : \n");
	for(i = 0; i < FLOAT_ARRAY_NUM_ELEMENTS; i++)
	{
		scanf("%f",&fArray[i]);
	}
	
	printf("\n\n");
	printf("\nEnter Elements for 'Char' Array : \n");
	for(i = 0; i < CHAR_ARRAY_NUM_ELEMENTS; i++)
	{
		scanf("%c",&cArray[i]);
	}
	
	// Array Elements Output
	
	printf("\n******************************************\n");
	printf("\nElements of an Integer array are : \n");
	for(i = 0; i < INT_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%d\n",iArray[i]);
	}
	
	printf("\n******************************************\n");
	printf("\nElements of Float Array are : \n");
	for(i = 0; i < FLOAT_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%f\n",fArray[i]);
	}
	
	printf("\n******************************************\n");
	printf("\nElements of Char Array are : \n");
	for(i = 0; i < CHAR_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%c\n",cArray[i]);
	}
	
	
	return(0);
}
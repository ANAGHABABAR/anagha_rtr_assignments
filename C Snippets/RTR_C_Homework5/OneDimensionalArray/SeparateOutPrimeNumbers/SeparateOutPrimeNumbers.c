#include<stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	int iArray[NUM_ELEMENTS];
	int i, num, j, count =0;
	
	//code
	printf("\n\n");
	
	//Array Elements Input
	printf("\nEnter Integer Elements : \n");
	for(i = 0; i < NUM_ELEMENTS ; i++)
	{
		scanf("%d", &num);
		
		//Filter  : if number is negative then convert it into  positve number 
		if(num < 0)
		{
			num = -(num); // or -1 * num
		}
		
		iArray[i] = num;
	}
	
	//Printing entire array
	printf("\n*********************************\n");
	printf("\nArray Elements are : \n");
	for(i = 0; i < NUM_ELEMENTS; i++)
	{
		printf("%d\n",iArray[i]);
	}
	
	//Separating out Prime numbers from an Array
	for(i = 0; i < NUM_ELEMENTS; i++)
	{
		for(j = 1; j <= iArray[i]; j++)
		{
			if((iArray[i] % j) == 0)
			{
				count++;
			}	
			
		}
		if(count == 2)
		{
			printf("Prime number : %d\n",iArray[i]);
		}
		count = 0;		

	}
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[] = {11, 21, 51, 91, 101, 201, 501, 1001, 2001, 3001};
	int intSize;
	int iArraySize;
	int iArrayNumElements;
	
	float fArray[] = {1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f};
	int floatSize;
	int fArraySize;
	int fArrayNumElements;
	
	char cArray[] = {'A','N','A','G','H','A','B','A','B','A','R'};
	int charSize;
	int cArraySize;
	int cArrayNumElements;
	
	
	//code
	//iArray[]
	printf("\nInline Initialization and Piece meal displaying of an array : \n");
	printf("\niArray[0] = %d\n",iArray[0]);
	printf("\niArray[1] = %d\n",iArray[1]);
	printf("\niArray[2] = %d\n",iArray[2]);
	printf("\niArray[3] = %d\n",iArray[3]);
	printf("\niArray[4] = %d\n",iArray[4]);
	printf("\niArray[5] = %d\n",iArray[5]);
	printf("\niArray[6] = %d\n",iArray[6]);
	printf("\niArray[7] = %d\n",iArray[7]);
	printf("\niArray[8] = %d\n",iArray[8]);
	printf("\niArray[9] = %d\n",iArray[9]);
	
	intSize = sizeof(int);
	iArraySize = sizeof(iArray);
	iArrayNumElements = iArraySize / intSize;
	printf("\n\nSize of Data type 'int'	= %d bytes\n",intSize);
	printf("\nNumber of elements in 'int' Array named 'iArray[]'= %d Elements\n",iArrayNumElements);
	printf("\nSize of Array 'iArray[]' (%d Elements * %d Bytes) = %d Bytes\n",iArrayNumElements, intSize, iArraySize);
	
	
	//fArray
	printf("\n\n");
	printf("\nInline initialization and piece meal displaying of an 'fArray'\n");
	printf("fArray[0] (1st Element) = %f\n",fArray[0]);
	printf("fArray[1] (2nd Element) = %f\n",fArray[1]);
	printf("fArray[2] (3rd Element) = %f\n",fArray[2]);
	printf("fArray[3] (4th Element) = %f\n",fArray[3]);
	printf("fArray[4] (5th Element) = %f\n",fArray[4]);
	printf("fArray[5] (6th Element) = %f\n",fArray[5]);
	
	floatSize = sizeof(float);
	fArraySize = sizeof(fArray);
	fArrayNumElements = fArraySize / floatSize;
	printf("\nSize of datatype float = %d\n",floatSize);
	printf("\nSize of float array 'fArray' = %d\n",fArraySize);
	printf("\nSize of Array 'fArray[]' (%d Elements * %d Bytes) = %d Bytes\n",fArrayNumElements, floatSize, fArraySize);
	
	//cArray
	printf("\n\n");
	printf("\nInline initialization and Piece meal diasplaying of character array 'cArray'");
	charSize = sizeof(char);
	cArraySize = sizeof(cArray);
	cArrayNumElements = cArraySize / charSize;
	printf("\nSize of datatype char = %d\n",charSize);
	printf("\nSize of char array 'cArray' = %d\n",cArraySize);
	printf("\nSize of Array 'cArray[]' (%d Elements * %d Bytes) = %d Bytes\n",cArrayNumElements, charSize, cArraySize);
	printf("cArray[0] (1st Element) = %c\n",cArray[0]);
	printf("cArray[1] (2nd Element) = %c\n",cArray[1]);
	printf("cArray[2] (3rd Element) = %c\n",cArray[2]);
	printf("cArray[3] (4th Element) = %c\n",cArray[3]);
	printf("cArray[4] (5th Element) = %c\n",cArray[4]);
	printf("cArray[5] (6th Element) = %c\n",cArray[5]);
	printf("cArray[6] (7th Element) = %c\n",cArray[6]);
	printf("cArray[7] (8th Element) = %c\n",cArray[7]);
	printf("cArray[8] (9th Element) = %c\n",cArray[8]);
	printf("cArray[9] (10th Element) = %c\n",cArray[9]);
	printf("cArray[10] (11th Element) = %c\n",cArray[10]);
	
	
	
	
	
	
}
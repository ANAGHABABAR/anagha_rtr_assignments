#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[] = {1, 11, 21, 31, 51, 91, 101, 201, 501};
	int intSize;
	int iArraySize;
	int iArrayNumElements;
	
	float fArray[] = {11.2f, 12.3f, 13.4f, 14.5f, 15.6f, 16.7f, 17.8f, 18.9f};
	int floatSize;
	int fArraySize;
	int fArrayNumElements;
	
	char cArray[] = {'A','N','A','G','H','A','B','A','B','A','R'};
	int charSize;
	int cArraySize;
	int cArrayNumElements;
	
	int i = 0;
	
	printf("\nInline initialization and Loop(for) Display of Elements of Array 'iArray[]' : \n");
	
	intSize = sizeof(int);
	iArraySize = sizeof(iArray);
	iArrayNumElements = iArraySize/intSize;
	
	for(i = 0; i < iArrayNumElements; i++)
	{
		printf("\niArray[%d] (Element %d) = %d\n",i, (i+1), iArray[i]);
	}
	
	printf("\nSize of data type int = %d\n", intSize);
	printf("\nSize of an 'iArray' = %d\n",iArraySize);
	printf("\nSize of an Array is (%d Elements * %d Bytes) = %d\n",iArrayNumElements, intSize, iArraySize);
	
	//float array
	printf("\n**************************************************************************\n");
	printf("\nInline initialization and Loop(for) Display of Elements of Array 'fArray[]' : \n");
	
	floatSize = sizeof(float);
	fArraySize = sizeof(fArray);
	fArrayNumElements = fArraySize/floatSize;
	
	for(i = 0; i < fArrayNumElements; i++)
	{
		printf("\nfArray[%d] (Element %d) = %f\n",i, (i+1), fArray[i]);
	}
	
	printf("\nSize of data type float = %d\n", floatSize);
	printf("\nSize of an 'fArray' = %d\n",fArraySize);
	printf("\nSize of an Array is (%d Elements * %d Bytes) = %d\n",fArrayNumElements, floatSize, fArraySize);
	
	
	//char array
	printf("\n**************************************************************************\n");
	printf("\nInline initialization and Loop(for) Display of Elements of Array 'cArray[]' : \n");
	
	charSize = sizeof(char);
	cArraySize = sizeof(cArray);
	cArrayNumElements = cArraySize/charSize;
	
	for(i = 0; i < cArrayNumElements; i++)
	{
		printf("\nfArray[%d] (Element %d) = %c\n",i, (i+1), cArray[i]);
	}
	
	printf("\nSize of data type float = %d\n", charSize);
	printf("\nSize of an 'fArray' = %d\n",cArraySize);
	printf("\nSize of an Array is (%d Elements * %d Bytes) = %d\n",cArrayNumElements, charSize, cArraySize);
	
	
}
#include<stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int iArray[NUM_ELEMENTS];
	int i, num, sum = 0;
	
	//code
	printf("\n\n");
	
	//Array Elements Input
	printf("\nEnter Integer Elements for Array :\n");
	for(i = 0; i < NUM_ELEMENTS ; i++)
	{
		scanf("%d",&num);
		iArray[i] = num;
	}
	
	//separating out even numbers from Array Elements
	printf("\nEven numbers amongst the Array Elements are : \n");
	for(i = 0; i < NUM_ELEMENTS; i++)
	{
		if((iArray[i] % 2) == 0)
		{
			printf("%d\n",iArray[i]);
		}
	}

	//Separating out odd numbers
	printf("\n\n");
	printf("Odd numbers Amongst the Array elements are : \n\n");
	for(i = 0;i < NUM_ELEMENTS; i++)
	{
		if((iArray[i] % 2) != 0)
		{
			printf("%d\n",iArray[i]);
		}
	}
	
	return(0);
}
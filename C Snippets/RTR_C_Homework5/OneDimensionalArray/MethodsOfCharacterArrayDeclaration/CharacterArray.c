#include<stdio.h>

int main()
{
	//variable declarations
	char chArray_01[] = {'A','S','T','R','O','M','E','D','I','C','O','M','P','\0'};//Must give \0 explicitly for proper initialization
	char chArray_02[9] = {'W','E','L','C','O','M','E','S','\0'};//Must give \0 explicitly for proper initialization
	char chArray_03[] = {'Y','O','U','\0'};//Must give \0 explicitly for proper initialization
	char chArray_04[] = "To";// \0 is assumed , size is given as 3, although string has only 2 characters
	char chArray_05[] = "Real Time Rendering Batch of 2020-2021";// \0 is assumed, 39 characters, Actual input of 38 charactes and 38th character is \0 which is assumed.
	
	char chArray_WithoutNullTerminator[] = {'H','e','l','l','o'};
	
	//code
	printf("\n\n");
	printf("\nSize of chArray_01 : %lu \n\n", sizeof(chArray_01));
	printf("\nSize of chArray_02 : %lu \n\n", sizeof(chArray_02));
	printf("\nSize of chArray_03 : %lu \n\n", sizeof(chArray_03));
	printf("\nSize of chArray_04 : %lu \n\n", sizeof(chArray_04));
	printf("\nSize of chArray_05 : %lu \n\n", sizeof(chArray_05));
	
	printf("\n\n");
	
	printf("\n The strings are : \n\n");
	printf("chArray_01 : %s\n",chArray_01);
	printf("chArray_02 : %s\n",chArray_02);
	printf("chArray_03 : %s\n",chArray_03);
	printf("chArray_04 : %s\n",chArray_04);
	printf("chArray_05 : %s\n",chArray_05);
	
	printf("\n\n");
	printf("\nSize of chArray_WithoutNullTerminator : %lu\n", sizeof(chArray_WithoutNullTerminator));
	printf("\nchArray_WithoutNullTerminator : %s\n",chArray_WithoutNullTerminator);//will print garbage value at the end since there is no \0

	return(0);
}
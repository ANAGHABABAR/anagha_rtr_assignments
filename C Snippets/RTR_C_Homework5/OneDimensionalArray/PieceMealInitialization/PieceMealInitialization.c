#include<stdio.h>

int main(void)
{
	//variaable declarations
	int iArrayOne[10];
	int iArrayTwo[10];
	
	//code
	//***iArrayOne ***
	iArrayOne[0] = 9;
	iArrayOne[1] = 18;
	iArrayOne[2] = 27;
	iArrayOne[3] = 36;
	iArrayOne[4] = 45;
	iArrayOne[5] = 54;
	iArrayOne[6] = 63;
	iArrayOne[7] = 72;
	iArrayOne[8] = 81;
	iArrayOne[9] = 90;
	
	printf("\nDisplaying values for piece meal initialized iArrayOne : \n");
	printf("\n1st element of iArrayOne is at 0th position = %d",iArrayOne[0]);
	printf("\n2nd element of iArrayOne is at 1st position = %d",iArrayOne[1]);
	printf("\n3rd element of iArrayOne is at 2nd position = %d",iArrayOne[2]);
	printf("\n4th element of iArrayOne is at 3rd position = %d",iArrayOne[3]);
	printf("\n5th element of iArrayOne is at 4th position = %d",iArrayOne[4]);
	printf("\n6th element of iArrayOne is at 5th position = %d",iArrayOne[5]);
	printf("\n7th element of iArrayOne is at 6th position = %d",iArrayOne[6]);
	printf("\n8th element of iArrayOne is at 7th position = %d",iArrayOne[7]);
	printf("\n9th element of iArrayOne is at 8th position = %d",iArrayOne[8]);
	printf("\n10th element of iArrayOne is at 9th position = %d",iArrayOne[9]);
	
	
	//***iArrayTwo ***
	printf("\n\n************************************************************\n");
	printf("\nAccepting array elements using scanf() : \n");
	printf("Enter 1st Element of 'iArrayTwo[0]' : ");
	scanf("%d",&iArrayTwo[0]);
	
	printf("Enter 2nd Element of 'iArrayTwo[1]' : ");
	scanf("%d",&iArrayTwo[1]);
	
	printf("Enter 3rd Element of 'iArrayTwo[2]' : ");
	scanf("%d",&iArrayTwo[2]);
	
	printf("Enter 4th Element of 'iArrayTwo[3]' : ");
	scanf("%d",&iArrayTwo[3]);
	
	printf("Enter 5th Element of 'iArrayTwo[4]' : ");
	scanf("%d",&iArrayTwo[4]);
	
	printf("Enter 6th Element of 'iArrayTwo[5]' : ");
	scanf("%d",&iArrayTwo[5]);
	
	printf("Enter 7th Element of 'iArrayTwo[6]' : ");
	scanf("%d",&iArrayTwo[6]);
	
	printf("Enter 8th Element of 'iArrayTwo[7]' : ");
	scanf("%d",&iArrayTwo[7]);
	
	printf("Enter 9th Element of 'iArrayTwo[8]' : ");
	scanf("%d",&iArrayTwo[8]);
	
	printf("Enter 10th Element of 'iArrayTwo[9]' : ");
	scanf("%d",&iArrayTwo[9]);
	
	printf("\nDisplaying elements of iArrayTwo[] :\n");
	printf("\n1st element of iArrayTwo is at 0th position = %d",iArrayTwo[0]);
	printf("\n2nd element of iArrayTwo is at 1st position = %d",iArrayTwo[1]);
	printf("\n3rd element of iArrayTwo is at 2nd position = %d",iArrayTwo[2]);
	printf("\n4th element of iArrayTwo is at 3rd position = %d",iArrayTwo[3]);
	printf("\n5th element of iArrayTwo is at 4th position = %d",iArrayTwo[4]);
	printf("\n6th element of iArrayTwo is at 5th position = %d",iArrayTwo[5]);
	printf("\n7th element of iArrayTwo is at 6th position = %d",iArrayTwo[6]);
	printf("\n8th element of iArrayTwo is at 7th position = %d",iArrayTwo[7]);
	printf("\n9th element of iArrayTwo is at 8th position = %d",iArrayTwo[8]);
	printf("\n10th element of iArrayTwo is at 9th position = %d",iArrayTwo[9]);
	
	printf("\n\n");

	return(0);
	
}
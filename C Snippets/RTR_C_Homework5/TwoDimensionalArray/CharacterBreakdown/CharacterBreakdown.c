#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	
	//variable declarations
	char strArray[10][15] = {"Hello!","Welcome","To","Real","Time","Rendering","Batch","2020-2021","of","ASTROMEDICOMP"};
	
	int iStrLengths[10];
	
	int strArraySize;
	int strNumRows;
	int i, j;
	
	//code
	strArraySize = sizeof(strArray);
	strNumRows = strArraySize /sizeof(strArray[0]);
	
	for(i = 0; i < strNumRows; i++)
	{
		iStrLengths[i] = MyStrlen(strArray[i]);
	}
	
	printf("\n\n");
	printf("The Entire string Array : \n");
	for(i = 0;i < strNumRows; i++)
	{
		printf("%s ",strArray[i]);
	}
	printf("\n\n");
	
	printf("Strings in the 2D Array : \n\n");
	for(i = 0; i < strNumRows ;i++)
	{
		printf("String number %d => %s\n\n",(i),strArray[i]);
		for(j = 0; j < iStrLengths[i]; j++)
		{
			printf("\nCharacter %d = %c\n", j, strArray[i][j]);
		}
		printf("\n\n");
	}
	
	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int stringLength = 0;
	
	//code
	for(j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if(str[j] == '\0')
		{
			break;
		}
		else
		{
			stringLength++;
		}
	}
	return(stringLength);
}
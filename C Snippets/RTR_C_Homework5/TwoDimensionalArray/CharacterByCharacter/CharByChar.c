#include<stdio.h>

#define MAX_STRING_LENGTH

int main(void)
{
	char strArray[5][10];
	int charSize;
	int strArraySize;
	int strArrayNumElements, strArrayNumRows, strArrayNumColumns;
	int i;
	
	//code
	printf("\n\n");
	
	charSize = sizeof(char);
	strArraySize = sizeof(strArray);
	printf("\nSize of a 2-Dimensional strArray is = %d\n", strArraySize);
	
	strArrayNumRows = strArraySize / sizeof(strArray[0]);
	printf("\nNumber of Rows in 2-Dimensional strArray = %d\n",strArrayNumRows);
	
	strArrayNumColumns = sizeof(strArray[0]) / charSize;
	printf("\nNumber of columns in 2-Dimensional strArray are = %d\n", strArrayNumColumns);
	
	strArrayNumElements = strArrayNumRows * strArrayNumColumns;
	printf("\nElements is 2-Dimensional strArray are = %d\n",strArrayNumElements);
	
	//Piece meal Assignment
	strArray[0][0] = 'M';
	strArray[0][1] = 'y';
	strArray[0][2] = '\0';
	
	strArray[1][0] = 'N';
	strArray[1][1] = 'a';
	strArray[1][2] = 'm';
	strArray[1][3] = 'e';
	strArray[1][4] = '\0';
	
	strArray[2][0] = 'i';
	strArray[2][1] = 's';
	strArray[2][2] = '\0';
	
	strArray[3][0] = 'A';
	strArray[3][1] = 'n';
	strArray[3][2] = 'a';
	strArray[3][3] = 'g';
	strArray[3][4] = 'h';
	strArray[3][5] = 'a';
	strArray[3][6] = '\0';
	
	strArray[4][0] = 'B';
	strArray[4][1] = 'a';
	strArray[4][2] = 'b';
	strArray[4][3] = 'a';
	strArray[4][4] = 'r';
	strArray[4][5] = '\0';
	
	//Printing
	printf("\nThe strings in 2-Dimensional array are :\n");
	for(i = 0; i < strArrayNumRows; i++)
	{
		printf("%s ",strArray[i]);
	}
	printf("\n\n");
	
	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	int iArray[3][5]; //3 rows, 5 columns
	int intSize;
	int iArraySize;
	int iArrayNumElements, iArrayNumRows, iArrayNumColumns;
	int i, j;
	
	//code
	printf("\n\n");
	intSize = sizeof(int);
	iArraySize = sizeof(iArray);
	printf("\nSize of 2- Dimensional Array is : %d\n",iArraySize);
	
	iArrayNumRows = iArraySize / sizeof(iArray[0]);
	printf("\nNumber of rows in the 2-Dimensional Array are = %d\n",iArrayNumRows);
	
	iArrayNumColumns = sizeof(iArray[0]) / intSize;
	printf("\nNumber of columns in 2-Dimensional Array are :\n iArrayNumColumns : (%d) = sizeof(iArraySize[0]) : (%d) / intSize : (%d) \n",iArrayNumColumns, sizeof(iArray[0]), intSize );
	
	iArrayNumElements = iArrayNumRows * iArrayNumColumns;
	printf("\nSize of elements in 2-Dimensional array are : %d\n",iArrayNumElements);
	
	printf("\nElements in 2Dimensional Array are : \n");
	
	//***Piece meal initialization
	//Row 0
	iArray[0][0] = 11;
	iArray[0][1] = 21;
	iArray[0][2] = 51;
	iArray[0][3] = 101;
	iArray[0][4] = 201;
	
	//Row 1
	iArray[1][0] = 301;
	iArray[1][1] = 401;
	iArray[1][2] = 501;
	iArray[1][3] = 601;
	iArray[1][4] = 701;
	
	//Row 2
	iArray[2][0] = 801;
	iArray[2][1] = 901;
	iArray[2][2] = 1001;
	iArray[2][3] = 2001;
	iArray[2][4] = 3001;
	
	
	//Display
	for(i = 0; i < iArrayNumRows; i++)
	{
		for(j = 0; j < iArrayNumColumns; j++)
		{
			printf("\niArray[%d][%d] = %d ",i,j,iArray[i][j]);
		}
		printf("\n\n");
	}
	
	return(0);
}
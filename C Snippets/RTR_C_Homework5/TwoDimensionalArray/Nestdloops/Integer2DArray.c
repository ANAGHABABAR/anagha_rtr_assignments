#include<stdio.h>

int main(void)
{
	//variable declarations
	int iArray[5][3] = {{1,2,3},{4,5,6},{7,8,9},{10,11,12},{13,14,15}};
	int intSize;
	int iArraySize;
	int iArrayNumElements, iArrayNumRows, iArrayNumColumns;
	int i, j;
	
	//code
	printf("\n\n");
	
	intSize = sizeof(int);
	
	iArraySize = sizeof(iArray);
	printf("Size of 2-Dimensional array is = %d\n",iArraySize);
	
	iArrayNumRows = iArraySize / sizeof(iArray[0]);
	printf("\nNumber of Rows in 2-Dimensional Array are : \niArrayNumRows : (%d) = iArraySize : (%d) / sizeof(iArray[0]) : (%d)\n\n",iArrayNumRows, iArraySize , sizeof(iArray[0]));
	
	iArrayNumColumns = sizeof(iArray[0]) / intSize;
	printf("\nNumber of Columns in 2-Dimensional Array are : \niArrayNumColumns : (%d) = sizeof(iArray[0]) : (%d) / intSize : (%d)\n",iArrayNumColumns ,sizeof(iArray[0]) ,intSize);
	
	//printing elements of an array using nested loop
	for(i = 0; i < iArrayNumRows; i++)
	{
		for(j = 0; j < iArrayNumColumns; j++)
		{
			printf("iArray[%d][%d] = %d\n",i,j,iArray[i][j]);
		}
		printf("\n\n");
	}
		
	
	
		
	return(0);
}
#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	
	//variable declarations
	
	//NOTE : A 'STRING' IS AN ARRAY OF CHARACTERS... so char[] IS A char ARRAY AND HENCE, char[] IS A 'STRING' ***
	// ***An array of char ARRAYS is an ARRAY OF STRINGS
	// ***HENCE, char[] is ONE char ARRAY and Hence is ONE STRING
	// ***HENCE, char[][] is an ARRAY of char ARRAYS and Hence , is an ARRAY OF STRINGS
	
	//Here, the string array can allow a maximum number of 10 strings(10 rows) and each of these 10 strings can have only upto 15 characters maximun(15 columns)
	
	char strArray[10][15] = {"Hello!","Welcome","To","Real","Time","Rendering","Batch","(2020-21)","of","ASTROMEDICOMP."};//inline initialization
	
	int charSize;
	int strArraySize;
	int strArrayNumElements, strArrayNumRows, strArrayNumColumns;
	int strActualNumChars = 0;
	int i ;
	
	//code
	printf("\n\n");
	
	charSize = sizeof(char);
	
	strArraySize = sizeof(strArray);
	printf("Size of 2-Dimensional character Array (String Array) is = %d\n\n",strArraySize);
	
	strArrayNumRows = strArraySize / sizeof(strArray[0]);
	printf("\nNumber of Rows in 2-Dimensional character Array (String Array) is :\nstrArrayNumRows : %d = strArraySize : %d / sizeof(strArray[0]) : %d\n",	strArrayNumRows , strArraySize ,sizeof(strArray[0]));
	
	strArrayNumColumns = sizeof(strArray[0]) / charSize;
	printf("\nNumber of Columns in 2-Dimensional Character Array(String Array) : \nstrArrayNumColumns = sizeof(strArray[0]) / charSize;\n");
	
	strArrayNumElements = strArrayNumRows * strArrayNumColumns;
	printf("\nMaximum number of Elements (characters) in 2-Dimensional Character array = %d\n",strArrayNumElements);
	
	for(i = 0; i < strArrayNumRows; i++)
	{
		strActualNumChars = strActualNumChars + MyStrlen(strArray[i]);
		printf("\nstrActualNumChars = %d",strActualNumChars);
	}
	
	printf("\n\nActual number of Elements (characters) in 2- Dimensional character array (String Array) is = %d\n\n",strActualNumChars);
	
	printf("\n\n");
	
	printf("\nStrings in the 2d array : \n");
	printf("%s ",strArray[0]);
	printf("%s ",strArray[1]);
	printf("%s ",strArray[2]);
	printf("%s ",strArray[3]);
	printf("%s ",strArray[4]);
	printf("%s ",strArray[5]);
	printf("%s ",strArray[6]);
	printf("%s ",strArray[7]);
	printf("%s ",strArray[8]);
	printf("%s ",strArray[9]);
	
	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int stringLength = 0;
	
	//code
	// ***Determining exact length of the string , by detecting the first occurrence of Null terminating characte (\0)
	for(j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if(str[j] == '\0')
		{
			break;
		}
		else
		{
			stringLength++;
		}
	}
	return(stringLength);
}
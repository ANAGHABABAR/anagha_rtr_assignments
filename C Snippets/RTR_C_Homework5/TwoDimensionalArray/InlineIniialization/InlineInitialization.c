#include<stdio.h>

int main()
{
	//variable declarations
	int iArray[5][3] = {{1,2,3},{4,5,6},{7,8,9},{10,11,12},{13,14,15}};
	int intSize;
	int iArraySize;
	int iArrayNumElements, iArrayNumRows, iArrayNumColumns;
	
	//code
	printf("\n\n");
	
	intSize = sizeof(int);
	
	iArraySize = sizeof(iArray);
	printf("\nTwo Dimensional (2D) Integer array is = %d \n",iArraySize);
	
	iArrayNumRows = iArraySize / sizeof(iArray[0]);
	printf("Number of Rows in 2-Dimensional Integer array is = %d \n\n ",iArrayNumRows);
	
	iArrayNumColumns = sizeof(iArray[0]) / intSize;//sizeof(iArray[0]) will give size of first ROW which is 12 in our case
	printf("\nNumber of Columns in 2-Dimensional Integer array is = %d\n\n",iArrayNumColumns);
	
	iArrayNumElements = iArrayNumRows * iArrayNumColumns;
	printf("\nNumber of Elements in 2-Dimensional array are = %d \n",iArrayNumElements);
	
	printf("\n\n");
	printf("Elements in 2D array are : \n");
	
	printf("\n***************************ROW 1**************************\n");
	printf("\niArray[0][0] = %d \n",iArray[0][0]);
	printf("\niArray[0][1] = %d \n",iArray[0][1]);
	printf("\niArray[0][2] = %d \n",iArray[0][2]);
	printf("\n\n");
	
	printf("\n***************************ROW 2**************************\n");
	printf("\niArray[1][0] = %d \n",iArray[1][0]);
	printf("\niArray[1][1] = %d \n",iArray[1][1]);
	printf("\niArray[1][2] = %d \n",iArray[1][2]);
	printf("\n\n");
	
	printf("\n***************************ROW 3**************************\n");
	printf("\niArray[2][0] = %d \n",iArray[2][0]);
	printf("\niArray[2][1] = %d \n",iArray[2][1]);
	printf("\niArray[2][2] = %d \n",iArray[2][2]);
	printf("\n\n");
	
	printf("\n***************************ROW 4**************************\n");
	printf("\niArray[3][0] = %d \n",iArray[3][0]);
	printf("\niArray[3][1] = %d \n",iArray[3][1]);
	printf("\niArray[3][2] = %d \n",iArray[3][2]);
	printf("\n\n");
	
	printf("\n***************************ROW 5**************************\n");
	printf("\niArray[4][0] = %d \n",iArray[4][0]);
	printf("\niArray[4][1] = %d \n",iArray[4][1]);
	printf("\niArray[4][2] = %d \n",iArray[4][2]);
	printf("\n\n");
		
	return(0);
} 
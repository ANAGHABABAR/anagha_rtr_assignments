#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcpy(char [], char []);
	
	char strArray[5][10];
	int charSize;
	int strArraySize;
	int strArrayNumElements, strArrayNumRows, strArrayNumColumns;
	int i;
	
	//code
	printf("\n\n");
	
	charSize = sizeof(char);
	strArraySize = sizeof(strArray);
	printf("\nSize of a 2-Dimensional strArray is = %d\n", strArraySize);
	
	strArrayNumRows = strArraySize / sizeof(strArray[0]);
	printf("\nNumber of Rows in 2-Dimensional strArray = %d\n",strArrayNumRows);
	
	strArrayNumColumns = sizeof(strArray[0]) / charSize;
	printf("\nNumber of columns in 2-Dimensional strArray are = %d\n", strArrayNumColumns);
	
	strArrayNumElements = strArrayNumRows * strArrayNumColumns;
	printf("\nElements is 2-Dimensional strArray are = %d\n",strArrayNumElements);
	
	//Piece meal Assignment
	
	MyStrcpy(strArray[0],"My ");
	MyStrcpy(strArray[1],"name ");
	MyStrcpy(strArray[2],"is ");
	MyStrcpy(strArray[3],"Anagha ");
	MyStrcpy(strArray[4],"Babar ");
	
	printf("\n\n");
	printf("\nThe Strings in 2-Dimensional array are : \n");
	
	for(i = 0; i < strArrayNumRows; i++)
	{
		printf("%s ",strArray[i]);
	}
	printf("\n\n");
	
	return(0);
}

void MyStrcpy(char Destination[], char Source[])
{
	//function declaration
	int MyStrlen(char []);
	
	//variable declaration
	int iStrLength = 0;
	int j;
	
	iStrLength = MyStrlen(Source);
	
	for(j = 0; j < iStrLength; j++)
		Destination[j] = Source[j];

	Destination[j] = '\0';
	
}

int MyStrlen(char str[])
{
	//variable declarations
	int j;
	int stringLength = 0;
	
	//code
	// ***Determining exact length of the string , by detecting the first occurrence of Null terminating characte (\0)
	for(j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if(str[j] == '\0')
		{
			break;
		}
		else
		{
			stringLength++;
		}
	}
	return(stringLength);
}
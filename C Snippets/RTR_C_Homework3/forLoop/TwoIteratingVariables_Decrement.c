#include <stdio.h>

int main()
{
	int i,j;
	
	printf("\nPrinting 10 to 1 AND 100 to 10\n");
	for(i = 10,j = 100; i>=1, j >= 10 ; i--, j = j-10)
	{
		printf("\n%d AND %d",i,j);
	}
	
	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	int i, j;
	
	//code
	printf("\nPrinting digits between 1 to 10 and 10 to 100\n");
	
	for(i = 1,j = 10; i <= 10, j <= 100; i++,j = j+10)
	{
		printf("\n Number between 1 to 10 = %d. \nNumber between 10 to 100 = %d",i,j);
	}
	printf("\n\n");
	return(0);
}
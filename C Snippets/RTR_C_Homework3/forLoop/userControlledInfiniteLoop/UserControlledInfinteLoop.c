#include<stdio.h>

int main()
{
	char option, ch = '\0';
	
	printf("\n\n");
	printf("Once the Infinite loop begins, enter 'Q' or 'q' to quit the Infinity loop.\n\n");
	printf("\nEnter 'y' or 'Y' to initiate the User controlled infinity loop.\n");
	
	option =getch();
	if(option == 'y' || option == 'Y')
	{
		for(;;)//InFINITE LOOP
		{
			printf("\nIn loop..\n");
			ch = getch();
			if(ch == 'Q' || ch == 'q')
				break;
		}
	}
	
	printf("\nUser controlled infinite loop has been exited successfully!\n");
	
	return(0);
}
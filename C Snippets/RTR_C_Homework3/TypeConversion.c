#include<stdio.h>

int main(void)
{
	
	//variable declarations
	int i, j;
	char ch_01, ch_02;
	
	int a, result_int;
	float f, result_float;
	
	int i_explicit;
	float f_explicit;
	
	//code
	printf("\n\n");
	
	//INTERCONVERSION AND IMPLICIT TYPE CASTING BETWEEN 'char' and 'int' TYPES..
	i = 70;
	ch_01 = i ;
	printf("i = %d\n",i);
	printf("\nCharacter 1 (after ch_01 = i) = %c\n\n",ch_01);
	
	ch_02 = 'Q';
	j = ch_02;
	printf("Character 2 = %c\n", ch_02);
	printf("J (After j =ch_02) = %d\n\n",j);
	
	//IMPLICIT CONVERSION OF 'int' To 'FLOAT'
	a = 5;
	f = 7.8f;
	result_float = a + f;
	printf("\nInteger a = %d  And Floating-Point number %f Added Gives Floating-Point Sum = %f\n",a,f,result_float);
	
	result_int = a + f;
	printf("\nInteger a = %d And Floating-Point number %f Added gives Integer Sum = %d\n\n",a,f,result_int);
	
	//XPLICIT TYPE CASTING USING CAST OPERATOR
	f_explicit = 30.121995f;
	i_explicit = (int)f_explicit;
	printf("\nFloating point Number which will be Type casted explicitly = %f\n",f_explicit);
	printf("\nResultant Integer after explicit type conversion = %d\n",i_explicit);
	
	return(0);
}

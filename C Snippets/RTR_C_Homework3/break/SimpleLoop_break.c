#include<stdio.h>
#include<conio.h>

int main()
{
	//variable declarations
	int i;
	char ch;
	
	printf("\nPrinting 1 to 100 numbers for user input. Exiting the loop when user enters the character 'Q' or 'q'\n");
	printf("\nEnter 'Q' or 'q' to Exit the loop:\n");
	
	for(i = 0; i <= 100; i++)
	{
		printf("\t%d\n",i);
		ch = getch();
		
		if(ch == 'Q' || ch == 'q')
		{
			break;
		}
	}
	
	printf("\nExiting the loop..\n");

	return(0);
}

#include <stdio.h>

int main(void)
{
	//variable declarations
	int i,j;
	
	i = 10;
	j = 100;
	printf("\nPrinting Digits from 10 to 1 :\n");
	while(i >= 1, j >= 10)
	{
		printf("\ni = %d,\tj = %d\n",i,j);
		i--;
		j = j-10;
	}
	
	return(0);
}
#include <stdio.h>

int main(void)
{
	//variable declarations
	int i;
	
	i = 10;
	printf("\nPrinting Digits from 10 to 1 :\n");
	while(i >= 1)
	{
		printf("\ni = %d\n",i);
		i--;
	}
	
	return(0);
}
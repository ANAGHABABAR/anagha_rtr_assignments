#include<stdio.h>

int main(void)
{
	//variable declarations
	char option, ch = '\0';
	
	//code
	printf("\n\n");
	printf("\nOnce the Infinite Loop begins, Enter 'Q' or 'q' to Quit the Infinite Loop!");
	printf("\nEnter 'Y' or 'y' to initiate the User controlled Infinite Loop.\n");
	
	option = getch();
	
	if(option == 'Y' || option == 'y')
	{
		while(1)//Infinite loop
		{
			printf("\nIn Loop..\n");
			ch = getch();
			if(ch == 'Q' || ch == 'q')
				break;
		}
		printf("\nExiting USer controlle Infinite Loop...\n");
	}
	else
	{
		printf("You must Press 'Y' or 'y' to initiate the user controlled infinite loop. Please try again!!");
	}
	
	return(0);
}
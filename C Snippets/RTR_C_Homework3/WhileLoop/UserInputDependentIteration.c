#include <stdio.h>

int main(void)
{	
	//variable declarations
	int iNum, num, i;
	
	//code
	printf("\nEnter an Integer value from which Iteration must begin : \n");
	scanf("%d\n",&iNum);
	
	printf("\nHow many Digits do you want to print %d onwards ? \t",iNum);
	scanf("\n%d",&num);
	
	i = iNum;
	while(i <= iNum + num )
	{
		printf("\t%d\n",i);
		i++;
	}
	return(0);
}
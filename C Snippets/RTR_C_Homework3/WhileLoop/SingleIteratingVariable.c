#include <stdio.h>

int main()
{
	//variable declaraions
	int i;
	
	//code
	printf("\n\n");
	printf("\nPrinting 1 to 10 Digits : \n");
	
	i = 1;
	while(i <= 10)
	{
		printf("\ni = %d\n",i);
		i++;
	}
	
	return(0);
}
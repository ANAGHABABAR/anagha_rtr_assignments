#include <stdio.h>

int main(void)
{
	//variable declarations
	int a;
	
	//code
	printf("\n\n");
	
	a = 5;
	if(a)//Non-Zero positive value
	{
		printf("if-block 1 : 'a' exists and has a value = %d.\n",a);
	}
	
	a = -5;//Non-zero negative value
	if(a)
	{
		printf("if-block 2 : 'a' exists and has a value = %d.\n",a);
	}
	
	a = 0;//zero value
	if(a)
	{
		printf("if-block 3 : 'a' exists and has a value = %d.\n",a);
	}
	
	printf("\nAll 3 statements are executed!");
	
	return(0);
}
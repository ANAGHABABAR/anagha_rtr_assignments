#include <stdio.h>

int main(void)
{
	//variable declarations
	int a, b, p;
	
	//code
	a = 9;
	b = 30;
	p = 40;
	
	printf("\n\n");
	
	if(a < b)
	{
		printf("\nA is less than B\n");
	}
	
	if(b != p)
	{
		printf("\nb is not equal to p!\n");
	}
	
	printf("\nBoth Comparisons have been completed\n");
	
	return 0;
}
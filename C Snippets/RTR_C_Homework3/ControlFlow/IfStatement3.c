#include <stdio.h>

int main(void)
{
	//variable declarations
	int num;
	
	//code
	printf("\n\n");
	
	printf("Enter value of 'num' : ");
	scanf("%d",&num);
	
	if(num < 0)
	{
		printf("\nNumber = %d is less than 0\n",num);
	}
	
	if((num >= 0) && (num <= 100))
	{
		printf("\nNumber %d is between 0 AND 100.\n",num);
	}
	
	if((num > 100) && (num <= 200))
	{
		printf("\n Number %d is between 100 AND 200.\n",num);
	}
	
	if((num > 200) && (num <= 300))
	{
		printf("\n The number %d is between 200 to 300.\n",num);
	}
	
	if((num > 300) && ( num <= 400))
	{
		printf("\nThe Number %d is between 300 and 400.\n",num);
	}
	
	if(num > 500)
	{
		printf("\nThe Number %d is Greater than 500\n",num);
	}
	return(0);
}
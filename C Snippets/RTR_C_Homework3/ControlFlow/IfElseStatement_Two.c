#include <stdio.h>

int main(void)
{
	//variable declarations
	int iAge;
	
	//code
	printf("\n\n");
	printf("Enter Age : ");
	scanf("%d",&iAge);
	printf("\n\n");
	if (iAge >= 18)
	{
		printf("\nEntering if-block ...\n");
		printf("\nYou are ELGIBLE for voting!!");
	}
	else
	{
		printf("\nEntering else-block ...\n\n");
		printf("\nYou are Not Eligible for voting!! ");
	}
	
	return(0);
}
#include <stdio.h>

int main(void)
{
	//variable declarations
	int a, b, p;
	
	//code
	a = 9;
	b = 30;
	p = 30;
	
	//First IF-ELSE pair
	printf("\n\n");
	if(a < b)
	{
		printf("Entering first if-block..\n");
		printf("A less than B!!!\n\n");
	}
	else
	{
		printf("Entering First else-block ...\n\n");
		printf("\nA is NOT less than B!!\n");
	}
	printf("\nFirst IF-ELSE pair execution completed.\n");
	
	//SECOND if-else pair
	printf("\n\n");
	if(b != p)
	{
		printf("\nInside second if-block.");
		printf("\nb is NOT Equal to p!\n");
	}
	else
	{
		printf("\nEntering second Else-block.");
		printf("\nb is EQUAL to p.");
	}
	printf("\nSecond if-else pair execution completed!!\n\n");

	return(0);
}

#include <stdio.h>

int main(void)
{
	//variable declarations
	int iNum;
	
	//code
	printf("\n\n");
	
	printf("\n Enter value of 'iNum' : ");
	scanf("%d",&iNum);
	
	if(iNum < 0) //if-01
	{
		printf("Number = %d is Less Than 0 (NEGATIVE)!!\n\n",iNum);
	}//closing of 'if - 01'
	else
	{
		if((iNum > 0) && (iNum <= 100))
		{
			printf("\nNumber %d is between range 0 to 100\n",iNum);
		}
		else //else - 01
		{
			if((iNum >= 100) && (iNum <= 200))//if - 03
			{
				printf("Number %d is between 100 to 200\n\n",iNum);
			}
			else
			{
				if((iNum > 200) && ( iNum <= 300))
				{
					printf("\nNumber %d is between 200 to 300 range\n\n",iNum);
				}
				else
				{
					if((iNum > 300) && (iNum <= 400))
					{
						printf("\nNumber %d is between 300 to 400 range\n",iNum);
					}
					else
					{
						if((iNum > 400) && (iNum <500))
						{
							printf("\nNumber %d is between 400 to 500 range\n",iNum);
						}
						else
						{
							if(iNum > 500)
							{
								printf("\nNumber %d is greater than 500\n",iNum);
							}
						}
					}
						
				}
			}
		}
	}
	return(0);
}
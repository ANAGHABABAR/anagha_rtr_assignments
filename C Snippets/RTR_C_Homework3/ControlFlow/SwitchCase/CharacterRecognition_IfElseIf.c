#include <stdio.h> //for printf()
#include <conio.h> //for getch()

//ASCII values for A to Z => 65 to 90
#define CHAR_ALPHA_UPPERcase_BEGIN 65
#define CHAR_ALPHA_UPPERcase_END 90

//ASCII values for a to z => 97 to 122
#define CHAR_ALPHA_LOWERcase_BEGIN 97
#define CHAR_ALPHA_LOWERcase_END 122

//ASCII values for 0 to 9 => 48 to 57
#define CHAR_DIGIT_BEGIN 48
#define CHAR_DIGIT_END 57

int main()
{
	//variale declarations
	char ch;
	int ch_value;
	
	//code
	printf("\nEnter Character : \n");
	ch = getchar();

	if( (ch == 'a' || ch == 'A') || (ch == 'e' || ch == 'E') || (ch == 'i' || ch == 'I') || (ch == 'o' || ch == 'O') || (ch == 'u' || ch == 'U') )
	{
		printf("\nThe Character \'%c\' is a VOWEL.\n",ch);
	}
	else
	{
		ch_value = (int)ch;
		if(((ch_value >=97) && (ch_value <= 122)) ||
			((ch_value >= 65) && (ch_value <= 90)))
			{
				printf("\nThe character \'%c\' is a CONSONANT.\n",ch);
			}
			
		else if((ch_value >=48) && (ch_value <=57))
		{
			printf("\nThe character \'%c\' is a DIGIT..\n",ch);
		}
		else
		{
			printf("\nThe character entered by YOU is a SPECIAL CHARACTER!!!\n");
		}
	}
	
	return(0);
}
#include <stdio.h>
#include <conio.h>

int main(void)
{
	//variable declaration
	int a,b;
	int result;
	
	char option, option_division;
	
	//code
	printf("\nEnter value for a : \n");
	scanf("%d",&a);
	
	printf("\nEnter value for b : \n");
	scanf("%d",&b);
	
	printf("\nEnter option in Character : \n");
	printf("\n'A' or 'a' For Addition : \n");
	printf("\n'S' or 's' For Subtraction : \n");
	printf("\n'M' or 'm' For Multiplication : \n");
	printf("\n'D' or 'd' For Division : \n");
	
	printf("Enter Option : ");
	option = getch();
	
	switch(option)
	{
		//FALL THROUGH for 'A' and 'a'
		case 'A' :
		case 'a' :
				result = a + b;
				printf("\nAddition of %d and %d is : %d\n",a,b,result);
				break;
		
		case 'S' :
		case 's' :
			if(a >= b)
			{
				result = a - b;
				printf("\n Subtraction of %d and %d is : %d\n",a,b,result);
				break;
			}
			else
			{
				result = b - a;
				printf("\n Subtraction of %d and %d is : %d\n",b,a,result);
				break;	
			}
				
		case 'M' :
		case 'm' :
				result = a * b;
				printf("\nMultiplication of %d and %d is : %d\n",a,b,result);
				break;
				
		case 'D' :
		case 'd' :
				printf("\nEnter option in Character : \n");
				printf("\n'Q' or 'q' or '/' for Quotient Upon Division : \n 'R' or 'r' or '%' for Remainder Upon the Division : ");
				
				printf("\nEnter option : ");
				option_division = getch();
				
				switch(option_division)
				{
					case '/':
					case 'Q':
					case 'q':
					
							if(a >= b)
							{
									result = a / b;
									printf("\nDivision of %d and %d gives Quotient %d\n",a,b,result);
							}
							else
							{
								result = b / a;
									printf("\nDivision of %d and %d Quotient %d\n",b,a,result);
							}
							break;
							
					case '%':
					case 'R':
					case 'r':
							if(a >= b)
							{
									result = a % b;
									printf("\nDivision of %d and %d gives Remainder %d\n",a,b,result);
							}
							else
							{
								result = b % a;
									printf("\nDivision of %d and %d Remainder %d\n",b,a,result);
							}
							break;
							
					default : 
							printf("\n%c is an Invalid character entered for Division. Please try again!\n",option_division);
							break;
				}
				break;//break case of d
			default :
					printf("\n%c is an Invalid character entered for Arithmetic operation\n",option);
					break;
	}
	printf("\nSwitch block execution completed!\n");

	
	
	return(0);
}
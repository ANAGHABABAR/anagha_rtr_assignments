#include <stdio.h>
#include <conio.h>

int main(void)
{
	int a,b;
	int result;
	
	char option, option_division;
	
	//code
	printf("\n\n");
	
	printf("\nEnter the value for a :\n");
	scanf("%d",&a);
	
	printf("\nEnter the value for b :\n");
	scanf("%d",&b);
	
	printf("\nEnter option in character : \n");
	printf("\nEnter option in Character : \n");
	printf("\n'A' or 'a' For Addition : \n");
	printf("\n'S' or 's' For Subtraction : \n");
	printf("\n'M' or 'm' For Multiplication : \n");
	printf("\n'D' or 'd' For Division : \n");
	
	printf("Enter Option : ");
	option = getch();
	
	if(option =='a' || option == 'A')
	{
		result = a + b;
		printf("Addition of a = %d and b = %d gives result %d!!!",a,b,result);
		
	}
	else if(option =='s' || option == 'S')
	{
		result = a - b;
		printf("\nSubtraction of a = %d and b = %d gives result %d!!!",a,b,result);	
	}
	else if(option =='m' || option == 'M')
	{
		result = a * b;
		printf("\nMultiplication of a = %d and b = %d gives result %d!!!",a,b,result);	
	}
	else  if(option == 'd' || option == 'D')
	{
		printf("\nEnter an opton in character : \n");
		printf("\n'Q' or 'q' or '/' For Quotient Upon Division \n");
		printf("\n'R' or 'r' or '%' For Remainder Upon Division\n");
		
		printf("\nEnter option :\n");
		option_division = getch();
		if(option_division == 'd' || option_division == 'D' || option_division == '/')
		{
			if(a >= b)
			{
				result = a/b;
				printf("\nDivision of a = %d and b = %d gives remainder %d!!!",a,b,result);
			}
			else
			{	
				result = b/a;
				printf("\nDivision of b = %d and a = %d gives remainder %d!!!",b,a,result);	
			}
		}
		else if(option_division == 'r' || option_division == 'R' || option_division == '%')
		{
			if(a >= b)
			{
				result = a%b;
				printf("\nDivision of a = %d and b = %d gives Quotient %d!!!",a,b,result);
			}
			else
			{	
				result = b%a;
				printf("\nDivision of b = %d and a = %d gives Quotient %d!!!",b,a,result);	
			}
		}
		else
		{
			printf("\nInvalid Input. Please try again!!\n");
		}
	}
	else
	{
		printf("\nInvalid character entered. Please enter valid input!!");
	}
	return(0);
}
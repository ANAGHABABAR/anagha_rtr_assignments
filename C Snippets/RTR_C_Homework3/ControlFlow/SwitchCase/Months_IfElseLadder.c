#include <stdio.h>

int main(void)
{
	//variable declarations
	int iMonth;
	
	//code
	printf("\nEnter Month Number : \n");
	scanf("%d",&iMonth);
	
	if(iMonth == 1)
	{
		printf("\nThe Month is JANUARY\n");
	}
	
	else if(iMonth == 2)
	{
		printf("\nThe Month is FEBRUARY\n");
	}
	
	else if(iMonth == 3)
	{
		printf("\nThe Month is MARCH\n");
	}
	
	else if(iMonth == 4)
	{
		printf("\nThe Month is APRIL\n");
	}
	
	else if(iMonth == 5)
	{
		printf("\nThe Month is MAY\n");
	}
	
	else if(iMonth == 6)
	{
		printf("\nThe Month is JUNE\n");
	}
	
	else if(iMonth == 7)
	{
		printf("\nThe Month is JULY\n");
	}
	
	else if(iMonth == 8)
	{
		printf("\nThe Month is AUGUST\n");
	}
	
	else if(iMonth == 9)
	{
		printf("\nThe Month is SEPTEMBER\n");
	}
	
	else if(iMonth == 10)
	{
		printf("\nThe Month is OCTOBER\n");
	}
	
	else if(iMonth == 11)
	{
		printf("\nThe Month is NOVEMBER\n");
	}
	
	else if(iMonth == 12)
	{
		printf("\nThe Month is DECEMBER\n");
	}
	
	else
	{
		printf("\nInvalid Input\n");
	}
	return(0);
}
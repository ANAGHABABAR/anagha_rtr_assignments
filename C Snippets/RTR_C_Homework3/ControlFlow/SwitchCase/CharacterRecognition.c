#include <stdio.h>//for printf()
#include <conio.h>//for getch()

//ASCII values for A to Z => 65 to 90
#define CHAR_ALPHA_UPPERcase_BEGIN 65
#define CHAR_ALPHA_UPPERcase_END 90

//ASCII values for a to z => 97 to 122
#define CHAR_ALPHA_LOWERcase_BEGIN 97
#define CHAR_ALPHA_LOWERcase_END 122

//ASCII values for 0 to 9 => 48 to 57
#define CHAR_DIGIT_BEGIN 48
#define CHAR_DIGIT_END 57

int main()
{
	//variale declarations
	char ch;
	int ch_value;
	
	//code
	printf("\nEnter Character : \n");
	ch = getch();
	
	switch(ch)
	{
		//FALL THROUGH Condition
		case 'A':
		case 'a':
		
		case 'E':
		case 'e':
		
		case 'I':
		case 'i':
		
		case 'O':
		case 'o':
		
		case 'U':
		case 'u':
				printf("\nCharacter \'%c\' Entered by you, Is A VOWEL CHARACTER From The English Alphabet!!!\n",ch);
				break;
		default : 
				ch_value = (int)ch;
				if(((ch_value >= CHAR_ALPHA_LOWERcase_BEGIN) && (ch_value <=CHAR_ALPHA_LOWERcase_END)) ||
				((ch_value >= CHAR_ALPHA_UPPERcase_BEGIN) && (ch_value <= CHAR_ALPHA_UPPERcase_END)))
				{
					printf("\nCharacter \'%c\' Entered by you, Is A CONSONANT CHARACTER From The English Alphabet!\n",ch);
				}
				else if((ch_value >= CHAR_DIGIT_BEGIN) && (ch_value <= CHAR_DIGIT_END))
				{
					printf("\nCharacter \'%c\' Entered by you, Is a DIGIT!\n",ch);
				}
	}
	
	
	return(0);
}




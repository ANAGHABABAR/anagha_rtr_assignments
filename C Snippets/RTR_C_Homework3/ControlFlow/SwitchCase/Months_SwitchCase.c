#include <stdio.h>

int main(void)
{
	//variable declarations
	
	int iMonth = 0;
	
	//code
	printf("\nEnter Number of Month between 1 to 12: \n");
	scanf("%d",&iMonth);
	
	switch(iMonth)
	{
		case 1:
				printf("\nThe months is JANUARY");
				break;
		
		case 2: 
				printf("\nThe months is FEBRUARY");
				break;
		
		case 3: 
				printf("\nThe months is MARCH");
				break;
		
		case 4: 
				printf("\nThe months is APRIL");
				break;
		
		case 5: 
				printf("\nThe months is MAY");
				break;
		
		case 6: 
				printf("\nThe months is JUNE");
				break;
		
		case 7: 
				printf("\nThe months is JULY");
				break;
		
		case 8: 
				printf("\nThe months is AUGUST");
				break;
				
		case 9: 
				printf("\nThe months is SEPTEMBER");
				break;
		
		case 10: 
				printf("\nThe months is OCTOBER");
				break;
		
		case 11: 
				printf("\nThe months is NOVEMBER");
				break;
		
		case 12: 
				printf("\nThe months is DECEMBER");
				break;
			
		default : 
				printf("\nInvalid Month Number %d Entered !!! Please try again...\n",iMonth);
				break;
			
	}
	return(0);
}
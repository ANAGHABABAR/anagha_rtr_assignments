#include<stdio.h>

int main()
{
	//variable declarations
	int i, j, k;
	
	//code
	printf("\n\n");
	
	i = 1;
	do
	{
		printf("\ni = %d",i);
		printf("\n--------------\n");
		
		j = 1;
		do
		{
			printf("\nj = %d\n",j);
			printf("\n***************\n");
			
			k = 1;
			do
			{
				printf("\nk = %d",k);
				k++;
			}while(k <= 3);
			j++;
		}while(j <= 5);
		i++;
		printf("\n\n");
	}while(i <= 10);
	
	return(0);
}
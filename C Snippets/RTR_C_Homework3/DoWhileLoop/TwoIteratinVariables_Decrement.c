#include<stdio.h>

int main(void)
{
	///variable declarations
	int i, j;
	
	i = 10;
	j = 100;
	
	printf("\nPrinting numbers in reverse order : \n");
	
	do
	{
		printf("i = %d,\tj = %d\n",i,j);
		i--;
		j = j - 10;
		
	}while(i >= 1, j >= 10);
	
	return(0);
}
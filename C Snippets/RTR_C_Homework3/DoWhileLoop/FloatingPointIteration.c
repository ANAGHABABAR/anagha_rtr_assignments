#include<stdio.h>

int main()
{
	//variable declarations
	float f = 0.0f, fNum = 1.2f;
	
	//code
	printf("\nPrinting numbers from %f to %f\n",f, fNum * 10.0f);
	
	f = fNum;
	
	do
	{
		printf("\n%f", f);
		f = f + fNum;
		
	}while(f <= fNum * 10.0f);

	return(0);
}
#include<stdio.h>

int main()
{
	//variable declarations
	int i;
	
	printf("\n\n");
	
	printf("Printing Digits 1 to 10\n");
	
	i = 1;
	do
	{
		printf("\n%d",i);
		i++;
	}while(i <= 10);
	
	return(0);
}
#include<stdio.h>

int main()
{
	char option, ch = '\0';
	
	//code
	printf("\n\n");
	printf("\nOnce the infinite loop begins, Enter 'Q' or 'q' to Exit the Infinite loop.\n Enter 'Y' or 'y' to Initiate the infinite loop. \n");
	printf("\n\n");
	
	option = getch();
	if(option == 'Y' || option == 'y')
	{
		do
		{
			printf("\nIn Loop ...\n");
			ch = getch();
			if(ch == 'Q' || ch == 'q')
			{
				break;
			}
		}while(1);
		printf("\nExiting Infinite Loop...\n");
	}
	else
	{
		printf("\nYou must press 'y' or 'Y' to Initiate the user controlled infinite loop. Please try again..\n");
	}
	
	
	return(0);
}
#include<stdio.h>

int main(void)
{
	//variable declarations
	char option,ch = '\0';
	
	//code
	printf("Once Infinite loop bgins, enter 'Q' or 'q' to Quit the Infinite loop.");
	
	do
	{
		do
		{	
			printf("\nIn Loop...\n");
			ch = getch();
			
		}while(ch == 'Q' || ch == 'q');
		
		printf("\nExiting user controlled infinite loop..\n");
		printf("\nDo you want to Enter Infinite loop again?\n");
		option = getch();
		
	}while(option == 'Y' || option == 'y');
	
	return(0);
}
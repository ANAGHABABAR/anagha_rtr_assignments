#include<stdio.h>

int main()
{
	//variable declarations
	int i,j;
	
	//code
	printf("\n\n");
	
	printf("\nThe outer loop prints odd numbers between 1 to 10\n");
	printf("\nInner loop prints even numbers between 1 to 10 for every odd number printed in outer loop.\n");
	
	for(i = 0; i <= 10 ; i++)
	{
		if(i % 2 != 0)
		{
			printf("\ni = %d\n",i);
			for(j = 0; j <= 10; j++)
			{
				if(j % 2 == 0)
				{
					printf("\nj = %d\n",j);
				}
				else
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else
		{
			continue;
		}
	}
	printf("\n\n");
	
	return(0);
}
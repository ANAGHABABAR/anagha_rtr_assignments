#include<stdio.h>

int main()
{
	//variable declarations
	int i;
	
	//code
	printf("\n\n");
	printf("\nPrinting Even numbers from 0 to 100.\n");
	
	for(i = 0; i<=100 ; i++)
	{
		if(i % 2 != 0)
		{
			continue;
		}
		else
		{
			printf("\ni = %d\n",i);
		}
	}
	printf("\n\n");
	
	return(0);
}